# Laravel PHP Framework Scaffold  Webflavia

Laravel PHP Framework Scaffold Webflavia, is a fork of Laravel PHP Framework with an implementation of a cms.

# Usage
1 - Clone the repository

2 - Turn the following commands on your terminal:

	* Composer install

	* npm install

	* php artisan key:generate

3 - Create a database and arrow the settings in the .env file.

	* DB_CONNECTION=

	* DB_HOST=

	* DB_PORT=

	* DB_DATABASE=

	* DB_USERNAME=

	* DB_PASSWORD=

4 - Turn the following commands on your terminal:

	* php artisan migrate

	* php artisan db:seed

	* gulp or gulp --production


Ready your application is ready to run, access your application / cms to access the cms. By default the email is development@webflavia.com.br and password 123123.