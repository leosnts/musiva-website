<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunitiesTable extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_community', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('community_id');
            $table->string('name')->nullable();
            $table->string('history')->nullable();
            $table->string('coordinators')->nullable();
            $table->string('mass')->nullable();
            $table->string('activities')->nullable();
            $table->string('contact')->nullable();
            $table->integer('status')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_community');
    }
}
