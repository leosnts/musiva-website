const elixir = require('laravel-elixir');


//require('laravel-elixir-vue-2');


elixir(function(mix) {

	/*
	 |--------------------------------------------------------------------------
	 |	Core
	 |--------------------------------------------------------------------------
	*/
	 	mix.less(['core/404.less'], "public/css/404.css")
        .less(['core/default.less'], "public/css/default.css");  

    
    /*
	 |--------------------------------------------------------------------------
	 |	Site
	 |--------------------------------------------------------------------------
	*/
    	//LESS
   	 	mix.less(['pages/site/home.less'], "public/css/site-home.css");   
   	 	mix.less(['pages/site/community.less'], "public/css/site-community.css"); 
   	 	mix.less(['pages/site/views-all.less'], "public/css/site-community-show.css");  
   	 	mix.less(['pages/site/views-all.less'], "public/css/site-views-all.css");  
   	 	mix.less(['pages/site/views-all.less'], "public/css/site-views-all-show.css");  
   	 	mix.less(['pages/site/views-all.less'], "public/css/site-patron.css");  
   	 	mix.less(['pages/site/priest.less'], "public/css/site-priest.css");   
   	 	mix.less(['pages/site/priest.less'], "public/css/site-priest-show.css");   
   	 	mix.less(['pages/site/news.less'], "public/css/site-news.css");   
   	 	mix.less(['pages/site/news-show.less'], "public/css/site-news-show.css");   
   	 	mix.less(['pages/site/photo.less'], "public/css/site-photo.css"); 
   	 	mix.less(['pages/site/photo-show.less'], "public/css/site-photo-show.css");   
   	 	mix.less(['pages/site/video.less'], "public/css/site-video.css");   
   	 	mix.less(['pages/site/video-show.less'], "public/css/site-video-show.css");  
   	 	mix.less(['pages/site/contacts.less'], "public/css/site-contacts.css");    
   	 	mix.less(['pages/site/sector.less'], "public/css/site-sectorCommunity.css");    
   	 	mix.less(['pages/site/ministry.less'], "public/css/site-ministry.css");    
   	 	mix.less(['pages/site/ministry-show.less'], "public/css/site-ministry-show.css");    
   	 	mix.less(['pages/site/liturgy.less'], "public/css/site-liturgy.css");    
    
	    //JAVASCRIPT  LIBS
	    mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
		], 'public/js/site-home-libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
		], 'public/js/site-community-libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
	    ], 'public/js/site-community-show-libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
		], 'public/js/site-views-all-libs.js'); 

		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
		], 'public/js/site-views-all-show.libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
		], 'public/js/site-views-all-cordenadores.libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
		], 'public/js/site-views-all-contatos.libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
		], 'public/js/site-views-all-atividades.libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
		], 'public/js/site-views-all-missa.libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
	    ], 'public/js/site-priest-libs.js'); 
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
	    ], 'public/js/site-priest-show-libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
	    ], 'public/js/site-news-libs.js'); 
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
	    ], 'public/js/site-news-show-libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
	    ], 'public/js/site-video-libs.js'); 
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
	    ], 'public/js/site-video-show-libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
			 'libs/lightbox.js', 
	    ], 'public/js/site-photo-libs.js'); 
		mix.scripts([
	        'libs/jquery-3.1.0.js',
			'libs/slick.js', 
			'libs/lightbox.js', 
		], 'public/js/site-photo-show-libs.js'); 
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
		], 'public/js/site-patron-libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	        'libs/jquery.mask.min.js',
	        'libs/sweetalert.min.js',
	        'libs/jquery.validate.min.js',
		], 'public/js/site-contacts-libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
			 'libs/lightbox.js', 
		], 'public/js/site-sectorCommunity-libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
			 'libs/lightbox.js', 
		], 'public/js/site-ministry-libs.js');
		 
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
			 'libs/lightbox.js', 
		], 'public/js/site-ministry-show-libs.js'); 
		
		mix.scripts([
	        'libs/jquery-3.1.0.js',
	         'libs/slick.js', 
			 'libs/lightbox.js', 
	    ], 'public/js/site-liturgy-libs.js'); 

	    //JAVASCRIPT  COMPONENTES
	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-home.js'); 
		

	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-community.js'); 
		
	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
	    ], 'public/js/site-community-show.js'); 

	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
	    ], 'public/js/site-patron.js'); 

	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-views-all.js'); 

		mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-views-all-show.js'); 
		mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-views-all-atividades.js'); 
		mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-views-all-contatos.js'); 
		mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-views-all-cordenadores.js'); 
		mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-views-all-missa.js'); 
		
	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
	    ], 'public/js/site-priest.js'); 
	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
	    ], 'public/js/site-priest-show.js'); 
		
	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
	    ], 'public/js/site-news.js'); 
	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
	        'modules/acessibilidade.js',
	    ], 'public/js/site-news-show.js'); 
		
	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
	    ], 'public/js/site-video.js'); 
	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
	    ], 'public/js/site-video-show.js'); 
		
	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
	    ], 'public/js/site-photo.js'); 
	    mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-photo-show.js'); 
		
		mix.scripts([
	        'modules/navigation.js',
	        'modules/maps.js',
	        'modules/mask.js',
			'modules/contact.js',
			'modules/navbar.js',
	        'modules/newsletters.js',
		], 'public/js/site-contacts.js'); 
		
		mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-sectorCommunity.js'); 

		mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-ministry.js'); 

		mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-ministry-show.js'); 

		mix.scripts([
	        'modules/navigation.js',
	        'modules/slides.js',
	        'modules/navbar.js',
		], 'public/js/site-liturgy.js'); 
		
		

    /*
	 |--------------------------------------------------------------------------
	 |	CMS
	 |--------------------------------------------------------------------------
	*/
    	//LESS
        mix.less(['pages/cms/home.less'], "public/css/cms-home.css")
        .less(['pages/cms/auth.less'], "public/css/cms-auth.css");   
        
    
        //JAVASCRIPT  LIBS      
        mix.scripts([
            'libs/jquery-3.1.0.js',
            'libs/es6-promise.auto.min.js',
            'libs/sweetalert2.min.js',
            'libs/jquery.mask.js',
            'libs/select2.min.js'
        ], 'public/js/default-libs.js');       


        // JAVASCRIPT  COMPONENTES
        mix.scripts([
            'modules/navigation-sidebar.js',
            'modules/flexUpload.js',
            'modules/mask.js',
            'modules/select.js'
        ], 'public/js/default.js'); 

    /*
	 |--------------------------------------------------------------------------
	 | Version files  
	 |--------------------------------------------------------------------------
	*/

    	mix.version(["css/*.css", "js/*.js"]);

    /*
	 |--------------------------------------------------------------------------
	 | Version files  
	 |--------------------------------------------------------------------------
	*/
   	
   		//mix.browserify('../../../public/js/home.js', null, null);   

    
    /*
	 |--------------------------------------------------------------------------
	 | Copy Fonts
	 |--------------------------------------------------------------------------
	*/
    
	    mix.copy('resources/assets/fonts', 'public/build/css/fonts');
	    mix.copy('resources/assets/img', 'public/img');
});