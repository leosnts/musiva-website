@extends('layouts._app')

@section('content')	
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
			<div class="row">	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-portfolio')!!}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Novidades Musiva</a>
						</li>
						<li>
							<a><i class="fa fa-angle-double-right" aria-hidden="true"></i> {!!(!isset($portfolio)) ? 'Novo' : 'Editar'!!}</a>
						</li>
					</ul>
				</div>		
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
						@if(isset($portfolio))
							
							<form autocomplete="on" action="{!! route('cms-portfolio-update') !!}" method="post"
							enctype="multipart/form-data">
								<input type="hidden" name="id" id="id" value="{{$portfolio->portfolio_id}}">
			            @else
							
							<form autocomplete="off" action="{!! route('cms-portfolio-store') !!}" method="post"
							enctype="multipart/form-data">
						@endif        		    
						{{ csrf_field() }}
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<label for="imagem_portfolio">Imagem do portfólio</label>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<div style="width: 150px;height: 150px;">
											@if(isset($portfolio))
												<img src="<?php echo asset('storage/app/public/'.$portfolio->imagem_portfolio)?>" style="" alt="">	
											@endif
										</div>
										
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-row">     
				                       	<input type="file" name="imagem_portfolio" id="imagem_portfolio">
			            			   	<label class="error">{!!$errors->first('imagem_portfolio')!!}</label>                 
					                </div>
								</div>	
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<label for="nome">Nome o evento/cantor(a)/Banda</label>                     
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									@if(isset($portfolio))
										<div class="form-row">                    
										<input type="text" name="nome" id="nome" value="{{$portfolio->nome}}">
											<label class="error">{!!$errors->first('nome')!!}</label>    
										</div>
									@else 
										<div class="form-row">                    
											<input type="text" name="nome" id="nome">
											<label class="error">{!!$errors->first('nome')!!}</label>    
										</div>
									@endif
								</div>										
							</div>
							
							<div class="row">
								<div class="col-lg-12">
									<div class="form-row">										
										<button class="pull-right btn-primary">Salvar <i class="fa fa-check-square" aria-hidden="true"></i></button>
									</div>
								</div>
							</div>	           	            
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>
@include('cms.includes.footer')
@endsection