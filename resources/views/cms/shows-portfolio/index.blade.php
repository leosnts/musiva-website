@extends('layouts._app')

@section('content')	
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
                <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-default">
                                {!! Form::open(['method' => 'get', 'autocomplete' => 'on', 'route' => ['cms-states']]) !!}      
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-row">
                                                <a href="{!!route('cms-portfolio-create')!!}" class="pull-right btn-quartenary">Novo <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>	           	            
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
			<div class="row">	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-portfolio')!!}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ja passaram pela Musiva</a>
						</li>
					</ul>
				</div>		
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
                        @if (isset($portfolios))
                            @foreach($portfolios as $n)
                                <div class="col-md-3">
                                    <div class="col-md-12" style="text-align: center;">
                                        <img src="<?php echo asset('storage/app/public/'.$n->imagem_portfolio)?>" style="width: 200px;height: 200px;margin-top: 25px;" alt="">
                                        <p>{{$n->nome}}</p>
                                        <p>Cadastro: {{ Carbon\Carbon::parse($n->created_at)->format('d/m/Y') }}</p>
                                    </div>
                                    <div class="col-md-6" style="text-align: center;">
                                        <a href="{!!route('cms-portfolio-edit', $n->portfolio_id)!!}" style="background-color: #003469;width: 100%;height: 20px;padding: 9px;color: white;border-radius: 6px;"> Alterar </a>
                                    </div>
                                    <div class="col-md-6" style="text-align: center;">
                                        <a onclick="excluir(<?php echo $n->portfolio_id ?>)" style="background-color: red;width: 100%;height: 20px;padding: 9px;color: white;border-radius: 6px;"> Excluir </a>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div style="background-color: blue;width: 400px;height: 400px;"></div>
                        @endif
                    </div>
				</div>
			</div>
		</div>
			@include('layouts._alerts')	
			
		</div>
	</div>	
</section>
<script>
    function excluir(id){

        var confirmando = confirm('Deseja realmente excluir esta imagem ?');

        if(confirmando == true){
            var ajax = new XMLHttpRequest();
            token = document.querySelector('meta[name="csrf-token"]').content;

            ajax.open("GET", "Musiva2020/cms/portfolio/excluir/"+id, true);
            ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            ajax.send("_token="+token+"&id="+id);

            ajax.onreadystatechange = function() {
                if (ajax.status == 200) {
                    var data = ajax.responseText;
                    console.log(data);
                    window.location.reload(true);
                } else {
                    console.log('error');
                }
            }
        } else {
            console.log('request cancel');
        }
    }
</script>
@include('cms.includes.footer')
@endsection