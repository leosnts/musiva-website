@extends('layouts._app')

@section('content')	
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
			<div class="row">	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-makeup')!!}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> MakeUp do site Studio202</a>
						</li>
					</ul>
				</div>		
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
                        @if(isset($makeup))
                            @foreach($makeup as $m)
                                {!! Form::model($makeup, ['route' => ['cms-makeup-update', $m->makeups_id], 'method' => 'put']) !!}
                            @endforeach
			            @else
                            {!! Form::open(['method' => 'post', 'autocomplete' => 'on', 'route' => ['cms-makeup-create']]) !!}  
			            @endif 
                        
                        {!! csrf_field() !!}         		    
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-row">                    
                                    {!!Form::label('content', 'Conteúdo')!!}
                                    @foreach($makeup as $m)
                                        {!!Form::textarea('content', $m->conteudo) !!}	
                                    @endforeach
                                    @ckeditor('content')
                                   <label class="error">{!!$errors->first('content')!!}</label>                      
                                </div>
                            </div>																					
                        </div>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-row">										
										<button class="pull-right btn-primary">Buscar <i class="fa fa-search" aria-hidden="true"></i></button>
									</div>
								</div>
							</div>	           	            
						{!! Form::close() !!}
					</div>
				</div>
			</div>
			@include('layouts._alerts')	
			
		</div>
	</div>	
</section>
@include('cms.includes.footer')
@endsection