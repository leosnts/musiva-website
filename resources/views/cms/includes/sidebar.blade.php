<div class="sidebar">
	<div class="profile text-center">
		<figure>
			{!!img(@Auth::user()->image, 120, 120, true, true)!!}
		</figure>
		<h1 class="name">Olá {!!@Auth::user()->first_name." ".@Auth::user()->last_name!!}</h1>
		<div class="text">
			Seja bem vindo (a)
		</div>
	</div>
	<ul>
		<li class="active">
			<a href="{{route('cms-home')}}">
				<i class="fa fa-home" aria-hidden="true"></i> Home
			</a>
		</li>
		<li class="sub-menu">
			<a>
				<i class="fa fa-cog" aria-hidden="true"></i> Conteúdos do site
			</a>
			<ul>
				<li class="">
					<a href="{{route('cms-novidades')}}">
						<i class="fa fa-caret-right" aria-hidden="true"></i> Novidades
					</a>
				</li>
				<li class="">
					<a href="{{route('cms-portfolio')}}">
						<i class="fa fa-caret-right" aria-hidden="true"></i> Portfólios
					</a>
				</li>
				{{-- <li class="">
					<a href="{{route('cms-unhas')}}">
						<i class="fa fa-caret-right" aria-hidden="true"></i> Unhas
					</a>
				</li>
				<li class="">
					<a href="{{route('cms-esteticas')}}">
						<i class="fa fa-caret-right" aria-hidden="true"></i> Estética
					</a>
				</li>
				<li class="">
					<a href="{{route('cms-barbearias')}}">
						<i class="fa fa-caret-right" aria-hidden="true"></i> Barbearia
					</a>
				</li> --}}
			</ul>
		</li>
		<li class="sub-menu">
			<a>
				<i class="fa fa-cog" aria-hidden="true"></i> Configurações
			</a>
			{{-- <ul>
				@shield('cms-countries')
				<li>
					<a href="{!!route('cms-countries')!!}"><i class="fa fa-caret-right" aria-hidden="true"></i>
						Paises</a>
				</li>
				@endshield
				@shield('cms-states')
				<li>
					<a href="{!!route('cms-states')!!}"><i class="fa fa-caret-right" aria-hidden="true"></i> Estados</a>
				</li>
				@endshield
				@shield('cms-cities')
				<li>
					<a href="{!!route('cms-cities')!!}"><i class="fa fa-caret-right" aria-hidden="true"></i> Cidades</a>
				</li>
				@endshield
				@shield('cms-sexes')
				<li>
					<a href="{!!route('cms-sexes')!!}"><i class="fa fa-caret-right" aria-hidden="true"></i> Sexo</a>
				</li>
				@endshield
				@shield('cms-users')
				<li>
					<a href="{!!route('cms-users')!!}"><i class="fa fa-caret-right" aria-hidden="true"></i> Usuários</a>
				</li>
				@endshield
				@shield('cms-roles')
				<li>
					<a href="{!!route('cms-roles')!!}"><i class="fa fa-caret-right" aria-hidden="true"></i> Grupos de
						Usuários</a>
				</li>
				@endshield
				@shield('cms-log-errors')
				<li>
					<a href="{!!route('cms-log-errors')!!}"><i class="fa fa-caret-right" aria-hidden="true"></i> Log de
						erros</a>
				</li>
				@endshield
			</ul> --}}
		</li>
		<li>
			<a href="{!!route('cms-auth-logout')!!}">
				<i class="fa fa-sign-out" aria-hidden="true"></i> Sair
			</a>
		</li>
	</ul>
</div>