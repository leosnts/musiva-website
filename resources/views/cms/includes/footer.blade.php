
<footer>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="pull-right"><i class="fa fa-globe" aria-hidden="true"></i> {!!date('Y')!!} - Todos os direitos reservados.</p>
				<p>Desenvolvido por WEBFLAVIA</p>
			</div>
		</div>
	</div>
</footer>