@extends('layouts._app')

@section('content')
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-news')!!}"><i class="fa fa-angle-double-right"
									aria-hidden="true"></i> adicionar grupos</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
						{!! Form::open(['method' => 'get', 'autocomplete' => 'on', 'route' => ['cms-community']]) !!}
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('title', 'Titulo')!!}
									{!!Form::text('title') !!}
									<label class="error">{!!$errors->first('title')!!}</label>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('status', 'Status')!!}
									{!!Form::select('status', ['1' => 'Ativo', '2' => 'Inativo'], null, ['placeholder'
									=> "Selecione"]) !!}
									<label class="error">{!!$errors->first('status')!!}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-row">
									@shield('cms-news-create')
									<a href="{!!route('cms-community-create')!!}" class="pull-right btn-quartenary">Novo
										<i class="fa fa-plus-circle" aria-hidden="true"></i></a>
									@endshield
									<button class="pull-right btn-primary">Buscar <i class="fa fa-search"
											aria-hidden="true"></i></button>
								</div>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
			@include('layouts._alerts')
			<div class="row">
				@if(session()->has('message'))
				<div class="alert alert-success">
					{{ session()->get('message') }}
				</div>
				@endif
				<div class="col-lg-12">
					<div class="panel-default list">
						<table>
							<thead>
								<tr>
									<th>Id</th>
									<th>Comunidade</th>
									<th>Status</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody>
								@foreach($community as $c)
								<tr>
									<td data-label='ID'>{!!$c->community_id!!}</td>
									<td data-labe='Nome'>{!! $c->name !!}</td>
									<td data-label='Status'>
										@if($c->status == 1)
										<p style="color: green">Ativo</p>
										@else
										<p style="color: red">Inativo</p>
										@endif
									</td>
									<td data-label='Acoes'>
										<a href="{!!route('cms-community-delete', $c->community_id)!!}" title="Excluir"
											class="btn-primary"><i class="fa fa-times-circle"
												aria-hidden="true"></i></a>
										<a href="{!!route('cms-community-show', $c->community_id)!!}" title="Visualizar"
											class="btn-secondary"><i class="fa fa-pencil-square-o"
												aria-hidden="true"></i></a>
										<a style="background-color: green"
											href="{!!route('cms-community-image', $c->community_id)!!}"
											title="Adicionar imagens" class="btn-secondary"><i class="fa fa-plus"
												aria-hidden="true"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">

			</div>
		</div>
	</div>
</section>
@include('cms.includes.footer')
@endsection