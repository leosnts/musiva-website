@extends('layouts._app')

@section('content')
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-news')!!}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Comunidade</a>
						</li>
						<li>
							<a><i class="fa fa-angle-double-right" aria-hidden="true"></i> {!!(!isset($gropCommunity)) ?
								'Novo' : 'Editar'!!}</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
						@if(isset($gropCommunity))
						{!! Form::model($gropCommunity, ['route' => ['cms-community-update',
						$gropCommunity->community_id], 'method' => 'put']) !!}
						@else
						{!! Form::open(['method' => 'post', 'autocomplete' => 'off', 'route' =>
						['cms-community-create']]) !!}
						@endif
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('name', 'Nome ')!!}
									{!!Form::text('name') !!}
									<label class="error">{!!$errors->first('name')!!}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12col-xs-12">
								<div class="form-row">
									<label for="">Selecione o setor</label>
									<select name="sector_id" id="">
										<option value="">selecione</option>
										@foreach($sector as $s)
										<option value="{{ $s->id_sector }}">{{ $s->name }}</option>
										@endforeach
									</select>
									<label class="error">{!!$errors->first('')!!}</label>
								</div>
							</div>
						</div>
						<div class="row box-image">
								<div class="col-lg-12">
									<div class="form-row"> 
										<label for="">Imagem Capa</label>
										<input type="file" class="FlexUpload" data-url="{{route('flexUpload')}}" data-entity="Community" data-entity-id="{!!(isset($gropCommunity)) ? $gropCommunity->community_id : ''!!}"  name="image">   
					                </div>
								</div>
							</div>	
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('history', 'Historia')!!}
									{!!Form::textarea('history') !!}
									@ckeditor('history')
									<label class="error">{!!$errors->first('history')!!}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('coordinators', 'Cordenadores')!!}
									{!!Form::textarea('coordinators') !!}
									@ckeditor('coordinators')
									<label class="error">{!!$errors->first('coordinators')!!}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('mass', 'Missa')!!}
									{!!Form::textarea('mass') !!}
									@ckeditor('mass')
									<label class="error">{!!$errors->first('mass')!!}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('activities', 'Atividades')!!}
									{!!Form::textarea('activities') !!}
									@ckeditor('activities')
									<label class="error">{!!$errors->first('activities')!!}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('contact', 'Contato')!!}
									{!!Form::textarea('contact') !!}
									@ckeditor('contact')
									<label class="error">{!!$errors->first('contact')!!}</label>
								</div>
							</div>
						</div>
						<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12col-xs-12">
									<div class="form-row">
										{!!Form::label('status', 'Status')!!}
										{!!Form::select('status', ['1' => 'Ativo', '2' => 'Inativo'], null, ['placeholder'
										=> "Selecione"]) !!}
										<label class="error">{!!$errors->first('status')!!}</label>
									</div>
								</div>
							</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-row">
									<button class="pull-right btn-primary">Salvar <i class="fa fa-check-square"
											aria-hidden="true"></i></button>
								</div>
							</div>
						</div>
			
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('cms.includes.footer')
@endsection