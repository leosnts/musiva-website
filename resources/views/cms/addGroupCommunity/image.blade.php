@extends('layouts._app')

@section('content')
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-cities')!!}"><i class="fa fa-angle-double-right"
									aria-hidden="true"></i> Adicionar imagens</a>
						</li>

					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<h3 style="background-color:crimson; color: #fff; padding: 10px;">
								@foreach ($errors->all() as $error)
								{{ $error }}
								@endforeach
							</h3>
						</div>
						@endif
						<form action="{!! route('cms-community-addImage', $idImage) !!}" method="post"
							enctype="multipart/form-data">
							{{ csrf_field() }}
							<label style="font-size: 20px;" for="">Selecionar imagems</label>
							<br><br>
							<input type="file" id="imagem" name="image[]" multiple>
							<br><br>
							<button style="background-color:cadetblue" class="btn btn-primary" type="submit">Salvar
								Imagens</button>
						</form>
						<br><br>
						<div class="row">
							@foreach($imagem as $i)
							<div class="card col-md-3">
								<?php $id = $i->imagem_id ?>
										<img  style="width: 250px; height: 250px;" src="<?php echo asset('public/storage/'.$i->path.'')?>" class="card-img-top" alt="imagem">
								<br><br>
								<div class="card-body">
									<button type="button" onclick="excluir(<?php echo $id ?>)" class="btn btn-primary">Excluir</button>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<script type="text/javascript">
	function excluir(id){

		var confirmando = confirm('Deseja realmente excluir esta imagem ?')

		if(confirmando == true){
			// Exemplo de requisição POST
			var ajax = new XMLHttpRequest();
			token = document.querySelector('meta[name="csrf-token"]').content;

			// Seta tipo de requisição: Post e a URL da API
			ajax.open("POST", "paroquia/cms/community/deleteimage/"+id, true);
			ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			// Seta paramêtros da requisição e envia a requisição
			ajax.send("_token="+token+"&id="+id);

			// Cria um evento para receber o retorno.
			ajax.onreadystatechange = function() {
			// Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
				if (ajax.status == 200) {
					var data = ajax.responseText;
				// Retorno do Ajax
					console.log(data);
					window.location.reload(true);
				} else {
					console.log('error')
				}
			}
		} else {
			console.log('request cancel')
		}
	}
</script>
@include('cms.includes.footer')
@endsection