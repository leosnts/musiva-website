@extends('layouts._app')

@section('content')
@include('cms.includes.header')
<section class="wrapper">
    @include('cms.includes.sidebar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="breadcrumb">
                        <li>
                            <a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li>
                            <a href="{!!route('cms-contacts')!!}"><i class="fa fa-angle-double-right"
                                    aria-hidden="true"></i> contatos site</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel-default">
                        {!! Form::open(['method' => 'get', 'autocomplete' => 'on', 'route' => ['cms-contacts']]) !!}
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-row">
                                    {!!Form::label('name', 'Nome')!!}
                                    {!!Form::text('name') !!}
                                    <label class="error">{!!$errors->first('name')!!}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-row">
                                    <button class="pull-right btn-primary">Buscar <i class="fa fa-search"
                                            aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            @include('layouts._alerts')
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel-default list">
                        @if(count($contacts) > 0)
                        <table>
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Telefone</th>
                                    <th>Ver mensagem</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contacts as $c)
                                <tr>
                                    <td data-label='NOME'>{!! $c->name !!}</td>
                                    <td data-label='EMAIL'>{!! $c->email !!}</td>
                                    <td data-label='TELEFONE'>{!! $c->cellFone !!}</td>
                                    <td data-label='Acoes'>
                                        <a href="{!!route('cms-contacts-show', $c->contact_id)!!}" title="Visualizar mensagem" class="btn-secondary"><i class="fa fa-binoculars" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        @else
                        <p style="color:red; font-size: 20px;">Nenhuma mensagem de contato encontrada</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    {!!$contacts->appends(Request::input())->render()!!}
                </div>
            </div>
        </div>
    </div>
</section>
@include('cms.includes.footer')
@endsection