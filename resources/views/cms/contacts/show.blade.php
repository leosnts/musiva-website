@extends('layouts._app')

@section('content')
@include('cms.includes.header')
<section class="wrapper">
    @include('cms.includes.sidebar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="breadcrumb">
                        <li>
                            <a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li>
                            <a href="{!!route('cms-contacts')!!}"><i class="fa fa-angle-double-right"
                                    aria-hidden="true"></i> contatos site</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel-default list">
                        @foreach($contacts as $c)
                        <h3>Nome: </h3>
                        <p>{{ $c->name }}</p>
                        <h3>Email: </h3>
                        <p>{{ $c->email }}</p>
                        <h3>Celular: </h3>
                        <p>{{ $c->cellFone }}</p>
                        <h3>Assunto: </h3>
                        <p>{{ $c->subject }}</p>
                        <h3>Mensagem: </h3>
                        <p>{{ $c->message }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">

                </div>
            </div>
        </div>
    </div>
</section>
@include('cms.includes.footer')
@endsection