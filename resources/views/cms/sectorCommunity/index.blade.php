@extends('layouts._app')

@section('content')	
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
			<div class="row">	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-sector')!!}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> adicionar setores</a>
						</li>
					</ul>
				</div>		
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-row">
										  <a href="{!!route('cms-sectorView-create')!!}" class="pull-right btn-quartenary">Novo <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
									</div>
								</div>
							</div>	           	            
					</div>
				</div>
			</div>
			@include('layouts._alerts')	
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default list">
						<table>
							<thead>
								<tr>
									<th>Id</th>
									<th>Nome</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($sector as $s)
                                <tr>
                                    <td data-label='ID'>{!!$s->id_sector!!}</td>
                                    <td data-labe='Nome'>{!! $s->name !!}</td>
                                    <td data-label='Acoes'>
                                        <a href="{!!route('cms-sector-delete', $s->id_sector)!!}" title="Excluir" class="btn-primary"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                                        <a href="{!!route('cms-sector-show', $s->id_sector)!!}" title="Visualizar" class="btn-secondary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
		
			</div>
		</div>
	</div>	
</section>
@include('cms.includes.footer')
@endsection