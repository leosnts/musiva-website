@extends('layouts._app')

@section('content')	
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
                <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-default">
                                {!! Form::open(['method' => 'get', 'autocomplete' => 'on', 'route' => ['cms-states']]) !!}      
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-row">
                                                <a href="{!!route('cms-novidades-create')!!}" class="pull-right btn-quartenary">Novo <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>	           	            
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
			<div class="row">	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-novidades')!!}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Novidades Musiva</a>
						</li>
					</ul>
				</div>		
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
                        @if (isset($novidades))
                        @foreach($novidades as $n)
                            <div class="col-md-2">
                                <img src="<?php echo asset('storage/app/public/'.$n->imagem_novidade)?>" style="    width: 100%;
                                height: 238px;
                                margin-top: 25px;" alt="">
                                <p style="width: 100%;text-align: center;font-size: 24px;">{{$n->nome}}</p>
                                <p style="width: 100%;text-align: center;font-size: 24px;">{{ Carbon\Carbon::parse($n->data_novidade)->format('d/m/Y') }}</p>
                                <div class="col-md-6">
                                    <a href="{!!route('cms-novidades-show', $n->novidades_id)!!}" style="
                                        background-color: #003469;
                                        /* width: 105%; */
                                        /* height: 31px; */
                                        /* padding: 9px; */
                                        padding-top: 9px;
                                        color: white;
                                        /* margin: 0px; */
                                        margin-left: -15px;
                                        /* margin-right: -9px; */
                                        border-radius: 6px;
                                        padding-bottom: 9px;
                                        padding-left: 30%;
                                        padding-right: 21%;
                                        font-size: 23px;
                                        "> Editar </a>
                                </div>
                                <div class="col-md-6">
                                    <a onclick="excluir(<?php echo $n->novidades_id ?>)" style="
                                        background-color: red;
                                        /* width: 105%; */
                                        /* height: 31px; */
                                        /* padding: 9px; */
                                        padding-top: 9px;
                                        color: white;
                                        /* margin: 0px; */
                                        margin-left: -12px;
                                        /* margin-right: -9px; */
                                        border-radius: 6px;
                                        padding-bottom: 9px;
                                        padding-left: 30%;
                                        padding-right: 21%;
                                        font-size: 23px;
                                        width: 100%;
                                        "> Excluir </a>
                                </div>
                                
                            </div>
                        @endforeach
                        @else
                            <div style="background-color: blue;width: 400px;height: 400px;"></div>
                        @endif
                    </div>
				</div>
			</div>
		</div>
			@include('layouts._alerts')	
			
		</div>
	</div>	
</section>
<script>
    function excluir(id){

        var confirmando = confirm('Deseja realmente excluir esta imagem ?');

        if(confirmando == true){
            var ajax = new XMLHttpRequest();
            token = document.querySelector('meta[name="csrf-token"]').content;

            ajax.open("GET", "Musiva2020/cms/novidades/excluir/"+id, true);
            ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            ajax.send("_token="+token+"&id="+id);

            ajax.onreadystatechange = function() {
                if (ajax.status == 200) {
                    var data = ajax.responseText;
                    console.log(data);
                    window.location.reload(true);
                } else {
                    console.log('error');
                }
            }
        } else {
            console.log('request cancel');
        }
    }
    
</script>
@include('cms.includes.footer')
@endsection