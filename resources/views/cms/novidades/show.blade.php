@extends('layouts._app')

@section('content')	
@include('cms.includes.header')
<style>
	.segundaImagemClass{
		display:none;
	}
	.aparece{
		display:block;
	}
	.desaparece{
		display:none;
	}
</style>
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
			<div class="row">	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-novidades')!!}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Novidades Musiva</a>
						</li>
						<li>
							<a><i class="fa fa-angle-double-right" aria-hidden="true"></i> {!!(!isset($novidade)) ? 'Novo' : 'Editar'!!}</a>
						</li>
					</ul>
				</div>		
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
						@if(isset($novidade))
							
							<form autocomplete="on" action="{!! route('cms-novidades-update') !!}" method="post"
							enctype="multipart/form-data">
							<input type="hidden" value="{{$novidade->novidades_id}}" id="id" name="id">
			            @else
							
							<form autocomplete="off" action="{!! route('cms-novidades-store') !!}" method="post"
							enctype="multipart/form-data">
							
						@endif        		    
							{{ csrf_field() }}
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<label for="imagem_novidade">Imagem</label>                 
									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<div style="width: 150px;height: 150px;">
											@if(isset($novidade))
												<img src="<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>" style="" alt="">	
											@endif
										</div>
										
									</div>
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-row">     
				                       	<input type="file" name="imagem_novidade" id="imagem_novidade">
			            			   	<label class="error">{!!$errors->first('imagem_novidade')!!}</label>                 
					                </div>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<label for="nome">Nome Evento/Artista/Banda</label>                     
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									@if(isset($novidade))
										<div class="form-row">                    
										<input type="text" name="nome" id="nome" value="{{$novidade->nome}}">
											<label class="error">{!!$errors->first('nome')!!}</label>    
										</div>
									@else
										<div class="form-row">                    
											<input type="text" name="nome" id="nome">
											<label class="error">{!!$errors->first('nome')!!}</label>    
										</div>
									@endif
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<label for="imagem_novidade">Data</label>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									@if(isset($novidade))
										<div class="form-row">                    
										<input type="date" name="data_novidade" id="data_novidade" value="{{$novidade->data_novidade}}">
											<label class="error">{!!$errors->first('data_novidade')!!}</label>                    
										</div>
									@else
										<div class="form-row">                    
											<input type="date" name="data_novidade" id="data_novidade">
											<label class="error">{!!$errors->first('data_novidade')!!}</label>                    
										</div>
									@endif
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<label for="link_playlist">Link da playlist</label>                     
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									@if(isset($novidade))
										<div class="form-row">                    
										<input type="text" name="link_playlist" id="link_playlist" value="{{$novidade->link_playlist}}">
											<label class="error">{!!$errors->first('link_playlist')!!}</label>    
										</div>
									@else
										<div class="form-row">                    
											<input type="text" name="link_playlist" id="link_playlist">
											<label class="error">{!!$errors->first('link_playlist')!!}</label>    
										</div>
									@endif
								</div>

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<label for="link_compra_ingresso">Link para compra de ingresso</label>                     
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									@if(isset($novidade))
										<div class="form-row">                    
										<input type="text" name="link_compra_ingresso" id="link_compra_ingresso" value="{{$novidade->link_compra_ingresso}}">
											<label class="error">{!!$errors->first('link_compra_ingresso')!!}</label>    
										</div>
									@else
										<div class="form-row">                    
											<input type="text" name="link_compra_ingresso" id="link_compra_ingresso">
											<label class="error">{!!$errors->first('link_compra_ingresso')!!}</label>    
										</div>
									@endif
								</div>

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<label for="principal">Postagem Principal? Imagem principal em resolução 1920 X 1080 px</label>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									@if(isset($novidade))
										<div class="form-row">
											<select class="selectPrincipal" name="principal" id="principal">
												@if($novidade->principal == 'N')
													<option value="N" selected>Não</option>
													<option value="S">Sim</option>
												@else 
													<option value="S" selected>Sim</option>
													<option value="N">Não</option>
												@endif
											</select>
											<label class="error">{!!$errors->first('principal')!!}</label>    
										</div>
									@else 
										<div class="form-row">
											<select class="selectPrincipal" name="principal" id="principal">
												<option value="">Selecione</option>
												<option value="S">Sim</option>
												<option value="N">Não</option>
											</select>
											<label class="error">{!!$errors->first('principal')!!}</label>    
										</div>
									@endif
								</div>
								<div class="segundaImagemClass">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="form-row">
											<label for="segunda_imagem">Imagem de miniatura para a página principal</label>                 
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										@if(isset($novidade))
											@if($novidade->principal == 'S')
												<div class="form-row">
													<div style="width: 150px;height: 150px;">
														@if(isset($segunda_imagem))
														<img src="<?php echo asset('storage/app/public/'.$segunda_imagem[0]->imagem)?>" style="" alt="">	
														@endif
													</div>
												</div>
												<div class="form-row">     
													<input type="file" name="segunda_imagem" id="segunda_imagem">
													<label class="error">{!!$errors->first('segunda_imagem')!!}</label>                 
												</div>
											@else
												<div class="form-row">     
													<input type="file" name="segunda_imagem" id="segunda_imagem">
													<label class="error">{!!$errors->first('segunda_imagem')!!}</label>                 
												</div>
											@endif
										@else 
											<div class="form-row">     
												<input type="file" name="segunda_imagem" id="segunda_imagem">
												<label class="error">{!!$errors->first('segunda_imagem')!!}</label>                 
											</div>
										@endif
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="form-row">
											<label for="nome_musica">Nome da música</label>                     
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										@if(isset($novidade))
											<div class="form-row">                    
											<input type="text" name="nome_musica" id="nome_musica" value="{{$novidade->nome_musica}}">
												<label class="error">{!!$errors->first('nome_musica')!!}</label>    
											</div>
										@else 
											<div class="form-row">                    
												<input type="text" name="nome_musica" id="nome_musica">
												<label class="error">{!!$errors->first('nome_musica')!!}</label>    
											</div>
										@endif
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="form-row">
											<label for="audio">Audio</label>                     
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										@if(isset($novidade))
											<div class="form-row">                    
												<audio src="<?php echo asset($novidade->audio);?>" controls>
													
												</audio>
												<label class="error">{!!$errors->first('audio')!!}</label>    
											</div>
											<div class="form-row">                    
												<input type="file" name="audio" id="audio">
												<label class="error">{!!$errors->first('audio')!!}</label>    
											</div>
										@else 
											<div class="form-row">                    
												<input type="file" name="audio" id="audio">
												<label class="error">{!!$errors->first('audio')!!}</label>    
											</div>
										@endif
									</div>
								</div>

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<label for="color_botao" style="position: absolute;z-index: 7;border: 5px;border-color: 5px solid black;color: white;font-size: 22px;left: 97px;top: 57px;">[Customizar a cor do botão para compra de ingresso]</label>
										                      
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									@if(isset($novidade))
										<div class="form-row">                    
											<input type="color" name="color_botao" id="color_botao" value="{{$novidade->color_botao}}" style="border: none;width: 100%;height: 104px;">
											<label class="error">{!!$errors->first('color_botao')!!}</label>                     
										</div>
									@else 
										<div class="form-row">                    
											<input type="color" name="color_botao" id="color_botao" style="border: none;width: 100%;height: 104px;">
											<label class="error">{!!$errors->first('color_botao')!!}</label>                     
										</div>
									@endif
								</div>		
								
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-row">
										<label for="color_fonte" style="position: absolute;z-index: 7;border: 5px;border-color: 5px solid black;color: white;font-size: 22px;left: 54px;top: 57px;">[Customizar a cor da fonte do botão para compra de ingresso]</label>					
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									@if(isset($novidade))
										<div class="form-row">                    
										<input type="color" name="color_fonte" id="color_fonte" value="{{$novidade->color_fonte}}" style="border: none;width: 100%;height: 104px;">
											<label class="error">{!!$errors->first('color_fonte')!!}</label>                     
										</div>
									@else 
										<div class="form-row">                    
											<input type="color" name="color_fonte" id="color_fonte" style="border: none;width: 100%;height: 104px;">
											<label class="error">{!!$errors->first('color_fonte')!!}</label>                     
										</div>
									@endif
								</div>		
							</div>
							
							<div class="row">
								<div class="col-lg-12">
									<div class="form-row">										
										<button class="pull-right btn-primary">Salvar <i class="fa fa-check-square" aria-hidden="true"></i></button>
									</div>
								</div>
							</div>	           	            
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>
@include('cms.includes.footer')
@endsection