@extends('layouts._app')

@section('content')
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-news')!!}"><i class="fa fa-angle-double-right"
									aria-hidden="true"></i> adicionar ministração</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
						{!! Form::open(['method' => 'get', 'autocomplete' => 'on', 'route' => ['cms-community']]) !!}
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('title', 'Titulo')!!}
									{!!Form::text('title') !!}
									<label class="error">{!!$errors->first('title')!!}</label>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('status', 'Status')!!}
									{!!Form::select('status', ['1' => 'Ativo', '2' => 'Inativo'], null, ['placeholder'
									=> "Selecione"]) !!}
									<label class="error">{!!$errors->first('status')!!}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-row">
									@shield('cms-news-create')
									<a href="{!!route('cms-ministry-create')!!}" class="pull-right btn-quartenary">Novo
										<i class="fa fa-plus-circle" aria-hidden="true"></i></a>
									@endshield
									<button class="pull-right btn-primary">Buscar <i class="fa fa-search"
											aria-hidden="true"></i></button>
								</div>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
			@include('layouts._alerts')
			<div class="row">
				@if(session()->has('message'))
				<div class="alert alert-success">
					{{ session()->get('message') }}
				</div>
				@endif
				@if(session()->has('message-update'))
				<div class="alert alert-success">
					{{ session()->get('message-update') }}
				</div>
				@endif
				<div class="col-lg-12">
					<div class="panel-default list">
						<table>
							<thead>
								<tr>
									<th>Id</th>
									<th>Ministração</th>
									<th>Grupo</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody>
							@foreach($ministry as $m)
                                <tr>
                                    <td>{{ $m->ministry_id }}</td>
                                    <td>{{ $m->name }}</td>
                                    <td>{{ $m->nome_grupo }}</td>
                                    <td>
										<a href="{!!route('cms-ministry-delete', $m->ministry_id)!!}" title="Excluir" class="btn-primary"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                                        <a href="{!!route('cms-ministry-show', $m->ministry_id)!!}" title="Editar" class="btn-secondary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									</td>
                                </tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">

			</div>
		</div>
	</div>
</section>
@include('cms.includes.footer')
@endsection