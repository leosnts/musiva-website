@extends('layouts._app')

@section('content')
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-ministry')!!}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ministrações</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
						{!! Form::open(['method' => 'put', 'autocomplete' => 'off', 'route' => ['cms-ministry-update', $ministry->ministry_id]]) !!}
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-row">
                                    <label for="nome">Nome</label>
			            			<input type="text" name="name" value="{{ $ministry->name }}">	
									<label class="error">{!!$errors->first('name')!!}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12col-xs-12">
								<div class="form-row">
									<label for="">Selecione o grupo</label>
									<select name="group_id" id="">
										<option value="{{ $ministry->group_ministry_id }}">Selecione</option>
                                        @foreach($groupMinistry as $gm)
										<option value="{{ $gm->group_ministry_id }}">{{ $gm->name }}</option>
                                        @endforeach
									</select>
									<label class="error">{!!$errors->first('')!!}</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-row">
									{!!Form::label('content', 'Conteudo')!!}
									{!!Form::textarea('content', $ministry->content) !!}
									@ckeditor('content')
									<label class="error">{!!$errors->first('content')!!}</label>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
								<div class="form-row">
									<button class="pull-right btn-primary">Salvar <i class="fa fa-check-square"
											aria-hidden="true"></i></button>
								</div>
							</div>
						</div>
			
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('cms.includes.footer')
@endsection