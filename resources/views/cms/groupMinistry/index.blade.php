@extends('layouts._app')

@section('content')
@include('cms.includes.header')
<section class="wrapper">
	@include('cms.includes.sidebar')
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="breadcrumb">
						<li>
							<a href="{!!route('cms-home')!!}"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
						</li>
						<li>
							<a href="{!!route('cms-group-ministry')!!}"><i class="fa fa-angle-double-right"
									aria-hidden="true"></i> adicionar grupos</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel-default">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-row">
									@shield('cms-news-create')
									<a href="{!!route('cms-groupMinistry-create')!!}" class="pull-right btn-quartenary">Novo
										<i class="fa fa-plus-circle" aria-hidden="true"></i></a>
									@endshield
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@include('layouts._alerts')
			<div class="row">
				@if(session()->has('message'))
				<div class="alert alert-success">
					{{ session()->get('message') }}
				</div>
				@endif

				@if(session()->has('message-delete'))
				<div class="alert alert-success">
					{{ session()->get('message-delete') }}
				</div>
				@endif
				<div class="col-lg-12">
					<div class="panel-default list">
						<table>
							<thead>
								<tr>
									<th>Id</th>
									<th>Group Ministração</th>
									<th>Ações</th>
								</tr>
							</thead>
                            <tbody>
                                @foreach($groupMinistry as $gM)
                                    <tr>
                                        <td>{{ $gM->group_ministry_id }}</td>
                                        <td> {{ $gM->name }} </td>
                                        <td> 
											<a href="{!!route('cms-groupMinistry-delete', $gM->group_ministry_id)!!}" title="Excluir" class="btn-primary"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                                            <a href="{!!route('cms-groupMinistry-show', $gM->group_ministry_id)!!}" title="Editar" class="btn-secondary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">

			</div>
		</div>
	</div>
</section>
@include('cms.includes.footer')
@endsection