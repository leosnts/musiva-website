@extends('site.layout.app', ["titulo" => "Musiva"])
@section('body')
<style>

.lds-ripple div:nth-child(2) {
  animation-delay: -0.5s;
}
@keyframes lds-ripple {
  0% {
    top: 36px;
    left: 36px;
    width: 0;
    height: 0;
    opacity: 1;
  }
  100% {
    top: 0px;
    left: 0px;
    width: 100px;
    height: 100px;
    opacity: 0;
  }
}
@media(max-width: 2560px){
    .lds-ripple {
        display: inline-block;
        position: relative;
        width: 80px;
        left: 351px;
        height: 80px;
    }
    .lds-ripple div {
        position: absolute;
        border: 4px solid #fff;
        opacity: 1;
        border-radius: 50%;
        animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
    }
    .pai .inner img {
        width: 0.5%;
        margin-left: 291px;
        height: 0.13%;
        /* margin-left: -0.16%; */
        margin-top: 0.1%;
        position: absolute;
    }
}
@media(max-width: 1920px){
    .lds-ripple {
    display: inline-block;
    position: relative;
    left: -8px;
    width: 80px;
    height: 80px;
}
    .lds-ripple div {
        position: absolute;
        border: 4px solid #fff;
        opacity: 1;
        border-radius: 50%;
        animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
    }
    .pai .inner img {
        width: 0.5%;
        margin-left: -65px;
        height: 0.13%;
        /* margin-left: -0.16%; */
        margin-top: 0.1%;
        position: absolute;
    }
    
}
@media(max-width: 1440px){
    .pai .inner img {
        width: 0.5%;
        margin-left: -303px;
        height: 0.13%;
        /* margin-left: -0.16%; */
        margin-top: 0.1%;
        position: absolute;
    }
    .lds-ripple {
        display: inline-block;
        position: relative;
        width: 80px;
        left: -246px;
        height: 80px;
    }
}
@media(max-width: 1024px){
    .pai .inner img {
        width: 0.5%;
        margin-left: -508px;
        height: 0.13%;
        /* margin-left: -0.16%; */
        margin-top: 24px;
        position: absolute;
    }
    .lds-ripple {
        display: inline-block;
        position: relative;
        width: 80px;
        left: -447px;
        height: 80px;
    }
}
@media(max-width: 768px){
    .pai .inner img {
        width: 1.3%;
        margin-left: -793px;
        height: 0.33%;
        /* margin-left: -0.16%; */
        margin-top: 24px;
        position: absolute;
    }
    .lds-ripple {
        display: inline-block;
        position: relative;
        width: 80px;
        top: 29px;
        left: -566px;
        height: 80px;
    }
    @keyframes lds-ripple {
        0% {
            top: 36px;
            left: 36px;
            width: 0;
            height: 0;
            opacity: 1;
        }
        100% {
            top: 0px;
            left: 0px;
            width: 100px;
            height: 100px;
            opacity: 0;
        }
    }
}
@media(max-width: 425px){
    .pai .inner img {
        width: 0.4%;
        margin-left: -793px;
        height: 0.12%;
        /* margin-left: -0.16%; */
        margin-top: 24px;
        position: absolute;
    }
    .lds-ripple {
        display: inline-block;
        position: relative;
        width: 80px;
        top: -22px;
        left: -752px;
        height: 80px;
    }
    @keyframes lds-ripple {
        0% {
            top: 36px;
            left: 36px;
            width: 0;
            height: 0;
            opacity: 1;
        }
        100% {
            top: 0px;
            left: 0px;
            width: 80px;
            height: 80px;
            opacity: 0;
        }
    }
    iframe {
        border: 0;
        margin-left: 2px;
        width: 100%;
        height: 289px;
    }
    .textoHistoria {
        color: white;
        text-align: left;
        font-size: 13px;
    }
    input[id='subject'] {
        border-radius: 0px;
        border-radius: 0px;
        -moz-border-radius: 0px;
        -webkit-border-radius: 0px;
        /* box-shadow: 1px 1px 2px #333333; */
        /* -moz-box-shadow: 1px 1px 2px #333333; */
        /* -webkit-box-shadow: 1px 1px 2px #333333; */
        background: none;
        border: 2px solid #cccccc;
        width: 300px;
        height: 68px;
        /* align-items: center; */
        /* position: absolute; */
        /* left: 113px; */
        /* top: 113px; */
        color: white;
        margin-left: -59px;
        margin-top: 152px;
    }
    input[id='message'] {
        border-radius: 0px;
        border-radius: 0px;
        -moz-border-radius: 0px;
        -webkit-border-radius: 0px;
        /* box-shadow: 1px 1px 2px #333333; */
        /* -moz-box-shadow: 1px 1px 2px #333333; */
        /* -webkit-box-shadow: 1px 1px 2px #333333; */
        background: none;
        border: 2px solid #cccccc;
        width: 300px;
        height: 68px;
        /* align-items: center; */
        /* position: absolute; */
        /* left: 113px; */
        /* top: 113px; */
        color: white;
        margin-left: -59px;
        margin-top: 222px;
    }
    input[id='btnContato'] {
        border-radius: 0px;
        border-radius: 0px;
        -moz-border-radius: 0px;
        -webkit-border-radius: 0px;
        /* box-shadow: 1px 1px 2px #333333; */
        /* -moz-box-shadow: 1px 1px 2px #333333; */
        /* -webkit-box-shadow: 1px 1px 2px #333333; */
        background: none;
        border: 3px solid #cccccc;
        width: 298px;
        height: 68px;
        /* align-items: center; */
        /* position: absolute; */
        /* left: 113px; */
        /* top: 113px; */
        color: white;
        margin-top: -25px;
        margin-left: -34px;
    }
}
@media(max-width: 375px){
    .pai .inner img {
        width: 0.4%;
        margin-left: -811px;
        height: 0.12%;
        /* margin-left: -0.16%; */
        margin-top: -49px;
        position: absolute;
    }
    .lds-ripple {
        display: inline-block;
        position: relative;
        width: 80px;
        top: -85px;
        left: -771px;
        height: 80px;
    }
    .textoHistoria {
        color: white;
        text-align: left;
        font-size: 10px;
    }
    input[id='subject'] {
        border-radius: 0px;
        border-radius: 0px;
        -moz-border-radius: 0px;
        -webkit-border-radius: 0px;
        /* box-shadow: 1px 1px 2px #333333; */
        /* -moz-box-shadow: 1px 1px 2px #333333; */
        /* -webkit-box-shadow: 1px 1px 2px #333333; */
        background: none;
        border: 2px solid #cccccc;
        width: 300px;
        height: 68px;
        /* align-items: center; */
        /* position: absolute; */
        /* left: 113px; */
        /* top: 113px; */
        color: white;
        margin-left: -114px;
        margin-top: 110px;
    }
    input[id='message'] {
        border-radius: 0px;
        border-radius: 0px;
        -moz-border-radius: 0px;
        -webkit-border-radius: 0px;
        /* box-shadow: 1px 1px 2px #333333; */
        /* -moz-box-shadow: 1px 1px 2px #333333; */
        /* -webkit-box-shadow: 1px 1px 2px #333333; */
        background: none;
        border: 2px solid #cccccc;
        width: 300px;
        height: 68px;
        /* align-items: center; */
        /* position: absolute; */
        /* left: 113px; */
        /* top: 113px; */
        color: white;
        margin-left: -114px;
        margin-top: 168px;
    }
    input[id='btnContato'] {
        border-radius: 0px;
        border-radius: 0px;
        -moz-border-radius: 0px;
        -webkit-border-radius: 0px;
        /* box-shadow: 1px 1px 2px #333333; */
        /* -moz-box-shadow: 1px 1px 2px #333333; */
        /* -webkit-box-shadow: 1px 1px 2px #333333; */
        background: none;
        border: 3px solid #cccccc;
        width: 298px;
        height: 68px;
        /* align-items: center; */
        /* position: absolute; */
        /* left: 113px; */
        /* top: 113px; */
        color: white;
        margin-top: 0px;
        margin-left: -83px;
    }
}
@media(max-width: 320px){
    .pai .inner img {
        width: 0.4%;
        margin-left: -836px;
        height: 0.12%;
        /* margin-left: -0.16%; */
        margin-top: 24px;
        position: absolute;
    }
    .lds-ripple {
        display: inline-block;
        position: relative;
        width: 80px;
        top: -16px;
        left: -796px;
        height: 80px;
    }
}
</style>

{{-- MENU RESPONSIVO --}}
{{-- <div class="pai" style="background-color: black;">
    <div class="inner" style="height: 40000px;
    width: 40000px;margin-top: 0.8%;
    margin-left: 2.3%;">
    {!!img('logoMusivaBranca.png')!!}
    <br>
        <div class="lds-ripple"><div></div><div></div></div>
    </div>
</div> --}}

{{-- INICIO CABECALHO --}}
{{-- <header class="cabecalho" style="background-color: black"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"> --}}
{{-- <div class="espaco">
        <div class="margin-div icone-config menu-icone">
            
            <i class="zmdi zmdi-menu" id="iconeMenu"></i>
        </div>
        <div class="imagem">
            {!!img('LogoMusiva.png')!!}
        </div>
    </div>
=======

    {{-- INICIO CABECALHO --}}

{{-- </header> --}}
{{-- FIM CABECALHO --}}

{{-- INICIO CONTEUDO DO CORPO DO SITE --}}
{{-- <section id="conteudo" style="background-color: blue"> --}}
{{-- <div class="espaco">
        <div class="margin-div icone-config menu-icone">
            
            <i class="zmdi zmdi-menu" id="iconeMenu"></i>
        </div>
        <div class="imagem">
            {!!img('LogoMusiva.png')!!}
        </div>
        
    </div> --}}
{{-- </section> --}}
{{-- FIM CONTEUDO --}}
{{-- <div id="barra" class="barra-none">
        <div class="circulo">
                <i class="zmdi zmdi-menu" id="icone-movel"></i>
        </div>
    </div> --}}
<div class="grupo-menu-corpo">
    <ul class="menu-corpo">
        <li class="menu-nossa-historia"><a href="#proximaExperiencia">Próxima experiência</a></li>
        <li class="menu-nossa-historia"><a href="#reservas">Reservas</a></li>
        <li class="menu-nossa-historia"><a href="#passaram">Passaram pela musiva</a></li>
        <li class="menu-nossa-historia"><a href="#nossaHistoria">Nossa história</a></li>
    </ul>

    <div class="botao-fechar-menu"><i class="fa fa-times" id="fechar-menu" aria-hidden="true"></i></div>
</div>
{{-- <div class="menu">
    <div class="contorno-icone">
        <i class="zmdi zmdi-menu" id="icone"></i>
    </div>
    {!!img('LogoMusiva.png')!!}
</div> --}}
<div id="barra-menu-superior">
    <div id="botao-menu-musiva">
        <i class="zmdi zmdi-menu" id="icone-movel"></i>
        <i class="zmdi zmdi-close" id="fechar" style="display: none;color: white;
        font-size: 44px;
        margin-top: 1px;
        margin-left: 9px;"></i>
    </div>
    <div id="bola-maior">
        
    </div>
    <div id="menuWeb">
        <a href="#homeWeb">
        <div id="menu-bolas-menores">
        </div>
        </a>
        <a href="#formularioWeb">
                <div id="menu-bolas-menores2">
                    </div>
        </a>
            
            <div id="menu-bolas-menores3">
            </div>
            <div id="menu-bolas-menores4">
            </div>
    </div>
    
    {{-- <ul id="playlist">
        <li class="current-song"><a href="public/img/50_cent.mp3">mp3</a></li>
        <li><a href="public/img/50_cent.mp3">mp4</a></li>
        <li><a href="public/img/eminem.mp3">mplegal</a></li>
    </ul> --}}
</div>
<div id="layout">
    {{-- <div class="grupo-menu-corpo">
            <ul class="menu-corpo">
                <li class="menu-nossa-historia"><a href="#proximaExperiencia">Próxima experiência</a></li>
                <li class="menu-nossa-historia"><a href="#reservas">Reservas</a></li>
                <li class="menu-nossa-historia"><a href="#passaram">Passaram pela musiva</a></li>
                <li class="menu-nossa-historia"><a href="#nossaHistoria">Nossa história</a></li>
            </ul>

            <div class="botao-fechar-menu"><i class="fa fa-times" id="fechar-menu" aria-hidden="true"></i></div>
        </div> --}}
   
    <div class="inicio" id="homeWeb">
        @if(isset($novidades))
            @foreach ($novidades as $novidade)
                
                @if ($novidade->principal == 'S')
                    <div id="div-enfeite">
                            {{-- <p class="ingressosOnline">INGRESSOS ON-LINE</p> --}}
                            <p class="data-atracao-principal">{{ Carbon\Carbon::parse($novidade->data_novidade)->format('d/m') }} <i class="zmdi zmdi-calendar"></i></p>
                    <div onclick="window.open('{{ $novidade->link_compra_ingresso }}')" class="botao-ingresso-principal" style="background-color: <?php echo $novidade->color_botao;?>;color: <?php echo $novidade->color_fonte;?>;"><a style="text-decoration: none;color: {{$novidade->color_fonte}}"> INGRESSOS ON-LINE </a><i class="fa fa-ticket" aria-hidden="true"></i></div>
                    </div>
                    <img class="imagem-principal" src="<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>" alt="">
                    
                @endif
            @endforeach
        @else
        @endif
    </div>
    <div class="div-enfeite2"></div>
    <div class="parte1">
        <img class="segunda-imagem" src="{{ asset('public/img/banner2.png') }}" alt="">
        <div class="divc" id="fds">
            <p class="textoc" id="c">ENCONTRE SUA PRÓXIMA EXPERIÊNCIA.</p>
        </div>
        <div class="c" id="lista">
            @if (isset($novidades))
                @if (count($novidades) == 1)
                    @foreach($novidades as $novidade)
                    @if ($novidade->principal == 'N')
                        <a onclick="comprar('<?php echo $novidade->link_compra_ingresso;?>')">
                            <div id="atracoes-tela-cheia" style="float: right">
                                <p class="data-atracao">{{ Carbon\Carbon::parse($novidade->data_novidade)->format('d/m') }}</p>
                                <img src="<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>">
                                <p class="botao-ingresso" style="background-color: <?php echo $novidade->color_botao;?>;color: <?php echo $novidade->color_fonte;?>;linear-gradient(0deg, <?php echo $novidade->color_botao;?> 45%, rgba(255,255,255,0) 100%);">Ingressos On-Line!</p>
                            </div>    
                        </a>
                    @endif
                    @endforeach
                @endif
                @if (count($novidades) > 1)
                    @foreach($novidades as $novidade)
                    @if ($novidade->principal == 'N')
                        <a onclick="comprar('<?php echo $novidade->link_compra_ingresso;?>')">
                            <div id="atracoes" style="float: left">
                                <p class="data-atracao">{{ Carbon\Carbon::parse($novidade->data_novidade)->format('d/m') }}</p>
                                <p class="btt" style="background-color: <?php echo $novidade->color_botao;?>;color: <?php echo $novidade->color_fonte;?>;">Ingressos On-Line!</p>
                                <img src="<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>">
                                {{-- <div class="div-botao-customizavel">
    
                                </div> --}}
                            {{-- <a class="botao-customizavel" style="background-color: {{$novidade->color_botao}}" href="">COMPRAR</a> --}}
                            </div>
                        </a>
                    @endif
                    @endforeach    
                @endif
            @else
                
            @endif
            
            {{-- <div id="atracoes" style="float: left">{!!img('fotoLargadoAsTracas.png')!!}</div>
            <div id="atracoes" style="float: right">{!!img('tirulipa.png')!!}</div>
            <div id="atracoes" style="float: left">{!!img('simoneEsimaria.png')!!}</div>
            <div id="atracoes" style="float: right">{!!img('imagemMusiva.png')!!}</div>
            <div id="atracoes" style="float: left">{!!img('simoneEsimaria.png')!!}</div>
            <div id="atracoes" style="float: right">{!!img('imagemMusiva.png')!!}</div>
            <div id="atracoes" style="float: left">{!!img('fotoLargadoAsTracas.png')!!}</div>
            <div id="atracoes" style="float: right">{!!img('tirulipa.png')!!}</div>
            <div id="atracoes" style="float: left">{!!img('simoneEsimaria.png')!!}</div>
            <div id="atracoes" style="float: right">{!!img('imagemMusiva.png')!!}</div>
            <div id="atracoes" style="float: left">{!!img('simoneEsimaria.png')!!}</div>
            <div id="atracoes" style="float: right">{!!img('imagemMusiva.png')!!}</div> --}}
        </div>
        @if (isset($novidades))
            @if (count($novidades) > 4)
                <div id="botoes">
                    <ul class="fundoBotoes">
                        {{-- <li><i class="seta_cima"></i></li> --}}
                        <li><i class="fa fa-caret-up" aria-hidden="true"></i></li>
                        {{-- <li><i class="seta_baixo"></i></li> --}}
                        <li><i class="fa fa-caret-down" aria-hidden="true"></i></li>
                    </ul>
                </div>
            @else
                
            @endif
        @endif
        
        <div class="formulario">
            <p class="textoFormularioTitulo" id="reservas">RESERVAS</p>
            <p class="textoFormularioFrase1">Quer alugar a melhor casa de show de Mato Grosso para o seu evento?</p>
            <p class="textoFormularioFrase2">Entre em contato conosco e agenda sua falta!</p>
        <form action="{{ route('reserva.novo') }}" method="post" onsubmit="return validaForm(this);">
                {{ csrf_field() }}
                {{-- <input type="text" name="nome" id="nome" placeholder="NOME" required  oninvalid="setCustomValidity('Por favor, preencha o nome')" onchange="try{setCustomValidity('')}catch(e){}" > --}}
                <input type="text" name="nome" id="nome" placeholder="NOME">
                <input type="email" name="email" id="email" placeholder="E-MAIL">
                <input type="number" name="n_pessoas" id="n_pessoas" placeholder="NÚMERO DE RESERVAS" min="10">
                <input type="date" name="data_reserva" id="data_reserva" placeholder="DD/MM/AAAA">
                <input type="text" name="telefone" id="telefone" placeholder="TELEFONE">
                <input type="SUBMIT" name="reservas" value="RESERVAS" id="reservas">
            </form>
            <p class="textoEntreEmContato">ENTRE EM CONTATO</p>
        </div>

        <div class="info-contato">
            <div class="telefone">
                {!!img('telefone.png')!!}
                <p class="TextoTelefoneContatoTitulo">Telefone</p>
                <p class="TextoTelefoneContatoNumero">(65)9 3632-7777</p>
            </div>
            <div class="localizacao">
                {!!img('localizacao.png')!!}
                <p class="TextoLocalizacaoContatoTitulo">Endereço</p>
                <p class="TextoLocalizacaoContatoNumero">Av. Manoel José de Arruda, 4435 Porto, Cuiabá - MT, 78055-498
                </p>
            </div>
            <div class="wpp">
                {!!img('wpp.png')!!}
                <p class="TextoWppContatoTitulo">WhatsApp</p>
                <p class="TextoWppContatoNumero">(65) 9 9332-7777</p>
            </div>
        </div>

        <div class="divMusivaHistorico">
            <p class="textoMusivaHistorico" id="passaram">JÁ PASSARAM PELA MUSIVA.</p>
        </div>
        <div class="row d" id="listaRow" style="width: 1017px;">
            @if (isset($portfolios))
                @if (count($portfolios) == 1)
                    @foreach($portfolios as $portfolio)
                        <div id="atracoes-passadas-tela-cheia" style="float: right"><img src="<?php echo asset('storage/app/public/'.$portfolio->imagem_portfolio)?>"></div>    
                    @endforeach
                @endif
                @if (count($portfolios) > 1)
                    @foreach($portfolios as $portfolio)
                        <div id="atracoesPassadas" style="float: left">
                            <img src="<?php echo asset('storage/app/public/'.$portfolio->imagem_portfolio)?>">
                        </div>
                    @endforeach    
                @endif
            @else
                
            @endif
        </div>
            
            {{-- foreach do banco, caso o valor da index do array somado com +1. se for par:float left, se for impar:float rigth --}}
            {{-- <div id="atracoesPassadas" style="float: left">{!!img('simoneEsimaria.png')!!}</div>
                        <div id="atracoesPassadas" style="float: right">{!!img('imagemMusiva.png')!!}</div> --}}
        

        @if (isset($portfolios))
            @if (count($portfolios) > 6)
                <div id="botoes2">
                    <ul class="fundoBotoes">
                        {{-- <li><i class="seta_cima"></i></li> --}}
                        <li><i class="fa fa-caret-up" aria-hidden="true"></i></li>
                        {{-- <li><i class="seta_baixo"></i></li> --}}
                        <li><i class="fa fa-caret-down" aria-hidden="true"></i></li>
                    </ul>
                </div>
            @endif
        @endif
        <div class="DivHistoriaMusiva">
            <p class="textoHistoriaMusiva" id="nossaHistoria">NOSSA HISTÓRIA</p>    ]
        </div>
        
        <div class=" inauguracao-button1-ativado" id="inauguracao-button1">
            <i class="fa fa-circle" aria-hidden="true"></i>
            {!!img('m_da_musiva.png')!!}
        </div>
        <div class=" inauguracao-button2" id="inauguracao-button2">
            <i class="fa fa-circle" aria-hidden="true"></i>
            {!!img('m_da_musiva.png')!!}
        </div>
        <div class=" inauguracao-button3" id="inauguracao-button3">
            <i class="fa fa-circle" aria-hidden="true"></i>
            {!!img('m_da_musiva.png')!!}
        </div>
        <div class="inauguracao-button4"  id="inauguracao-button4">
            <i class="fa fa-circle" aria-hidden="true"></i>
            {!!img('m_da_musiva.png')!!}
        </div>
        <div class=" inauguracao-button5"  id="inauguracao-button5">
            <i class="fa fa-circle" aria-hidden="true"></i>
            {!!img('m_da_musiva.png')!!}
        </div>
        <div class="reta1"></div>
        <div class="reta2"></div>
        <div class="reta3"></div>
        <div class="reta4"></div>
        <div class="reta5"></div>
        <div class="reta6"></div>
        <p class="texto-inaugurarao-button1" id="botaoHistoria1">Inauguração / 2009</p>
        <p class="texto-inaugurarao-button2" id="botaoHistoria2">Inauguração / 2009</p>
        <p class="texto-inaugurarao-button3" id="botaoHistoria3">Inauguração / 2009</p>
        <p class="texto-inaugurarao-button4" id="botaoHistoria4">Inauguração / 2009</p>
        <p class="texto-inaugurarao-button5" id="botaoHistoria5">Inauguração / 2009</p>
        <div class="conteudo1" id="conteudo1" style="display: block;">
            <p>Build responsive, mobile-first projects on the web
                with the world’s most popular front-end component library.
                Bootstrap is an open source toolkit for developing with HTML, CSS, and JS.
                Quickly prototype your ideas or build your entire app with our Sass variables and mixins,
                responsive grid system, extensive prebuilt components, and powerful plugins built on jQuery.</p>
        </div>
        <div class="conteudo1" id="conteudo2" style="display: none;">
            <p>Build responsive, mobile-first projects on the web with the world’s most
                popular front-end component library.

                Bootstrap is an open source toolkit for developing with HTML, CSS, and JS. Quickly prototype your
                ideas or build your entire app with our Sass variables and mixins.</p>
        </div>
        <div class="conteudo1" id="conteudo3" style="display: none;">
            <p>Build responsive, mobile-first projects on the web with the world’s most popular
                front-end component library.

                Bootstrap is an open source toolkit for developing with HTML, CSS, and JS. Quickly prototype your
                ideas</p>
        </div>
        <div class="conteudo1" id="conteudo4" style="display: none;">
            <p>Build responsive, mobile-first projects on the web with the world’s most
                popular front-end component library.

                Bootstrap is an open source toolkit for developing with HTML, CSS, and JS. Quickly prototype your
                ideas or build your entire app with our Sass variables and mixins, responsive grid system, extensive
                prebuilt components</p>
        </div>
        <div class="conteudo1" id="conteudo5" style="display: none;">
            <p>Build responsive, mobile-first projects on the web with the world’s most
                popular front-end component library.</p>
        </div>

    </div>

    <div class="como-chegar">
        {!!img('logoMusivaBranca.png')!!}

        <div class="divTituloMapa">
            <p class="texto-mapa" id="frase-mapa">O local de fortes emoções se encontra aqui!</p>
        </div>
        <div class="iframestyle">
            <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3842.471913368409!2d-56.10003228460392!3d-15.619838922848185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x939db1e223d840e3%3A0xbd212d145772d9c5!2sMusiva!5e0!3m2!1spt-BR!2sbr!4v1572372775687!5m2!1spt-BR!2sbr"
            width="600" height="450" frameborder="0" style="border: 0;margin-left: 0px;width: 100%;height: 100%;"
            allowfullscreen=""></iframe>
        </div>
        
        <div class="traco"></div>
        <div class="msg-como-chegar">
            <p class="texto-mapa-inferior">Como chegar?</p>
        </div>
    </div>

</div>

<div id="layoutResponsivo">

    <div id="barra-menu-superior-responsivo2">
        
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="#">{!!img('logoMusivaBranca.png')!!}</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
            <span class="navbar-toggler-icon"></span>
          </button>
        
          <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" onclick="$('#carouselExampleSlidesOnly').animatescroll();">HOME <span class="sr-only">(página atual)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" onclick="$('#novidadesScrollAnimate').animatescroll();">EVENTOS</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" onclick="$('#idReservasScrollResponsivo').animatescroll();">RESERVA</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" onclick="$('#divPortfolioResponsivo').animatescroll();">JÁ PASSARAM POR AQUI</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" onclick="$('#nossaHistoriaScrollResponsivo').animatescroll();">A MUSIVA</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" onclick="$('#contatoScrollResponsivo').animatescroll();">CONTATO</a>
              </li>
            </ul>
          </div>
        </nav>
    </div>
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                @if(isset($novidades))
                    @foreach ($novidades as $novidade)
                        
                        @if ($novidade->principal == 'S')
                            <img class="imagem-principal-responsivo" src="<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>" alt="">
                            <a href="<?php echo $novidade->link_compra_ingresso;?>">
                            <div class="botaoResponsivoComprarIngresso2" style="background-color: <?php echo $novidade->color_botao;?>">
                                <p class="tituloCompreResponsivo2" style="color: <?php echo $novidade->color_fonte;?>">COMPRE SEU INGRESSO</p>
                            </div>
                            </a>
                        @endif
                    @endforeach
                @else
                @endif
            
            </div>
        </div>
    </div>
    <div style="width: 100%;" id="novidadesScrollAnimate">
        <div class="container-fluid fluido">
            <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <p class="lead">ENCONTRE SUA PRÓXIMA EXPERIÊNCIA</p>
            </div>
            </div>
            <!--<div id="barra-um"></div>-->
        </div>
    </div>
    <div class="container-fluid justify-content-center paah">
        
        @if(isset($novidades))
            @foreach ($novidades as $novidade)
                @if ($novidade->principal != 'S')
                    <div class="card text-center gf" id="dff" style="width: 18rem;margin-bottom: 0px;">
                        <img class="card-img-top carta<?php echo $novidade->novidades_id;?>" src="<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>" alt="">
                        <a href="<?php echo $novidade->link_compra_ingresso;?>">
                        <div class="botaoResponsivoComprarIngresso" style="background-color: <?php echo $novidade->color_botao;?>;border-top: 1px solid white;">
                            <p class="tituloCompreResponsivo">INGRESSOS ONLINE</p>
                        </div>
                        </a>
                        <div class="divFundo<?php echo $novidade->novidades_id;?>" ></div>
                        <div class="card-body">
                            <h5 class="card-title">Data: {{ Carbon\Carbon::parse($novidade->data_novidade)->format('d/m') }}</h5>
                        </div>
                    </div>
                @endif
            @endforeach
        @else
        @endif
    </div>
    <div style="width: 100%;" id="idReservasScrollResponsivo">
        <div class="container-fluid fluido">
            <div class="jumbotron jumbotron-fluid">
              <div class="container">
                <p class="lead">RESERVAS</p>
              </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="formularioWeb" style="height: 42rem;">
        <form action="{{ route('reserva.novo') }}" method="post" onsubmit="return validaForm(this);">
                {{ csrf_field() }}
            <div class="classeFormResponsivo">
                  <div class="form-row">
                    <div class="form-group col-md-12 col-sm-12">
                      <input type="text" name="nome" class="form-control" id="reservaResponsivo" placeholder="Nome">
                    </div>
                    <div class="form-group col-md-12 col-sm-12">
                      <input type="email" name="email" class="form-control" id="reservaResponsivoemail" placeholder="E-mail">
                    </div>
                  </div>
                  <div class="form-row">
                      <div class="form-group col-md-12 col-sm-12">
                          <input type="number" name="n_pessoas" class="form-control" id="reservaResponsivoemail" placeholder="Número" value="10">
                        </div>
                      <div class="form-group col-md-6 col-sm-12">
                          <input type="date" name="data_reserva" class="form-control" id="reservaResponsivodata" placeholder="Data">
                        </div>
                  </div>
                    <div class="form-row">
                        <div class="form-group col-md-12 col-sm-12">
                            <input type="text" name="telefone" class="form-control" id="telefone" placeholder="Telefone">
                        </div>
                  </div>
                  <div class="form-row">
                        <div class="form-group col-md-12 col-sm-12">
                            <input type="submit" name="reservas" value="RESERVAS" id="reservas">
                        </div>
                  </div>
            </div>
        </form>
    </div>
    <div style="width: 100%;">
        <div class="container-fluid fluido">
            <div class="jumbotron jumbotron-fluid">
              <div class="container">
                <p class="lead">JÁ PASSARAM POR AQUI</p>
              </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="divPortfolioResponsivo">
        @if (isset($portfolios))
            @foreach($portfolios as $portfolio)
                <div class="imagemPortfolioResponsivo">
                    <img src="<?php echo asset('storage/app/public/'.$portfolio->imagem_portfolio)?>" alt="...">
                </div>
            @endforeach
        @else
            
        @endif
        
    </div>
    <div style="width: 100%;" id="nossaHistoriaScrollResponsivo">
        <div class="container-fluid fluido">
            <div class="jumbotron jumbotron-fluid">
              <div class="container">
                <p class="lead">NOSSA HISTÓRIA</p>
              </div>
            </div>
        </div>
    </div>
    <div class="container-fluid historiaFluindoResponsivo">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Sobre nós</a>
        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Estrutura</a>
        </div>
        <div class="tab-content" id="v-pills-tabContent">
        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
            <p class="textoHistoria">TUDO QUE MEXE COM A GENTE TEM UM M.</p> 
            <p class="textoHistoria">NA MÚSICA MARCANTE OU MANSA QUE GERA O MOVIMENTO E NA MASSA DE MILHARES DE MUITOS QUE SE FEZ DESSA MISTURA MARAVILHOSA QUE SÓ O BRASIL TEM.</p>
            <p class="textoHistoria">NOS MOTIVOS QUE MOVEM E MODIFICAM O MUNDO. EM TODA MANIFESTAÇÃO QUE QUEBRA O PENSAMENTO MONÓTONO E MUDA A MENTE, MARCO DA MORTE DO MEDO DOS NOSSOS PRÓPRIOS MONSTROS, QUE AFUGENTA A MESMICE CHATA FEITO MOSQUITO. </p>
            <p class="textoHistoria">TAMBÉM TEM M NA MÃO QUE MIMA E ACARICIA, SEJA DE MÃE, MULHER, MENINO, MOÇA OU MACHO. EM TUDO QUE É MÁGICO, MÍSTICO, MITOLÓGICO. </p>
            <p class="textoHistoria">NO MEME MOMENTÂNEO QUE VIRA MANIA OU MIMIMI. NA MODA QUE MOVIMENTA O MERCADO E FAZ SEU PRÓPRIO MARKETING.</p>
            <p class="textoHistoria">EM TUDO QUE É MÚLTIPLO, MODERNO. UM MOSAICO, UMA MANDALA PARA MONTAR DE ACORDO COM SEU MOMENTO, PARA COLOCAR O MOLHO E FAZER DELE UM MANTRA.</p>
            <p class="textoHistoria">NAQUILO QUE É MAIS. MELHOR PRA VOCÊ, MELHOR PRA MIM. </p>
            <p class="textoHistoria">MUSIVA. </p>
            <p class="textoHistoria">UM ESPAÇO DE EVENTOS COMO VOCÊ NUNCA VIU.</p>
            <p class="textoHistoria">UM LUGAR DE MISTURA E MOVIMENTO.</p>



            {{-- <p class="textoHistoria">
                TUDO QUE MEXE COM A GENTE TEM UM M.<br>
                NA MÚSICA MARCANTE OU MANSA QUE GERA O MOVIMENTO E NA MASSA DE MILHARES DE MUITOS QUE SE FEZ DESSA MISTURA MARAVILHOSA QUE SÓ O BRASIL TEM.<br>
                NOS MOTIVOS QUE MOVEM E MODIFICAM O MUNDO. EM TODA MANIFESTAÇÃO QUE QUEBRA O PENSAMENTO MONÓTONO E MUDA A MENTE, MARCO DA MORTE DO MEDO DOS NOSSOS PRÓPRIOS MONSTROS, QUE AFUGENTA A MESMICE CHATA FEITO MOSQUITO. <br>
                TAMBÉM TEM M NA MÃO QUE MIMA E ACARICIA, SEJA DE MÃE, MULHER, MENINO, MOÇA OU MACHO. EM TUDO QUE É MÁGICO, MÍSTICO, MITOLÓGICO. <br>
                NO MEME MOMENTÂNEO QUE VIRA MANIA OU MIMIMI. NA MODA QUE MOVIMENTA O MERCADO E FAZ SEU PRÓPRIO MARKETING.<br>
                EM TUDO QUE É MÚLTIPLO, MODERNO. UM MOSAICO, UMA MANDALA PARA MONTAR DE ACORDO COM SEU MOMENTO, PARA COLOCAR O MOLHO E FAZER DELE UM MANTRA.<br>
                NAQUILO QUE É MAIS. MELHOR PRA VOCÊ, MELHOR PRA MIM. <br>
                MUSIVA. <br>
                UM ESPAÇO DE EVENTOS COMO VOCÊ NUNCA VIU.<br>
                UM LUGAR DE MISTURA E MOVIMENTO.<br>
            </p>  --}}
        </div>
        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
            <iframe width="420" height="315"
            src="https://www.youtube.com/embed/SGWweYhkwOo">
            </iframe>
        </div>
        </div>
    </div>
    <div style="width: 100%;">
        <div class="container-fluid fluido" id="contatoScrollResponsivo">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                <p class="lead">CONTATO</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="formularioWeb" style="height: 42rem;">
        <form action="{{ route('contato.novo2') }}" method="post">
            {{ csrf_field() }}
            <div class="classeFormResponsivo">
                <div class="form-row">
                    <div class="form-group col-md-12 col-sm-12">
                        <input type="text" name="name" class="form-control" id="reservaResponsivo" required placeholder="Nome">
                    </div>
                    <div class="form-group col-md-12 col-sm-12">
                        <input type="email" name="email" class="form-control" id="reservaResponsivoemail" required placeholder="E-mail">
                    </div>
                    <div class="form-group col-md-12 col-sm-12">
                        <input type="text" name="subject" class="form-control" id="subject" placeholder="Assunto" required>
                    </div>
                    <div class="form-group col-md-12 col-sm-12">
                        <input type="text" name="message" class="form-control" id="message" placeholder="Mensagem" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12 col-sm-12">
                        <input type="submit" name="contato" value="Enviar" id="btnContato">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div style="width: 100%;">
    </div>
    <div class="container-fluid contatoResponsivo">  
        <h4 style="color: white;text-align: center;">O local de fortes emoções se encontra aqui!</h4>
        <h6 style="color: white;font-size: 12px;text-align: center;margin-top: 81px;">Av. Manoel José de Arruda, 4435 - Porto, Cuiabá - MT, 78055-498</h6>
        
        <div class="estiloIframeResponsivo">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3842.471913368409!2d-56.10003228460392!3d-15.619838922848185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x939db1e223d840e3%3A0xbd212d145772d9c5!2sMusiva!5e0!3m2!1spt-BR!2sbr!4v1572372775687!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border: 0;margin-left: 0px;width: 100%;height: 100%;" allowfullscreen=""></iframe>
        </div>
        {!!img('m_da_musiva.png')!!}
        <h6 class="comoChegarResponsivo">Como chegar?</h6>
        <div class="tracoResponsivo"></div>    
        <div class="container">
            <div class="row">
                <p class="copyright">Copyright &copy; <?php echo date('Y');?> - MUSIVA - Todos os direitos reservados.</p>
            </div>
        </div>
    </div>
    
</div>


{{-- FIM CONTEUDO --}}
@endsection
@push('script')
<script>
</script>
<script>
    function teste(){
            alert("oi")
        }
    $(function(){


        
        <?php 
        if(isset($novidades)){
            foreach ($novidades as $novidade){
                if ($novidade->principal != 'S'){
        
        ?>
                    $(".carta<?php echo $novidade->novidades_id;?>").click(function(e){
                        e.preventDefault();
                        $('.divFundo<?php echo $novidade->novidades_id;?>').css('display', 'block');
                    });
        <?php 
    
    
                }
            }
        }
    
        ?>
    });

    $(window).scroll(function(){
        var posicaoScroll = $(document).scrollLeft();
        if(posicaoScroll >= 400){
            $('#barra').addClass('barra-block');
            $('#barra').removeClass('barra-none');
            $(".menu").css("display", "none");
            
        } else {
            $('#barra').removeClass('barra-block');
            $('#barra').addClass('barra-none');
            $(".menu").css("display", "block");
        }
    });


    $('#inauguracao-button1').click(function(){
        $('#conteudo1').css('display', 'block');
        $('#inauguracao-button1').removeClass('inauguracao-button1');
        $('#inauguracao-button1').addClass('inauguracao-button1-ativado');
        $('#conteudo2').css('display', 'none');
        $('#inauguracao-button2').removeClass('inauguracao-button2-ativado');
        $('#inauguracao-button2').addClass('inauguracao-button2');
        $('#conteudo3').css('display', 'none');
        $('#inauguracao-button3').removeClass('inauguracao-button3-ativado');
        $('#inauguracao-button3').addClass('inauguracao-button3');
        $('#conteudo4').css('display', 'none');
        $('#inauguracao-button4').removeClass('inauguracao-button4-ativado');
        $('#inauguracao-button4').addClass('inauguracao-button4');
        $('#conteudo5').css('display', 'none');
        $('#inauguracao-button5').removeClass('inauguracao-button5-ativado');
        $('#inauguracao-button5').addClass('inauguracao-button5');
    });
    $('#inauguracao-button2').click(function(){
        $('#conteudo1').css('display', 'none');
        $('#inauguracao-button1').removeClass('inauguracao-button1-ativado');
        $('#inauguracao-button1').addClass('inauguracao-button1');
        $('#conteudo2').css('display', 'block');
        $('#inauguracao-button2').removeClass('inauguracao-button2');
        $('#inauguracao-button2').addClass('inauguracao-button2-ativado');
        $('#conteudo3').css('display', 'none');
        $('#inauguracao-button3').removeClass('inauguracao-button3-ativado');
        $('#inauguracao-button3').addClass('inauguracao-button3');
        $('#conteudo4').css('display', 'none');
        $('#inauguracao-button4').removeClass('inauguracao-button4-ativado');
        $('#inauguracao-button4').addClass('inauguracao-button4');
        $('#conteudo5').css('display', 'none');
        $('#inauguracao-button5').removeClass('inauguracao-button5-ativado');
        $('#inauguracao-button5').addClass('inauguracao-button5');
        
    });
    $('#inauguracao-button3').click(function(){
        $('#conteudo1').css('display', 'none');
        $('#inauguracao-button1').removeClass('inauguracao-button1-ativado');
        $('#inauguracao-button1').addClass('inauguracao-button1');
        $('#conteudo2').css('display', 'none');
        $('#inauguracao-button2').removeClass('inauguracao-button2-ativado');
        $('#inauguracao-button2').addClass('inauguracao-button2');
        $('#conteudo3').css('display', 'block');
        $('#inauguracao-button3').removeClass('inauguracao-button3');
        $('#inauguracao-button3').addClass('inauguracao-button3-ativado');
        $('#conteudo4').css('display', 'none');
        $('#inauguracao-button4').removeClass('inauguracao-button4-ativado');
        $('#inauguracao-button4').addClass('inauguracao-button4');
        $('#conteudo5').css('display', 'none');
        $('#inauguracao-button5').removeClass('inauguracao-button5-ativado');
        $('#inauguracao-button5').addClass('inauguracao-button5');
    });
    $('#inauguracao-button4').click(function(){
        $('#conteudo1').css('display', 'none');
        $('#inauguracao-button1').removeClass('inauguracao-button1-ativado');
        $('#inauguracao-button1').addClass('inauguracao-button1');
        $('#conteudo2').css('display', 'none');
        $('#inauguracao-button2').removeClass('inauguracao-button2-ativado');
        $('#inauguracao-button2').addClass('inauguracao-button2');
        $('#conteudo3').css('display', 'none');
        $('#inauguracao-button3').removeClass('inauguracao-button3-ativado');
        $('#inauguracao-button3').addClass('inauguracao-button3');
        $('#conteudo4').css('display', 'block');
        $('#inauguracao-button4').removeClass('inauguracao-button4');
        $('#inauguracao-button4').addClass('inauguracao-button4-ativado');
        $('#conteudo5').css('display', 'none');
        $('#inauguracao-button5').removeClass('inauguracao-button5-ativado');
        $('#inauguracao-button5').addClass('inauguracao-button5');
    });
    $('#inauguracao-button5').click(function(){
        $('#conteudo1').css('display', 'none');
        $('#inauguracao-button1').removeClass('inauguracao-button1-ativado');
        $('#inauguracao-button1').addClass('inauguracao-button1');
        $('#conteudo2').css('display', 'none');
        $('#inauguracao-button2').removeClass('inauguracao-button2-ativado');
        $('#inauguracao-button2').addClass('inauguracao-button2');
        $('#conteudo3').css('display', 'none');
        $('#inauguracao-button3').removeClass('inauguracao-button3-ativado');
        $('#inauguracao-button3').addClass('inauguracao-button3');
        $('#conteudo4').css('display', 'none');
        $('#inauguracao-button4').removeClass('inauguracao-button4-ativado');
        $('#inauguracao-button4').addClass('inauguracao-button4');
        $('#conteudo5').css('display', 'block');
        $('#inauguracao-button5').removeClass('inauguracao-button5');
        $('#inauguracao-button5').addClass('inauguracao-button5-ativado');
    });

    $('#icone').click(function(){
        $('.grupo-menu-corpo').css('margin-left','0px');
    });
    $('#icone-movel').click(function(){
        $('#bola-maior').css('margin-top','-164px');
        $('#icone-movel').css('display', 'none');
        $('#fechar').css('display', 'block');
        setTimeout(function(){ 
            $('#menu-bolas-menores').css('margin-top','-105px');
            $('#menu-bolas-menores2').css('margin-top','-15px');
            $('#menu-bolas-menores3').css('margin-top','-58px');
            $('#menu-bolas-menores4').css('margin-top','-101px');
        }, 600);
    });
    $('#fechar').click(function(){
        $('#menu-bolas-menores').css('margin-top','-1000px');
        $('#menu-bolas-menores2').css('margin-top','-1000px');
        $('#menu-bolas-menores3').css('margin-top','-1000px');
        $('#menu-bolas-menores4').css('margin-top','-1000px');
        $('#icone-movel').css('display', 'block');
        $('#fechar').css('display', 'none');
        setTimeout(function(){ 
            $('#bola-maior').css('margin-top','-1000px');
        }, 600);
    });

    
$(document).ready(function() {
    var docWidth = $('body').width(),
    slidesWidth = $('#layout').width(),
    rangeX = slidesWidth - docWidth,
    $images = $('#layout');

    $(document).on('mousemove', function(e) {
        var mouseX = e.pageX,
        offset = mouseX / docWidth * slidesWidth - mouseX / 2;

    $images.css({
        '-webkit-transform': 'translate3d(' + -offset + 'px,0,0)',
        'transform': 'translate3d(' + -offset + 'px,0,0)'
        });
    });
});


$('#menuWeb a').click(function(e){
        e.preventDefault();
    var id = $(this).attr('href');
    var targetOffset = $(id).offset().left;
    console.log(targetOffset);

    $('html, body').animate({
        scrollRight: targetOffset - 230
    },1000);
    });
</script>
<script>
    AOS.init();
</script>
<script>
$("#botoes li").on("click", function(){
   var index = $(this).index();
   var lista_top = $("#lista").scrollTop();
   $("#lista").animate({ scrollTop: lista_top+Number((index == 0 && '-')+330) }, 250);
});
</script>
<script>
    $("#botoes2 li").on("click", function(){
        var index = $(this).index();
        var lista_top = $("#listaRow").scrollTop();
        $("#listaRow").animate({ scrollTop: lista_top+Number((index == 0 && '-')+330) }, 250);
    });
</script>



<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    function validaForm(frm) {
        if(frm.nome.value == "" || frm.nome.value == null) {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o seu Nome para reserva'
            });
            frm.nome.focus();
            return false;
        }
        if(frm.email.value == "" || frm.email.value == null) {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o seu E-mail para reserva'
            });
            frm.email.focus();
            return false;
        }
        if(frm.n_pessoas.value == '') {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o seu E-mail para reserva'
            });
            frm.n_pessoas.focus();
            return false;
        }
        if(frm.data_reserva.value == "") {
            Swal.fire({
            icon: 'warning',
            title: 'Informe a data para reserva'
            });
            frm.data_reserva.focus();
            return false;
        }
        if(frm.telefone.value == "") {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o telefone para reserva'
            });
            frm.telefone.focus();
            return false;
        }
        Swal.fire({
            icon: 'success',
            title: 'Formulário enviado com sucesso!'
            });

        return true;
    }
    function validaForm2(frm) {
        if(frm.nome.value == "" || frm.nome.value == null) {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o seu Nome para contato'
            });
            frm.nome.focus();
            return false;
        }
        if(frm.email.value == "" || frm.email.value == null) {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o seu E-mail para contato'
            });
            frm.email.focus();
            return false;
        }
        if(frm.subject.value == '') {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o seu E-mail para contato'
            });
            frm.n_pessoas.focus();
            return false;
        }
        Swal.fire({
            icon: 'success',
            title: 'Formulário enviado com sucesso!'
            });

        return true;
    }
    </script>
    <script>
    
    function comprar(url_ingresso){
        window.open(url_ingresso, '_blank');
    }
    var div = $('#botao-menu-musiva').css({ position: 'absolute' });
    $().mousemove(function(e){
    div.css({
        left: e.pageX
    });
    });
    </script>
@endpush
