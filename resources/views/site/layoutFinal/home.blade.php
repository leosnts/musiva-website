@extends('site.layout.app3')

@section('body')
<style>
    .dataNovidade{
        font-size: 84px;
        height: 146px;
        padding-top: 54px;
        font-weight: 800;
    }
    nav ul li {
        margin: 21px 0px;
    }
    .nomeAvisoPlay{
        padding-top: 28%;
        text-align: center;
        color: white;
        font-size: 20px;    font-weight: 200;    transition: 0.4s;
    }
    .divAvisoPlay{
        position: fixed;
        background-color: rgba(17, 2, 23, 0.94);
        width: 26%;
        left: 40%;
        height: 22%;
        top: -114%;
        border-radius: 15px;
        z-index: 200;
        opacity: 0;
        transition:0.4s;
    }
    .first-content .fb-btn a {
        display: inline-block;
    /* background-color: #fff; */
    border: 2px solid #fff;
    padding: 8px 15px;
    font-size: 13px;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: white;
    background-color: #f9089b;
    text-decoration: none;
    transition: all 0.5s;
}
    .apareceAvisoPlay{
        top: -12%!important;
        opacity: 1!important;
    }
    .h2Reserva{
        font-weight: 800;color: white;font-size: 42px;text-transform: uppercase;letter-spacing: 4px;
    }
    .h2Contato{
        font-weight: 800;
        font-family: sans-serif;
        color: white;
        font-size: 45px;
    }
    .third-content .main-btn a {
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 30px;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #fff;
        text-decoration: none;
        transition: all 0.5s;
    }
    .first-content p {
        color: #fff;
        font-size: 42px;
        text-transform: uppercase;
        font-weight: 300;
        letter-spacing: 2px;
        line-height: 26px;
        margin-bottom: 20px;
    }
    .third-content p {
        font-size: 110px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 800;
        line-height: 24px;
        margin-bottom: 20px;
    }
    .iconePlay {
        font-size: 55px;
        color: white;
        transition: 0.3s;
    }
    .iconePause {
        font-size: 55px;
        color: white;
        transition: 0.3s;
        display: none;
    }
    .aparecerIcone{
        display:block;
    }
    .iconePlayList {
        font-size: 55px;
        color: white;
        transition: 0.3s;
    }
    .aumentaIcone{
        font-size: 65px;
    }
    .first-content h2 {
    font-size: 75px;
    text-transform: uppercase;
    font-weight: 800;
    color: #fff;
    letter-spacing: 0.5px;
    border-bottom: 2px solid #fff;
    display: inline-block;
    padding-bottom: 10px;
    margin-bottom: 20px;
    margin-top: 0px;
}

.third-content h2 {
    font-size: 46px;
    text-transform: uppercase;
    font-weight: 800;
    color: #fff;
    letter-spacing: 0.5px;
    border-bottom: 2px solid #fff;
    display: inline-block;
    padding-bottom: 10px;
    margin-bottom: 20px;
    margin-top: 0px;
}
.slides .slide .content2 {
    overflow-y: scroll;
    position: relative;
    width: 28%;
    height: 80%;
    left: 72%;
    top: 10%;
    bottom: 10%;
    background: rgba(17, 2, 23, 0.94);
    /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
    box-shadow: inset 0 0 0em black, 0 0 3em black;
    /* background: rgba(0, 0, 0, 0.5); */
}
.botaoComprarIngresso {
    border: 2px solid white;
    font-weight: 800;
    font-size: 33px;
    padding: 9px;
    color: white;
    transition: 0.4s;
    font-style: none;
    text-decoration: none;
}
.botaoComprarIngresso:hover {
    border: 2px solid white;
    font-weight: 800;
    font-size: 33px;
    padding: 9px;
    background-color: white;
    color: black;
}
.issoai{
    width: 104%;
    height: 15%;
    top: 87%;
    z-index: 101;
    position: fixed;
    background-color: black;
    transform: rotate(1deg);
    box-shadow: inset 0 0 0em black, 0 0 3em black;
    display: none;
}
.second-content p {
    font-size: 23px;
    color: #fff;
    letter-spacing: 0.5px;
    font-weight: 400;
    text-align: justify;
    line-height: 24px;
    margin-bottom: 30px;
}
.slides .slide .content3 {
    overflow-y: scroll;
    position: relative;
    width: 48%;
    height: 80%;
    left: 52%;
    top: 11%;
    bottom: 10%;
    /* background: rgba(0, 0, 0, 0.5); */
    background: rgba(17, 2, 23, 0.94);
    /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
    box-shadow: inset 0 0 0em black, 0 0 3em black;
    /* background: rgba(0, 0, 0, 0.5); */
}
.slides .slide .content5 {
    overflow-y: scroll;
    position: relative;
    width: 48%;
    height: 80%;
    left: 52%;
    top: 11%;
    bottom: 10%;
    /* background: rgba(0, 0, 0, 0.5); */
    background: rgba(17, 2, 23, 0.94);
    /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
    box-shadow: inset 0 0 0em black, 0 0 3em black;
    /* background: rgba(0, 0, 0, 0.5); */
}
#conteudo1{
    transition: 0.4s;
    display:block;
}
#conteudo2{
    transition: 0.4s;
    display:none;
}
#conteudo3{
    transition: 0.4s;
    display:none;
}
#conteudo4{
    transition: 0.4s;
    display:none;
}
.first-content em {
    font-style: normal;
    font-weight: 800;
}
.first-content {
    padding: 0px;
    text-align: left;
    padding-top: 1%;
}
.second-content .left-content {
    padding: 0px;
    text-align: left;
    padding-top: 60px;
    padding-bottom: 60px;
}
.third-content .first-section {
    margin-top: 30px;
}
.third-content .first-section .right-image {
    margin-left: 0px;
}
.third-content .second-section .left-image {
    margin-right: 0px;
}
.escurecer:hover{
    background-color: black;
    opacity: 0.6;
}
.fifth-content input {
    border-radius: 0px;
    padding-left: 15px;
    font-size: 22px;
    font-weight: 800;
    color: #fff;
    background-color: rgba(250, 250, 250, 0.15);
    outline: none;
    border: none;
    box-shadow: none;
    line-height: 40px;
    height: 73px;
    margin-left: 5px;
    width: 100%;
    margin-bottom: 25px;
}
.spanPela{
    font-family: sans-serif;
    font-size: 108px;
    /* text-shadow: 13px 22px 41px rgb(26, 10, 31); */
    color: white;
}
.fifth-content button {
    max-width: 100%;
    width: 100%;
    display: inline-block;
    border: 2px solid #fff;
    padding: 8px 15px;
    font-size: 47px;
    font-weight: 800;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: #fff;
    text-decoration: none;
    background-color: transparent;
    border-radius: 0px;
    transition: all 0.5s;
}
nav {
    /* antigo */
    /* top: 0vh;
    bottom: 0vh;
    text-align: center;
    background: rgba(17, 2, 23, 0.94);
    position: fixed;
    z-index: 100;
    height: 100vh;
    left: 0;
    width: 20%;
    font-weight: 300;
    font-size: 1rem;
    transition: 1s; */
    /* fim antigo */

    top: 0vh;
    bottom: 0vh;
    text-align: center;
    background: rgba(17, 2, 23, 0.94);
    position: fixed;
    z-index: 100;
    height: 100vh;
    background: linear-gradient(180deg, rgba(17, 2, 23, 0.94) 66%, rgba(249,8,155,0.6783088235294117) 122%, rgba(249,8,155,1) 45%);
    left: 0;
    width: 20%;
    font-weight: 300;
    font-size: 1rem;
    transition: 1s;
}
.bolinha {
    position: absolute;
    background: rgb(26, 10, 31);
    width: 34%;
    top: 42%;
    bottom: 45%;
    left: 82%;
    border-radius: 50%;
    height: 14%;
    cursor: pointer;
    transition: 0.4s;
}

.bolinha img {
    position: absolute;
    top: 27%;
    height: 46%;
    width: 42%;
    left: 28%;
    right: 28%;
    bottom: 27%;
    transition: 0.4s;
}
nav em {
    font-style: normal;
    margin-left: 5px;
    font-weight: 700;
    font-size: 22px;
    font-family: sans-serif;
}
nav ul {
    margin-top: 60px;
    text-align: left;
    width: 70%;
    margin-left: 40px;
    list-style-type: none;
    height: 55vh;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -ms-flex-pack: distribute;
    justify-content: space-around;
}
.pulse {
  animation: pulse 0.7s infinite;
  margin: 0 auto;
  display: table;
  /* margin-top: 50px; */
  animation-direction: alternate;
  -webkit-animation-name: pulse;
  animation-name: pulse;
}
.pulse2 {
  animation: pulse2 0.5s infinite;
  margin: 0 auto;
  display: table;
  /* margin-top: 50px; */
  animation-direction: alternate;
  -webkit-animation-name: pulse2;
  animation-name: pulse2;
}
nav a:hover{
    animation: none!important;
    background: none!important;
}
@-webkit-keyframes pulse {
  0% {
    -webkit-transform: scale(1);
    -webkit-filter: brightness(100%);
    box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
    
  }
  100% {
    -webkit-transform: scale(1.1);
    -webkit-filter: brightness(200%);
    box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
  }
}

@keyframes pulse {
  0% {
    transform: scale(1);
    filter: brightness(100%);
    box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
  }
  100% {
    transform: scale(1.1);
    filter: brightness(200%);
    box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
  }
}



@-webkit-keyframes pulse2 {
  0% {
    -webkit-filter: brightness(50%);
    
  }
  100% {
    -webkit-filter: brightness(100%);
  }
}

@keyframes pulse2 {
  0% {
    filter: brightness(50%);
  }
  100% {
    filter: brightness(100%);
  }
}
.menuAtivo{
    left: -364px;
}
.menuHover{
    left: 90%!important;
}
.menuHoverimg{
    transform: rotate(11deg);
}
nav ul li a{
    transition: 0.4s;
}
nav ul li a:hover{
    
    color:#f9089b;
    margin-left: 19px;
}
nav a.active {
    /* background-color: #fff; */
    color: #f9089b;
}
.passaramMusiva{
    font-weight: 800;
    color: white;
    font-size: 42px;
    display: block;
    text-transform: uppercase;
    letter-spacing: 4px;
    transition: 0.4s;
}
.vemPorAi{
    position: relative;
    width: 5%;
    color: white;
    left: 14%;
    font-family: sans-serif;
    font-weight: 800;
    font-size: 109px;
    opacity: 0;
    transition: 1s;
}
.sai{
    /* opacity: 1; */
}
.textoPag2{
    position: absolute;
    left: 2047px;
    font-size: 142px;
    top: -537px;
    color: rgb(123, 17, 115);
    letter-spacing: -19px;
    line-height: 1;
    font-weight: 800;
    /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
    font-family: sans-serif;
    transform: rotate(-90deg);
    WIDTH: 711px;
    transition: 1s;
    opacity: 0;
}
.textoPag3{
    position: absolute;
    left: 5697px;
    font-size: 70px;
    top: -564px;
    color: rgb(26, 10, 31);
    letter-spacing: -11px;
    line-height: 1;
    font-weight: 800;
    /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
    font-family: sans-serif;
    /* transform: rotate(-90deg); */
    WIDTH: 711px;
    transition: 1s;
    opacity: 0;
}
.textoPag3 img{
    WIDTH: 63%!important;
}
.aparecerTextoPag2{
    top: 209px;
    opacity: 1;
}
.aparecerTextoPag3{
    top: 258px;
    opacity: 1;
}
.desaparecerMusiva{
    height: 0px;
    margin: 0px;
    opacity: 0;
}
#layoutPrincipal{
    display:block;
}
#layoutResponsivo{
    display:none;
}
@media(max-width: 1880px){
    .fifth-content #map {
        padding: 50px 0px 0px 50px;
    }
    .fifth-content textarea {
        border-radius: 0px;
        padding-left: 15px;
        padding-top: 10px;
        font-size: 13px;
        font-weight: 300;
        color: #fff;
        background-color: rgba(250, 250, 250, 0.15);
        outline: none;
        border: none;
        box-shadow: none;
        height: 107px;
        max-height: 220px;
        width: 100%;
        max-width: 100%;
        margin-bottom: 25px;
    }
    .first-content p {
        color: #fff;
        font-size: 42px;
        text-transform: uppercase;
        font-weight: 300;
        letter-spacing: 2px;
        line-height: 26px;
        margin-bottom: 20px;
    }
    .first-content h2 {
    font-size: 75px;
    text-transform: uppercase;
    font-weight: 800;
    color: #fff;
    letter-spacing: 0.5px;
    border-bottom: 2px solid #fff;
    display: inline-block;
    padding-bottom: 10px;
    margin-bottom: 20px;
    margin-top: 0px;
    }
    .third-content h2 {
        font-size: 46px;
        text-transform: uppercase;
        font-weight: 800;
        color: #fff;
        letter-spacing: 0.5px;
        border-bottom: 2px solid #fff;
        display: inline-block;
        padding-bottom: 10px;
        margin-bottom: 20px;
        margin-top: 0px;
    }
    .slides .slide .content2 {
        overflow-y: scroll;
        position: relative;
        width: 28%;
        height: 80%;
        left: 72%;
        top: 10%;
        bottom: 10%;
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .issoai{
        width: 104%;
        height: 15%;
        top: 87%;
        z-index: 101;
        position: fixed;
        background-color: black;
        transform: rotate(1deg);
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        display: none;
    }
    .second-content p {
        font-size: 23px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 400;
        text-align: justify;
        line-height: 24px;
        margin-bottom: 30px;
    }
    .slides .slide .content3 {
        overflow-y: scroll;
        position: relative;
        width: 48%;
        height: 80%;
        left: 52%;
        top: 11%;
        bottom: 10%;
        /* background: rgba(0, 0, 0, 0.5); */
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .slides .slide .content5 {
        overflow-y: scroll;
        position: relative;
        width: 48%;
        height: 80%;
        left: 52%;
        top: 11%;
        bottom: 10%;
        /* background: rgba(0, 0, 0, 0.5); */
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    #conteudo1{
        transition: 0.4s;
        display:block;
    }
    #conteudo2{
        transition: 0.4s;
        display:none;
    }
    #conteudo3{
        transition: 0.4s;
        display:none;
    }
    #conteudo4{
        transition: 0.4s;
        display:none;
    }
    .first-content em {
        font-style: normal;
        font-weight: 800;
    }
    .first-content {
        padding: 0px;
        text-align: left;
        padding-top: 1%;
    }
    .second-content .left-content {
        padding: 0px;
        text-align: left;
        padding-top: 60px;
        padding-bottom: 60px;
    }
    .third-content .first-section {
        margin-top: 30px;
    }
    .third-content .first-section .right-image {
        margin-left: 0px;
    }
    .third-content .second-section .left-image {
        margin-right: 0px;
    }
    .escurecer:hover{
        background-color: black;
        opacity: 0.6;
    }
    .fifth-content input {
        border-radius: 0px;
        padding-left: 15px;
        font-size: 22px;
        font-weight: 800;
        color: #fff;
        background-color: rgba(250, 250, 250, 0.15);
        outline: none;
        border: none;
        box-shadow: none;
        line-height: 40px;
        height: 66px;
        margin-left: 5px;
        width: 100%;
        margin-bottom: 25px;
    }
    .fifth-content button {
        max-width: 100%;
        width: 100%;
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 41px;
        font-weight: 800;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #fff;
        text-decoration: none;
        background-color: transparent;
        border-radius: 0px;
        transition: all 0.5s;
    }
    nav {
        /* antigo */
        /* top: 0vh;
        bottom: 0vh;
        text-align: center;
        background: rgba(17, 2, 23, 0.94);
        position: fixed;
        z-index: 100;
        height: 100vh;
        left: 0;
        width: 20%;
        font-weight: 300;
        font-size: 1rem;
        transition: 1s; */
        /* fim antigo */

        top: 0vh;
        bottom: 0vh;
        text-align: center;
        background: rgba(17, 2, 23, 0.94);
        position: fixed;
        z-index: 100;
        height: 100vh;
        background: linear-gradient(180deg, rgba(17, 2, 23, 0.94) 66%, rgba(249,8,155,0.6783088235294117) 122%, rgba(249,8,155,1) 45%);
        left: 0;
        width: 20%;
        font-weight: 300;
        font-size: 1rem;
        transition: 1s;
    }
    .bolinha {
        position: absolute;
        background: rgb(26, 10, 31);
        width: 34%;
        top: 42%;
        bottom: 45%;
        left: 82%;
        border-radius: 50%;
        height: 14%;
        cursor: pointer;
        transition: 0.4s;
    }

    .bolinha img {
        position: absolute;
        top: 27%;
        height: 46%;
        width: 42%;
        left: 28%;
        right: 28%;
        bottom: 27%;
        transition: 0.4s;
    }
    nav em {
        font-style: normal;
        margin-left: 5px;
        font-weight: 700;
        font-size: 27px;
        font-family: sans-serif;
    }
    nav ul {
        margin-top: 60px;
        text-align: left;
        margin-left: 40px;
        list-style-type: none;
        height: 55vh;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
    .pulse {
    animation: pulse 0.7s infinite;
    margin: 0 auto;
    display: table;
    /* margin-top: 50px; */
    animation-direction: alternate;
    -webkit-animation-name: pulse;
    animation-name: pulse;
    }
    nav a:hover{
        animation: none!important;
        background: none!important;
    }
    @-webkit-keyframes pulse {
    0% {
        -webkit-transform: scale(1);
        -webkit-filter: brightness(100%);
        box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
        
    }
    100% {
        -webkit-transform: scale(1.1);
        -webkit-filter: brightness(200%);
        box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
    }
    }

    @keyframes pulse {
    0% {
        transform: scale(1);
        filter: brightness(100%);
        box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
    }
    100% {
        transform: scale(1.1);
        filter: brightness(200%);
        box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
    }
    }
    .menuAtivo{
        left: -364px;
    }
    .menuHover{
        left: 90%!important;
    }
    .menuHoverimg{
        transform: rotate(11deg);
    }
    nav ul li a{
        transition: 0.4s;
    }
    nav ul li a:hover{
        
        color:#f9089b;
        margin-left: 19px;
    }
    nav a.active {
        /* background-color: #fff; */
        color: #f9089b;
    }
    .passaramMusiva{
        font-weight: 800;
        color: white;
        font-size: 42px;
        display: block;
        text-transform: uppercase;
        letter-spacing: 4px;
        transition: 0.4s;
    }
    .vemPorAi{
        position: relative;
        width: 5%;
        color: white;
        left: 14%;
        font-family: sans-serif;
        font-weight: 800;
        font-size: 109px;
        opacity: 0;
        transition: 1s;
    }
    .sai{
        /* opacity: 1; */
    }
    .textoPag2{
        position: absolute;
        left: 2047px;
        font-size: 142px;
        top: -537px;
        color: rgb(123, 17, 115);
        letter-spacing: -19px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        transform: rotate(-90deg);
        WIDTH: 711px;
        transition: 1s;
        opacity: 0;
    }
    .textoPag3{
        position: absolute;
        /* left: 5697px; localhost*/
        left: 5599px;
        font-size: 70px;
        top: -564px;
        color: rgb(26, 10, 31);
        letter-spacing: -11px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        /* transform: rotate(-90deg); */
        WIDTH: 711px;
        transition: 1s;
        opacity: 0;
    }
    .textoPag3 img{
        WIDTH: 63%!important;
    }
    .aparecerTextoPag2{
        top: 209px;
        opacity: 1;
    }
    .aparecerTextoPag3{
        top: 258px;
        opacity: 1;
    }
    .desaparecerMusiva{
        height: 0px;
        margin: 0px;
        opacity: 0;
    }
    .first-content .fb-btn a {
        display: inline-block;
        /* background-color: #fff; */
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 13px;
        font-size: 28px!important;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: white;
        background-color: #f9089b;
        text-decoration: none;
        transition: all 0.5s;
    }
}
@media(max-width: 1440px){
    .first-content p {
        color: #fff;
        font-size: 42px;
        text-transform: uppercase;
        font-weight: 300;
        letter-spacing: 2px;
        line-height: 26px;
        margin-bottom: 20px;
    }
    .nomeAvisoPlay {
        padding-top: 29%;
        text-align: center;
        color: white;
        font-size: 20px;
        font-weight: 200;
        transition: 0.4s;
    }
 
    .third-content h2 {
        font-size: 26px;
        text-transform: uppercase;
        font-weight: 800;
        color: #fff;
        letter-spacing: 0.5px;
        border-bottom: 2px solid #fff;
        display: inline-block;
        padding-bottom: 10px;
        margin-bottom: 20px;
        margin-top: 0px;
    }
    .third-content p {
        font-size: 45px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 800;
        line-height: 24px;
        margin-bottom: 20px;
    }
    .dataNovidade {
        font-size: 84px;
        height: 110px;
        padding-top: 50px;
        font-weight: 800;
    }
    .third-content .main-btn a {
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 20px;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #fff;
        text-decoration: none;
        transition: all 0.5s;
    }
    .slides .slide .content2 {
        overflow-y: scroll;
        position: relative;
        width: 28%;
        height: 80%;
        left: 72%;
        top: 10%;
        bottom: 10%;
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .issoai{
        width: 104%;
        height: 15%;
        top: 87%;
        z-index: 101;
        position: fixed;
        background-color: black;
        transform: rotate(1deg);
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        display: none;
    }
    .second-content p {
        font-size: 23px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 400;
        text-align: justify;
        line-height: 24px;
        margin-bottom: 30px;
    }
    .slides .slide .content3 {
        overflow-y: scroll;
        position: relative;
        width: 48%;
        height: 80%;
        left: 52%;
        top: 11%;
        bottom: 10%;
        /* background: rgba(0, 0, 0, 0.5); */
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .slides .slide .content5 {
        overflow-y: scroll;
        position: relative;
        width: 48%;
        height: 80%;
        left: 52%;
        top: 11%;
        bottom: 10%;
        /* background: rgba(0, 0, 0, 0.5); */
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    #conteudo1{
        transition: 0.4s;
        display:block;
    }
    #conteudo2{
        transition: 0.4s;
        display:none;
    }
    #conteudo3{
        transition: 0.4s;
        display:none;
    }
    #conteudo4{
        transition: 0.4s;
        display:none;
    }
    .first-content em {
        font-style: normal;
        font-weight: 800;
    }
    .first-content {
        padding: 0px;
        text-align: left;
        padding-top: 1%;
    }
    .second-content .left-content {
        padding: 0px;
        text-align: left;
        padding-top: 60px;
        padding-bottom: 60px;
    }
    .third-content .first-section {
        margin-top: 30px;
    }
    .third-content .first-section .right-image {
        margin-left: 0px;
    }
    .third-content .second-section .left-image {
        margin-right: 0px;
    }
    .escurecer:hover{
        background-color: black;
        opacity: 0.6;
    }
    .fifth-content input {
        border-radius: 0px;
        padding-left: 15px;
        font-size: 17px;
        font-weight: 800;
        color: #fff;
        background-color: rgba(250, 250, 250, 0.15);
        outline: none;
        border: none;
        box-shadow: none;
        line-height: 40px;
        height: 59px;
        margin-left: 5px;
        width: 100%;
        margin-bottom: 25px;
    }
    .fifth-content button {
        max-width: 100%;
        width: 100%;
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 33px;
        font-weight: 800;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #fff;
        text-decoration: none;
        background-color: transparent;
        border-radius: 0px;
        transition: all 0.5s;
    }
    nav {
        /* antigo */
        /* top: 0vh;
        bottom: 0vh;
        text-align: center;
        background: rgba(17, 2, 23, 0.94);
        position: fixed;
        z-index: 100;
        height: 100vh;
        left: 0;
        width: 20%;
        font-weight: 300;
        font-size: 1rem;
        transition: 1s; */
        /* fim antigo */

        top: 0vh;
        bottom: 0vh;
        text-align: center;
        background: rgba(17, 2, 23, 0.94);
        position: fixed;
        z-index: 100;
        height: 100vh;
        background: linear-gradient(180deg, rgba(17, 2, 23, 0.94) 66%, rgba(249,8,155,0.6783088235294117) 122%, rgba(249,8,155,1) 45%);
        left: 0;
        width: 20%;
        font-weight: 300;
        font-size: 1rem;
        transition: 1s;
    }
    .bolinha {
        position: absolute;
        background: rgb(26, 10, 31);
        width: 84px;
        top: 46%;
        bottom: 45%;
        left: 85%;
        border-radius: 50%;
        height: 102px;
        cursor: pointer;
        transition: 0.4s;
    }

    .bolinha img {
        position: absolute;
        top: 27%;
        height: 46%;
        width: 42%;
        left: 28%;
        right: 28%;
        bottom: 27%;
        transition: 0.4s;
    }
    nav em {
        font-style: normal;
        margin-left: 5px;
        font-weight: 700;
        font-size: 18px;
        font-family: sans-serif;
    }
    nav ul {
        margin-top: 60px;
        text-align: left;
        margin-left: 40px;
        list-style-type: none;
        height: 55vh;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
    .pulse {
    animation: pulse 0.7s infinite;
    margin: 0 auto;
    display: table;
    /* margin-top: 50px; */
    animation-direction: alternate;
    -webkit-animation-name: pulse;
    animation-name: pulse;
    }
    nav a:hover{
        animation: none!important;
        background: none!important;
    }
    @-webkit-keyframes pulse {
        0% {
            -webkit-transform: scale(1);
            -webkit-filter: brightness(100%);
            box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
            
        }
        100% {
            -webkit-transform: scale(1.1);
            -webkit-filter: brightness(200%);
            box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
        }
    }

    @keyframes pulse {
        0% {
            transform: scale(1);
            filter: brightness(100%);
            box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
        }
        100% {
            transform: scale(1.1);
            filter: brightness(200%);
            box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
        }
    }
    .menuAtivo {
        left: -254px;
    }
    .menuHover{
        left: 90%!important;
    }
    .menuHoverimg{
        transform: rotate(11deg);
    }
    nav ul li a{
        transition: 0.4s;
    }
    nav ul li a:hover{
        
        color:#f9089b;
        margin-left: 19px;
    }
    nav a.active {
        /* background-color: #fff; */
        color: #f9089b;
    }
    .passaramMusiva{
        font-weight: 800;
        color: white;
        font-size: 42px;
        display: block;
        text-transform: uppercase;
        letter-spacing: 4px;
        transition: 0.4s;
    }
    .vemPorAi{
        position: relative;
        width: 5%;
        color: white;
        left: 14%;
        font-family: sans-serif;
        font-weight: 800;
        font-size: 109px;
        opacity: 0;
        transition: 1s;
    }
    .sai{
        /* opacity: 1; */
    }
    .textoPag2 {
        position: absolute;
        left: 1475px;
        font-size: 142px;
        top: -537px;
        color: rgb(123, 17, 115);
        letter-spacing: -19px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        transform: rotate(-90deg);
        WIDTH: 711px;
        transition: 1s;
        opacity: 0;
    }
    .textoPag3 {
        position: absolute;
        left: 4293px;
        font-size: 57px;
        top: -564px;
        color: rgb(123, 17, 115);
        letter-spacing: -11px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        /* transform: rotate(-90deg); */
        WIDTH: 516px;
        transition: 1s;
        opacity: 0;
    }
    .textoPag3 img{
        WIDTH: 63%!important;
    }
    .aparecerTextoPag2{
        top: 209px;
        opacity: 1;
    }
    .aparecerTextoPag3 {
        top: 258px;
        opacity: 1;
    }
    .desaparecerMusiva{
        height: 0px;
        margin: 0px;
        opacity: 0;
    }
    .first-content .main-btn a {
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 13px;
        font-size: 30px!important;
        background-color: #f9089b;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: white;
        text-decoration: none;
        transition: all 0.5s;
        transition: all 0.5s;
        border-color: white;
    }
    .nomeAvisoPlay {
        padding-top: 29%;
        text-align: center;
        color: white;
        font-size: 20px;
        font-weight: 200;
        transition: 0.4s;
    }
    .botaoComprarIngresso {
        border: 2px solid white;
        font-weight: 800;
        font-size: 19px;
        padding: 9px;
        color: white;
        transition: 0.4s;
        font-style: none;
        text-decoration: none;
    }
    .second-content p {
        font-size: 14px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 400;
        line-height: 24px;
        margin-bottom: 3px;
    }
    .apa2{
        font-size: 18px!important;
    }
    .apa1{
        font-size: 17px!important;
    }
}
@media(max-width:1366px) and (max-height: 667px){
    .first-content p {
        color: #fff;
        font-size: 23px;
        text-transform: uppercase;
        font-weight: 300;
        letter-spacing: 2px;
        line-height: 26px;
        margin-bottom: 20px;
    }
    .nomeAvisoPlay {
        padding-top: 25%;
        text-align: center;
        color: white;
        font-size: 20px;
        font-weight: 200;
        transition: 0.4s;
    }
    .third-content h2 {
        font-size: 26px;
        text-transform: uppercase;
        font-weight: 800;
        color: #fff;
        letter-spacing: 0.5px;
        border-bottom: 2px solid #fff;
        display: inline-block;
        padding-bottom: 10px;
        margin-bottom: 20px;
        margin-top: 0px;
    }
    .third-content p {
        font-size: 45px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 800;
        line-height: 24px;
        margin-bottom: 20px;
    }
    .dataNovidade {
        font-size: 84px;
        height: 110px;
        padding-top: 50px;
        font-weight: 800;
    }
    .first-content .main-btn a {
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 13px;
        font-size: 22px!important;
        background-color: #f9089b;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: white;
        text-decoration: none;
        transition: all 0.5s;
        transition: all 0.5s;
        border-color: white;
    }
    .slides .slide .content2 {
        overflow-y: scroll;
        position: relative;
        width: 28%;
        height: 80%;
        left: 72%;
        top: 10%;
        bottom: 10%;
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .issoai{
        width: 104%;
        height: 15%;
        top: 87%;
        z-index: 101;
        position: fixed;
        background-color: black;
        transform: rotate(1deg);
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        display: none;
    }
    .second-content p {
        font-size: 23px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 400;
        text-align: justify;
        line-height: 24px;
        margin-bottom: 30px;
    }
    .slides .slide .content3 {
        overflow-y: scroll;
        position: relative;
        width: 48%;
        height: 80%;
        left: 52%;
        top: 11%;
        bottom: 10%;
        /* background: rgba(0, 0, 0, 0.5); */
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .slides .slide .content5 {
        overflow-y: scroll;
        position: relative;
        width: 48%;
        height: 80%;
        left: 52%;
        top: 11%;
        bottom: 10%;
        /* background: rgba(0, 0, 0, 0.5); */
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    #conteudo1{
        transition: 0.4s;
        display:block;
    }
    #conteudo2{
        transition: 0.4s;
        display:none;
    }
    #conteudo3{
        transition: 0.4s;
        display:none;
    }
    #conteudo4{
        transition: 0.4s;
        display:none;
    }
    .first-content em {
        font-style: normal;
        font-weight: 800;
    }
    .first-content {
        padding: 0px;
        text-align: left;
        padding-top: 1%;
    }
    .second-content .left-content {
        padding: 0px;
        text-align: left;
        padding-top: 60px;
        padding-bottom: 60px;
    }
    .third-content .first-section {
        margin-top: 30px;
    }
    .third-content .first-section .right-image {
        margin-left: 0px;
    }
    .third-content .second-section .left-image {
        margin-right: 0px;
    }
    .escurecer:hover{
        background-color: black;
        opacity: 0.6;
    }
    .fifth-content input {
        border-radius: 0px;
        padding-left: 15px;
        font-size: 17px;
        font-weight: 800;
        color: #fff;
        background-color: rgba(250, 250, 250, 0.15);
        outline: none;
        border: none;
        box-shadow: none;
        line-height: 40px;
        height: 59px;
        margin-left: 5px;
        width: 100%;
        margin-bottom: 25px;
    }
    .fifth-content button {
        max-width: 100%;
        width: 100%;
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 33px;
        font-weight: 800;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #fff;
        text-decoration: none;
        background-color: transparent;
        border-radius: 0px;
        transition: all 0.5s;
    }
    nav {
        /* antigo */
        /* top: 0vh;
        bottom: 0vh;
        text-align: center;
        background: rgba(17, 2, 23, 0.94);
        position: fixed;
        z-index: 100;
        height: 100vh;
        left: 0;
        width: 20%;
        font-weight: 300;
        font-size: 1rem;
        transition: 1s; */
        /* fim antigo */

        top: 0vh;
        bottom: 0vh;
        text-align: center;
        background: rgba(17, 2, 23, 0.94);
        position: fixed;
        z-index: 100;
        height: 100vh;
        background: linear-gradient(180deg, rgba(17, 2, 23, 0.94) 66%, rgba(249,8,155,0.6783088235294117) 122%, rgba(249,8,155,1) 45%);
        left: 0;
        width: 20%;
        font-weight: 300;
        font-size: 1rem;
        transition: 1s;
    }
    .bolinha {
        position: absolute;
        background: rgb(26, 10, 31);
        width: 84px;
        top: 46%;
        bottom: 43%;
        left: 85%;
        border-radius: 50%;
        height: 102px;
        cursor: pointer;
        transition: 0.4s;
    }

    .bolinha img {
        position: absolute;
        top: 27%;
        height: 46%;
        width: 42%;
        left: 28%;
        right: 28%;
        bottom: 27%;
        transition: 0.4s;
    }
    nav em {
        font-style: normal;
        margin-left: 5px;
        font-weight: 700;
        font-size: 18px;
        font-family: sans-serif;
    }
    nav ul {
        margin-top: 60px;
        text-align: left;
        margin-left: 40px;
        list-style-type: none;
        height: 55vh;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
    .pulse {
    animation: pulse 0.7s infinite;
    margin: 0 auto;
    display: table;
    /* margin-top: 50px; */
    animation-direction: alternate;
    -webkit-animation-name: pulse;
    animation-name: pulse;
    }
    nav a:hover{
        animation: none!important;
        background: none!important;
    }
    @-webkit-keyframes pulse {
        0% {
            -webkit-transform: scale(1);
            -webkit-filter: brightness(100%);
            box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
            
        }
        100% {
            -webkit-transform: scale(1.1);
            -webkit-filter: brightness(200%);
            box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
        }
    }

    @keyframes pulse {
        0% {
            transform: scale(1);
            filter: brightness(100%);
            box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
        }
        100% {
            transform: scale(1.1);
            filter: brightness(200%);
            box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
        }
    }
    .menuAtivo {
        left: -254px;
    }
    .menuHover{
        left: 90%!important;
    }
    .menuHoverimg{
        transform: rotate(11deg);
    }
    nav ul li a{
        transition: 0.4s;
    }
    nav ul li a:hover{
        
        color:#f9089b;
        margin-left: 19px;
    }
    nav a.active {
        /* background-color: #fff; */
        color: #f9089b;
    }
    .passaramMusiva{
        font-weight: 800;
        color: white;
        font-size: 42px;
        display: block;
        text-transform: uppercase;
        letter-spacing: 4px;
        transition: 0.4s;
    }
    .vemPorAi{
        position: relative;
        width: 5%;
        color: white;
        left: 14%;
        font-family: sans-serif;
        font-weight: 800;
        font-size: 109px;
        opacity: 0;
        transition: 1s;
    }
    .sai{
        /* opacity: 1; */
    }
    .textoPag2 {
        position: absolute;
        left: 1475px;
        font-size: 142px;
        top: -537px;
        color: rgb(123, 17, 115);
        letter-spacing: -19px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        transform: rotate(-90deg);
        WIDTH: 711px;
        transition: 1s;
        opacity: 0;
    }
    .textoPag3 {
        position: absolute;
        left: 4305px;
        font-size: 57px;
        top: -564px;
        color: rgb(123, 17, 115);
        letter-spacing: -11px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        /* transform: rotate(-90deg); */
        WIDTH: 516px;
        transition: 1s;
        opacity: 0;
    }
    .textoPag3 img{
        WIDTH: 63%!important;
    }
    .aparecerTextoPag2{
        top: 209px;
        opacity: 1;
    }
    .aparecerTextoPag3 {
        top: 258px;
        opacity: 1;
    }
    .desaparecerMusiva{
        height: 0px;
        margin: 0px;
        opacity: 0;
    }
    .slides .slide .content2 {
        overflow-y: scroll;
        position: relative;
        width: 23%;
        height: 80%;
        /* margin-left: 2px; */
        left: 77%;
        top: 10%;
        bottom: 10%;
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .first-content .author-image img {
        margin-top: 44px;
        border-radius: 15px;
        max-width: 100%;
        margin-left: -37px;
        overflow: hidden;
        float: left;
        margin-right: 35px;
    }
    .first-content h2 {
        font-size: 60px;
        text-transform: uppercase;
        font-weight: 800;
        color: #fff;
        letter-spacing: 0.5px;
        border-bottom: 2px solid #fff;
        display: inline-block;
        padding-bottom: 10px;
        margin-bottom: 20px;
        margin-top: 0px;
        margin-right: 29px;
    }
    .fifth-content input {
        border-radius: 0px;
        padding-left: 15px;
        font-size: 14px;
        font-weight: 800;
        color: #fff;
        background-color: rgba(250, 250, 250, 0.15);
        outline: none;
        border: none;
        box-shadow: none;
        line-height: 40px;
        height: 47px;
        margin-left: 5px;
        width: 100%;
        margin-bottom: 25px;
    }
    .fifth-content input {
        border-radius: 0px;
        padding-left: 15px;
        font-size: 11px;
        font-weight: 800;
        color: #fff;
        background-color: rgba(250, 250, 250, 0.15);
        outline: none;
        border: none;
        box-shadow: none;
        line-height: 40px;
        height: 33px;
        margin-left: 5px;
        width: 100%;
        margin-bottom: 25px;
    }
    .fifth-content button {
        max-width: 100%;
        width: 100%;
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 60px;
        font-weight: 800;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #fff;
        text-decoration: none;
        background-color: transparent;
        border-radius: 0px;
        transition: all 0.5s;
    }
    .inputReserva{
        height: 44px!important;
    }
    .botaoReserva{
        font-size: 39px!important;
    }
    .botaoContato{
        font-size: 23px!important;
    }
    .mapaResponsivo{
        height: 490px!important;
    }
    .textoPag2 {
        position: absolute;
        left: 1392px;
        font-size: 142px;
        top: -537px;
        color: rgb(123, 17, 115);
        letter-spacing: -19px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        transform: rotate(-90deg);
        WIDTH: 711px;
        transition: 1s;
        opacity: 0;
    }
    .aparecerTextoPag2 {
        top: 117px!important;
        opacity: 1!important;
    }
    .first-content .fb-btn a {
        display: inline-block;
        /* background-color: #fff; */
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 13px;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: white;
        background-color: #f9089b;
        font-size: 20px!important;
        text-decoration: none;
        transition: all 0.5s;
    }
    .apa2 {
        font-size: 12px!important;
    }
    .apa1 {
        font-size: 12px!important;
    }
    nav ul li {
        margin: 20px 0px;
    }
}
@media(max-width:1360px) and (max-height: 667px){
    .first-content p {
        color: #fff;
        font-size: 23px;
        text-transform: uppercase;
        font-weight: 300;
        letter-spacing: 2px;
        line-height: 26px;
        margin-bottom: 20px;
    }
    .third-content h2 {
        font-size: 26px;
        text-transform: uppercase;
        font-weight: 800;
        color: #fff;
        letter-spacing: 0.5px;
        border-bottom: 2px solid #fff;
        display: inline-block;
        padding-bottom: 10px;
        margin-bottom: 20px;
        margin-top: 0px;
    }
    .third-content p {
        font-size: 45px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 800;
        line-height: 24px;
        margin-bottom: 20px;
    }
    .dataNovidade {
        font-size: 84px;
        height: 110px;
        padding-top: 50px;
        font-weight: 800;
    }
    .first-content .main-btn a {
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 13px;
        font-size: 22px!important;
        background-color: #f9089b;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: white;
        text-decoration: none;
        transition: all 0.5s;
        transition: all 0.5s;
        border-color: white;
    }
    .slides .slide .content2 {
        overflow-y: scroll;
        position: relative;
        width: 28%;
        height: 80%;
        left: 72%;
        top: 10%;
        bottom: 10%;
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .issoai{
        width: 104%;
        height: 15%;
        top: 87%;
        z-index: 101;
        position: fixed;
        background-color: black;
        transform: rotate(1deg);
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        display: none;
    }
    .second-content p {
        font-size: 23px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 400;
        text-align: justify;
        line-height: 24px;
        margin-bottom: 30px;
    }
    .slides .slide .content3 {
        overflow-y: scroll;
        position: relative;
        width: 48%;
        height: 80%;
        left: 52%;
        top: 11%;
        bottom: 10%;
        /* background: rgba(0, 0, 0, 0.5); */
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .slides .slide .content5 {
        overflow-y: scroll;
        position: relative;
        width: 48%;
        height: 80%;
        left: 52%;
        top: 11%;
        bottom: 10%;
        /* background: rgba(0, 0, 0, 0.5); */
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    #conteudo1{
        transition: 0.4s;
        display:block;
    }
    #conteudo2{
        transition: 0.4s;
        display:none;
    }
    #conteudo3{
        transition: 0.4s;
        display:none;
    }
    #conteudo4{
        transition: 0.4s;
        display:none;
    }
    .first-content em {
        font-style: normal;
        font-weight: 800;
    }
    .first-content {
        padding: 0px;
        text-align: left;
        padding-top: 1%;
    }
    .second-content .left-content {
        padding: 0px;
        text-align: left;
        padding-top: 60px;
        padding-bottom: 60px;
    }
    .third-content .first-section {
        margin-top: 30px;
    }
    .third-content .first-section .right-image {
        margin-left: 0px;
    }
    .third-content .second-section .left-image {
        margin-right: 0px;
    }
    .escurecer:hover{
        background-color: black;
        opacity: 0.6;
    }
    .fifth-content input {
        border-radius: 0px;
        padding-left: 15px;
        font-size: 17px;
        font-weight: 800;
        color: #fff;
        background-color: rgba(250, 250, 250, 0.15);
        outline: none;
        border: none;
        box-shadow: none;
        line-height: 40px;
        height: 59px;
        margin-left: 5px;
        width: 100%;
        margin-bottom: 25px;
    }
    .fifth-content button {
        max-width: 100%;
        width: 100%;
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 33px;
        font-weight: 800;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #fff;
        text-decoration: none;
        background-color: transparent;
        border-radius: 0px;
        transition: all 0.5s;
    }
    nav {
        /* antigo */
        /* top: 0vh;
        bottom: 0vh;
        text-align: center;
        background: rgba(17, 2, 23, 0.94);
        position: fixed;
        z-index: 100;
        height: 100vh;
        left: 0;
        width: 20%;
        font-weight: 300;
        font-size: 1rem;
        transition: 1s; */
        /* fim antigo */

        top: 0vh;
        bottom: 0vh;
        text-align: center;
        background: rgba(17, 2, 23, 0.94);
        position: fixed;
        z-index: 100;
        height: 100vh;
        background: linear-gradient(180deg, rgba(17, 2, 23, 0.94) 66%, rgba(249,8,155,0.6783088235294117) 122%, rgba(249,8,155,1) 45%);
        left: 0;
        width: 20%;
        font-weight: 300;
        font-size: 1rem;
        transition: 1s;
    }
    .bolinha {
        position: absolute;
        background: rgb(26, 10, 31);
        width: 84px;
        top: 46%;
        bottom: 43%;
        left: 85%;
        border-radius: 50%;
        height: 102px;
        cursor: pointer;
        transition: 0.4s;
    }

    .bolinha img {
        position: absolute;
        top: 27%;
        height: 46%;
        width: 42%;
        left: 28%;
        right: 28%;
        bottom: 27%;
        transition: 0.4s;
    }
    nav em {
        font-style: normal;
        margin-left: 5px;
        font-weight: 700;
        font-size: 18px;
        font-family: sans-serif;
    }
    nav ul {
        margin-top: 60px;
        text-align: left;
        margin-left: 40px;
        list-style-type: none;
        height: 55vh;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
    .pulse {
    animation: pulse 0.7s infinite;
    margin: 0 auto;
    display: table;
    /* margin-top: 50px; */
    animation-direction: alternate;
    -webkit-animation-name: pulse;
    animation-name: pulse;
    }
    nav a:hover{
        animation: none!important;
        background: none!important;
    }
    @-webkit-keyframes pulse {
        0% {
            -webkit-transform: scale(1);
            -webkit-filter: brightness(100%);
            box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
            
        }
        100% {
            -webkit-transform: scale(1.1);
            -webkit-filter: brightness(200%);
            box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
        }
    }

    @keyframes pulse {
        0% {
            transform: scale(1);
            filter: brightness(100%);
            box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
        }
        100% {
            transform: scale(1.1);
            filter: brightness(200%);
            box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
        }
    }
    .menuAtivo {
        left: -254px;
    }
    .menuHover{
        left: 90%!important;
    }
    .menuHoverimg{
        transform: rotate(11deg);
    }
    nav ul li a{
        transition: 0.4s;
    }
    nav ul li a:hover{
        
        color:#f9089b;
        margin-left: 19px;
    }
    nav a.active {
        /* background-color: #fff; */
        color: #f9089b;
    }
    .passaramMusiva{
        font-weight: 800;
        color: white;
        font-size: 42px;
        display: block;
        text-transform: uppercase;
        letter-spacing: 4px;
        transition: 0.4s;
    }
    .vemPorAi{
        position: relative;
        width: 5%;
        color: white;
        left: 14%;
        font-family: sans-serif;
        font-weight: 800;
        font-size: 109px;
        opacity: 0;
        transition: 1s;
    }
    .sai{
        /* opacity: 1; */
    }
    .textoPag2 {
        position: absolute;
        left: 1416px;
        font-size: 142px;
        top: -537px;
        color: rgb(123, 17, 115);
        letter-spacing: -19px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        transform: rotate(-90deg);
        WIDTH: 711px;
        transition: 1s;
        opacity: 0;
    }
    .textoPag3 {
        position: absolute;
        left: 4046px;
        font-size: 57px;
        top: -564px;
        color: rgb(123, 17, 115);
        letter-spacing: -11px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        /* transform: rotate(-90deg); */
        WIDTH: 516px;
        transition: 1s;
        opacity: 0;
    }
    .textoPag3 img{
        WIDTH: 63%!important;
    }
    .aparecerTextoPag2{
        top: 209px;
        opacity: 1;
    }
    .aparecerTextoPag3 {
        top: 258px;
        opacity: 1;
    }
    .desaparecerMusiva{
        height: 0px;
        margin: 0px;
        opacity: 0;
    }
    .slides .slide .content2 {
        overflow-y: scroll;
        position: relative;
        width: 23%;
        height: 80%;
        /* margin-left: 2px; */
        left: 77%;
        top: 10%;
        bottom: 10%;
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .first-content .author-image img {
        margin-top: 44px;
        border-radius: 15px;
        max-width: 100%;
        margin-left: -37px;
        overflow: hidden;
        float: left;
        margin-right: 35px;
    }
    .first-content h2 {
        font-size: 60px;
        text-transform: uppercase;
        font-weight: 800;
        color: #fff;
        letter-spacing: 0.5px;
        border-bottom: 2px solid #fff;
        display: inline-block;
        padding-bottom: 10px;
        margin-bottom: 20px;
        margin-top: 0px;
        margin-right: 29px;
    }
    .fifth-content input {
        border-radius: 0px;
        padding-left: 15px;
        font-size: 14px;
        font-weight: 800;
        color: #fff;
        background-color: rgba(250, 250, 250, 0.15);
        outline: none;
        border: none;
        box-shadow: none;
        line-height: 40px;
        height: 47px;
        margin-left: 5px;
        width: 100%;
        margin-bottom: 25px;
    }
    .fifth-content input {
        border-radius: 0px;
        padding-left: 15px;
        font-size: 11px;
        font-weight: 800;
        color: #fff;
        background-color: rgba(250, 250, 250, 0.15);
        outline: none;
        border: none;
        box-shadow: none;
        line-height: 40px;
        height: 33px;
        margin-left: 5px;
        width: 100%;
        margin-bottom: 25px;
    }
    .fifth-content button {
        max-width: 100%;
        width: 100%;
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 60px;
        font-weight: 800;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #fff;
        text-decoration: none;
        background-color: transparent;
        border-radius: 0px;
        transition: all 0.5s;
    }
    .inputReserva{
        height: 44px!important;
    }
    .botaoReserva{
        font-size: 39px!important;
    }
    .botaoContato{
        font-size: 23px!important;
    }
    .mapaResponsivo{
        height: 490px!important;
    }
    .second-content p {
        font-size: 14px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 400;
        line-height: 24px;
        margin-bottom: 1px;
    }
    .apa2 {
        font-size: 12px!important;
    }
    .apa1 {
        font-size: 12px!important;
    }
}
@media(max-width: 1024px){
    .first-content p {
        color: #fff;
        font-size: 32px;
        text-transform: uppercase;
        font-weight: 300;
        letter-spacing: 1px;
        line-height: 26px;
        margin-bottom: 20px;
    }
    .nomeAvisoPlay {
        padding-top: 44%;
        text-align: center;
        color: white;
        font-size: 20px;
        font-weight: 200;
        transition: 0.4s;
    }
    .h2Contato{
        font-weight: 800;
        font-family: sans-serif;
        color: white;
        font-size: 45px;
    }
    .first-content h2 {
        font-size: 60px;
        text-transform: uppercase;
        font-weight: 800;
        color: #fff;
        letter-spacing: 0.5px;
        border-bottom: 2px solid #fff;
        display: inline-block;
        padding-bottom: 10px;
        margin-bottom: 20px;
        margin-top: 0px;
    }
    .third-content h2 {
        font-size: 20px;
        text-transform: uppercase;
        font-weight: 800;
        color: #fff;
        letter-spacing: 0.5px;
        border-bottom: 2px solid #fff;
        display: inline-block;
        padding-bottom: 10px;
        margin-bottom: 20px;
        margin-top: 0px;
    }
    .slides .slide .content2 {
        overflow-y: scroll;
        position: relative;
        width: 28%;
        width: 233px;
        height: 61%;
        left: 78%;
        top: 17%;
        bottom: 10%;
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .first-content .author-image img {
        margin-top: 15px;
        border-radius: 15px;
        max-width: 100%;
        margin-left: -34px;
        overflow: hidden;
        float: left;
        margin-right: 35px;
    }
    .issoai{
        width: 104%;
        height: 15%;
        top: 87%;
        z-index: 101;
        position: fixed;
        background-color: black;
        transform: rotate(1deg);
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        display: none;
    }
    .second-content p {
        font-size: 14px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 400;
        line-height: 24px;
        margin-bottom: 30px;
    }
    .h2Reserva {
        font-weight: 800;
        color: white;
        font-size: 42px;
        margin-top: 57px;
        text-transform: uppercase;
        letter-spacing: 4px;
    }
    .slides .slide .content3 {
        overflow-y: scroll;
        position: relative;
        width: 48%;
        height: 80%;
        left: 52%;
        top: 11%;
        bottom: 10%;
        /* background: rgba(0, 0, 0, 0.5); */
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .slides .slide .content5 {
        overflow-y: scroll;
        position: relative;
        width: 48%;
        height: 80%;
        left: 52%;
        top: 11%;
        bottom: 10%;
        /* background: rgba(0, 0, 0, 0.5); */
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    #conteudo1{
        transition: 0.4s;
        display:block;
    }
    #conteudo2{
        transition: 0.4s;
        display:none;
    }
    #conteudo3{
        transition: 0.4s;
        display:none;
    }
    #conteudo4{
        transition: 0.4s;
        display:none;
    }
    .first-content em {
        font-style: normal;
        font-weight: 800;
    }
    .first-content {
        padding: 0px;
        text-align: left;
        padding-top: 1%;
    }
    .second-content .left-content {
        padding: 0px;
        text-align: left;
        padding-top: 60px;
        padding-bottom: 60px;
    }
    .third-content .first-section {
        margin-top: 30px;
    }
    .third-content .first-section .right-image {
        margin-left: 0px;
    }
    .third-content .second-section .left-image {
        margin-right: 0px;
    }
    .escurecer:hover{
        background-color: black;
        opacity: 0.6;
    }
    .fifth-content input {
        border-radius: 0px;
        padding-left: 15px;
        font-size: 14px;
        font-weight: 800;
        color: #fff;
        background-color: rgba(250, 250, 250, 0.15);
        outline: none;
        border: none;
        box-shadow: none;
        line-height: 40px;
        height: 49px;
        margin-left: 5px;
        width: 100%;
        margin-bottom: 25px;
    }
    .fifth-content button {
        max-width: 100%;
        width: 100%;
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        /* font-size: 37px;localhost */
        font-size: 23px;
        font-weight: 800;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #fff;
        text-decoration: none;
        background-color: transparent;
        border-radius: 0px;
        transition: all 0.5s;
    }
    nav {
        top: 0vh;
        bottom: 0vh;
        text-align: center;
        background: rgba(17, 2, 23, 0.94);
        position: fixed;
        z-index: 100;
        height: 100vh;
        background: linear-gradient(180deg, rgba(17, 2, 23, 0.94) 66%, rgba(249,8,155,0.6783088235294117) 122%, rgba(249,8,155,1) 45%);
        left: 0;
        width: 29%;
        font-weight: 300;
        font-size: 1rem;
        transition: 1s;
    }
    .bolinha {
        position: absolute;
        background: rgb(26, 10, 31);
        width: 84px;
        top: 46%;
        bottom: 45%;
        left: 85%;
        border-radius: 50%;
        height: 102px;
        cursor: pointer;
        transition: 0.4s;
    }

    .bolinha img {
        position: absolute;
        top: 27%;
        height: 46%;
        width: 42%;
        left: 28%;
        right: 28%;
        bottom: 27%;
        transition: 0.4s;
    }
    nav em {
        font-style: normal;
        margin-left: 5px;
        font-weight: 700;
        font-size: 18px;
        font-family: sans-serif;
    }
    nav ul {
        margin-top: 60px;
        text-align: left;
        margin-left: 40px;
        list-style-type: none;
        height: 55vh;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
    .pulse {
    animation: pulse 0.7s infinite;
    margin: 0 auto;
    display: table;
    /* margin-top: 50px; */
    animation-direction: alternate;
    -webkit-animation-name: pulse;
    animation-name: pulse;
    }
    nav a:hover{
        animation: none!important;
        background: none!important;
    }
    @-webkit-keyframes pulse {
        0% {
            -webkit-transform: scale(1);
            -webkit-filter: brightness(100%);
            box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
            
        }
        100% {
            -webkit-transform: scale(1.1);
            -webkit-filter: brightness(200%);
            box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
        }
    }

    @keyframes pulse {
        0% {
            transform: scale(1);
            filter: brightness(100%);
            box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
        }
        100% {
            transform: scale(1.1);
            filter: brightness(200%);
            box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
        }
    }
    .menuAtivo {
        left: -254px;
    }
    .menuHover{
        left: 90%!important;
    }
    .menuHoverimg{
        transform: rotate(11deg);
    }
    nav ul li a{
        transition: 0.4s;
    }
    nav ul li a:hover{
        
        color:#f9089b;
        margin-left: 19px;
    }
    nav a.active {
        /* background-color: #fff; */
        color: #f9089b;
    }
    .passaramMusiva{
        font-weight: 800;
        color: white;
        font-size: 42px;
        display: block;
        text-transform: uppercase;
        letter-spacing: 4px;
        transition: 0.4s;
    }
    .vemPorAi{
        position: relative;
        width: 5%;
        color: white;
        left: 14%;
        font-family: sans-serif;
        font-weight: 800;
        font-size: 109px;
        opacity: 0;
        transition: 1s;
    }
    .sai{
        /* opacity: 1; */
    }
    .textoPag2 {
        position: absolute;
        left: 947px;
        font-size: 108px;
        top: -537px;
        color: rgb(123, 17, 115);
        letter-spacing: -19px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        transform: rotate(-90deg);
        WIDTH: 688px;
        transition: 1s;
        opacity: 0;
    }
    .textoPag3 {
        position: absolute;
        left: 3105px;
        font-size: 37px;
        top: -564px;
        color: rgb(123, 17, 115);
        letter-spacing: 1px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        /* transform: rotate(-90deg); */
        WIDTH: 278px;
        transition: 1s;
        opacity: 0;
    }
    .textoPag3 img{
        WIDTH: 63%!important;
    }
    .aparecerTextoPag2{
        top: 209px;
        opacity: 1;
    }
    .aparecerTextoPag3 {
        top: 139px;
        opacity: 1;
    }
    .desaparecerMusiva{
        height: 0px;
        margin: 0px;
        opacity: 0;
    }
    .first-content .main-btn a {
        display: inline-block;
        border: 2px solid #fff;
        padding: 8px 15px;
        width: 100%;
        font-size: 13px;
        font-size: 20px!important;
        background-color: #f9089b;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: white;
        text-align: center;
        text-decoration: none;
        transition: all 0.5s;
        transition: all 0.5s;
        border-color: white;
    }
    .dataNovidade{
        font-size: 63px;
        height: 37px;
        padding-top: 0px;
        font-weight: 800;
    }
    .third-content p {
        font-size: 54px;
        color: #fff;
        letter-spacing: 0.5px;
        font-weight: 800;
        line-height: 24px;
        margin-bottom: 20px;
    }
    .spanPela{
        font-family: sans-serif;
        font-size: 82px;
        /* text-shadow: 13px 22px 41px rgb(26, 10, 31); */
        color: white;
    }
    .first-content .fb-btn a {
        display: inline-block;
        /* background-color: #fff; */
        border: 2px solid #fff;
        padding: 8px 15px;
        font-size: 13px;
        font-size: 28px!important;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: white;
        text-align: center;
        background-color: #f9089b;
        text-decoration: none;
        transition: all 0.5s;
    }
    .apa2{
        font-size: 18px!important;
    }
    .apa1{
        font-size: 17px!important;
    }
    .botaoComprarIngresso {
        border: 2px solid white;
        font-weight: 800;
        font-size: 13px;
        padding: 9px;
        color: white;
        transition: 0.4s;
        font-style: none;
        text-decoration: none;
    }
    .second-content .left-content p {
        font-size: 13px;
        margin-bottom: 4px;
        /* text-align: left; */
    }
}
@media(max-width:768px){
    .foto {
        background-position-x: center;
    }
    .slides .slide .content2 {
        overflow-y: scroll;
        position: relative;
        width: 28%;
        width: 547px;
        height: 45%;
        left: 29%;
        top: 56%;
        bottom: 10%;
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        box-shadow: inset 0 0 0em black, 0 0 3em black;
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .first-content .author-image img {
        margin-top: 15px;
        border-radius: 15px;
        max-width: 38%;
        margin-left: 19px;
        overflow: hidden;
        float: left;
        margin-right: 35px;
    }
    .classesrdsftyfyujgfhrgsdfjggghkstgfhgdf{
        width: 100%;
        height: 125px!important;
    }
    .divResponsivo2{
        display:block!important;
    }
    .divPrincipal2{
        display:none!important;
    }
    .first-content .main-btn a {
        display: inline-block;
        border: 2px solid #fff;
        padding: 33px 15px;
        width: 100%;
        font-size: 13px;
        font-size: 33px!important;
        background-color: #f9089b;
        text-transform: uppercase;
        letter-spacing: 1px;
        font-weight: 800;
        color: white;
        height: 120px;
        text-align: center;
        text-decoration: none;
        transition: all 0.5s;
        transition: all 0.5s;
        border-color: white;
    }
    .textoPag3 {
        position: absolute;
        left: 2283px;
        font-size: 35px;
        top: -564px;
        color: rgb(123, 17, 115);
        letter-spacing: 1px;
        line-height: 1;
        font-weight: 800;
        /* text-shadow: 9px 9px 31px rgb(123, 17, 115); */
        font-family: sans-serif;
        transform: rotate(-90deg);
        WIDTH: 278px;
        transition: 1s;
        opacity: 0;
    }
    .spanPela {
        font-family: sans-serif;
        font-size: 72px;
        /* text-shadow: 13px 22px 41px rgb(26, 10, 31); */
        color: white;
    }
    .aparecerTextoPag3 {
        top: 139px!important;
        opacity: 1!important;
    }
    .second-content .left-content {
        padding: 0px;
        text-align: left;
        padding-top: 0px;
        padding-bottom: 8px;
    }
    .mapaResponsivo{
        height: 250px!important;
        width: 320px!important
    }
    .bolinha {
        position: absolute;
        background: rgb(26, 10, 31);
        width: 84px;
        top: 46%;
        bottom: 45%;
        left: 81%;
        border-radius: 50%;
        height: 102px;
        cursor: pointer;
        transition: 0.4s;
    }
    .menuAtivo {
        left: -199px;
    }
    nav ul {
        margin-top: 60px;
        text-align: left;
        margin-left: 18px;
        list-style-type: none;
        height: 55vh;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        width: 80%!important;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
    .aumentaIcone {
        font-size: 50px;
    }
}
@media(max-width: 425px){
    .imagem-principal-responsivo {
        width: 929px;
        height: 100%;
        /* margin-top: 54px; */
        margin-left: -229px;
    }
    .botaoResponsivoComprarIngresso2 {
        width: 100%;
        height: 26%;
        opacity: 80%;
        top: 74%;
        position: absolute;
    }
    .tituloCompreResponsivo2 {
        position: absolute;
        left: 6%;
        top: 35%;
        font-family: sans-serif;
        font-size: 30px;
    }
    #layoutPrincipal{
        display:none;
    }
    #layoutResponsivo{
        width: 100%;
        height: 100%;
        display:block;
    }
    .pulseResponsivo {
    animation: pulseResponsivo 0.7s infinite;
    margin: 0 auto;
    display: table;
    /* margin-top: 50px; */
    animation-direction: alternate;
    -webkit-animation-name: pulseResponsivo;
    animation-name: pulseResponsivo;
    }
    nav a:hover{
        animation: none!important;
        background: none!important;
    }
    @-webkit-keyframes pulseResponsivo {
        0% {
            -webkit-transform: scale(1);
            -webkit-filter: brightness(100%);
            box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
            
        }
        100% {
            -webkit-transform: scale(1.1);
            -webkit-filter: brightness(200%);
            box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
        }
    }

    @keyframes pulseResponsivo {
        0% {
            transform: scale(1);
            filter: brightness(100%);
            box-shadow: 0 0 0 0 rgba(82, 26, 86, 0.7);
        }
        100% {
            transform: scale(1.1);
            filter: brightness(200%);
            box-shadow: 0 0 0 19px rgba(0, 0, 0, 0);
        }
    }
    .bolinhaResponsivo {
        position: fixed;
        background: rgb(26, 10, 31);
        width: 56px;
        top: 5%;
        bottom: 89%;
        left: 5%;
        border-radius: 50%;
        height: 54px;
        cursor: pointer;
        z-index: 1001;
        transition: 0.4s;
    }
    .bolinha{
        display: none;
    }
    .bolinhaResponsivoX {
        position: absolute;
        /* background: rgb(26, 10, 31); */
        width: 56px;
        top: 2%;
        color: white;
        font-size: 43px;
        bottom: 43%;
        font-weight: 800;
        left: 8%;
        border-radius: 50%;
        height: 56px;
        cursor: pointer;
        z-index: 1001;
        display:none;
        transition: 0.4s;
    }

    .bolinhaResponsivo img {
        position: absolute;
        top: 27%;
        height: 46%;
        width: 42%;
        left: 28%;
        right: 28%;
        bottom: 27%;
        transition: 0.4s;
    }
    .menuResponsivo{
        position: fixed;
        width: 100%!important;
        height: 100%!important;
        z-index: -20;
        opacity: 0;
        background: rgba(17, 2, 23, 0.94);
        transition: 0.4s;
    }
    /* .apareceResponsivo{
        z-index: 1000;
        opacity: 1;
    } */
    .apareceResponsivo{
        /* left:0px; */
        z-index: 1000;
        opacity: 1;
    }
    ul.listaResponsivo{
        padding: 51px;
        height: 100%;
        text-align: center;
    }
    ul.listaResponsivo li {
        width: 100%;
        height: 13%;
        list-style: none;
        font-size: 32px;
        text-align: center;
        color: white;
        padding-bottom: 13%;
        font-weight: 800;
    }
    /* .imagem-principal-responsivo{
        height: 433px;
        margin-left: -165px;
    } */
    .fluido {
        padding-left        : 0px;
        padding-right       : 0px;
        /* -webkit-transform: skew(30deg); */
        -ms-transform       : skew(30deg);
        /* transform        : skew(30deg); */
        transform           : rotate(0deg);
        /* background-color : black; */

    }
    .gf {
        margin-left: 27px;
    }

    .gf .divFundo {
        background-color: #DF70A8;
        width           : 100%;
        opacity         : 40%;
        position        : absolute;
        height          : 87%;
        display         : none;
    }

    .gf .divFundo:hover {
        display: block;
    }
    #dff {
        width: 18rem!important;
        margin-left: 13%;
        margin-top: 19px;
    }
    img .carta:hover {
        opacity: 40%;
    }
    .botaoResponsivoComprarIngresso {
        width: 100%;
        height: 30%;
        opacity: 80%;
        top: 57%;
        position: absolute;
    }
    .tituloCompreResponsivo {
        position: absolute;
        left: 5%;
        top: 30%;
        font-size: 24px;
    }
    nav{
        width: 100%;
        left:-1000px;
    }
    #layoutResponsivo nav ul li{
        padding-bottom: 5%;
        text-align: center;
    }
    #layoutResponsivo nav ul li a{
        font-size: 38px;
        font-weight: 800;
    }
    .slides .slide .content2 {
        overflow-y: scroll;
        position: relative;
        width: 28%;
        width: 101%;
        height: 101%;
        left: 0%;
        top: -1%;
        bottom: 10%;
        background: rgba(17, 2, 23, 0.94);
        /* background: linear-gradient(160deg, rgba(17, 2, 23, 0.94) 0%, rgba(0,0,0,1) 104%); */
        /* box-shadow: inset 0 0 0em black, 0 0 3em black; */
        /* background: rgba(0, 0, 0, 0.5); */
    }
    .first-content .author-image img.img2 {
        margin-top: 0px;
        border-radius: 0px;
        max-width: 100%;
        margin-left: 0px;
        overflow: hidden;
        float: left;
        margin-right: 0px;
    }
    .imagem-principal-responsivo {
        width: 762px;
        height: 100%;
        /* margin-top: 54px; */
        margin-left: -229px;
    }
    .tituloCompreResponsivo2 {
        position: absolute;
        left: 6%;
        top: 35%;
        font-family: sans-serif;
        font-size: 21px;
    }
    ul.listaResponsivo {
        padding: 0px;
        height: 70%;
        text-align: center;
    }
    .menuResponsivo ul.listaResponsivo li a{
        color:white;
    }
    .lead {
        font-size: 2.6rem!important;
        font-weight: 800!important;
        width: 261px;
        margin-top: 12px;
        text-align: center;
    }
    .imagem-principal-responsivo {
        width: 929px;
        height: 100%;
        margin-top: 0px;
        margin-left: -308px;
    }
    
}
@media(max-width: 375px){
    .paah {
        background: url(../../img/Musiva-1.jpg) no-repeat center;
    }
    .gf .divFundo {
        background-color: #DF70A8;
        width           : 100%;
        opacity         : 40%;
        position        : absolute;
        height          : 87%;
        display         : none;
    }

    .gf .divFundo:hover {
        display: block;
    }
    #dff {
        margin-left: 27px;
    }
    img .carta:hover {
        opacity: 40%;
    }
    .botaoResponsivoComprarIngresso {
        width: 100%;
        height: 30%;
        opacity: 80%;
        top: 57%;
        position: absolute;
    }
    .tituloCompreResponsivo {
        position: absolute;
        left: 5%;
        top: 30%;
        font-size: 24px;
    }
}
@media(max-width:320px){
    .gf {
        margin-left: 27px;
    }

    .gf .divFundo {
        background-color: #DF70A8;
        width           : 100%;
        opacity         : 40%;
        position        : absolute;
        height          : 87%;
        display         : none;
    }

    .gf .divFundo:hover {
        display: block;
    }
    #dff {
        margin-left: 1px;
    }
    img .carta:hover {
        opacity: 40%;
    }
    .botaoResponsivoComprarIngresso {
        background-color: green;
        width: 100%;
        height: 100px;
        opacity: 80%;
        top: 57%;
        position: absolute;
    }
}
</style>
    
<div id="layoutPrincipal">
    <div class="sequence">
    
        <div class="seq-preloader">
            <svg width="39" height="16" viewBox="0 0 39 16" xmlns="http://www.w3.org/2000/svg" class="seq-preload-indicator"><g fill="#F96D38"><path class="seq-preload-circle seq-preload-circle-1" d="M3.999 12.012c2.209 0 3.999-1.791 3.999-3.999s-1.79-3.999-3.999-3.999-3.999 1.791-3.999 3.999 1.79 3.999 3.999 3.999z"/><path class="seq-preload-circle seq-preload-circle-2" d="M15.996 13.468c3.018 0 5.465-2.447 5.465-5.466 0-3.018-2.447-5.465-5.465-5.465-3.019 0-5.466 2.447-5.466 5.465 0 3.019 2.447 5.466 5.466 5.466z"/><path class="seq-preload-circle seq-preload-circle-3" d="M31.322 15.334c4.049 0 7.332-3.282 7.332-7.332 0-4.049-3.282-7.332-7.332-7.332s-7.332 3.283-7.332 7.332c0 4.05 3.283 7.332 7.332 7.332z"/></g></svg>
        </div>
    
    </div>
    <div class="divAvisoPlay">
        <p class="pulse2 nomeAvisoPlay" style=""><strong>{{$novidadesPrincipal[0]->nome}} - </strong>{{$novidadesPrincipal[0]->nome_musica}}</p>
    </div>
    <nav class="">
        <div class="bolinha pulse">{!!img('m_da_musiva.png')!!}</div>
        <div class="logo">
            <img src="{{asset("public/img/logoMusivaBranca.png")}}" alt="">
        </div>
        <div class="mini-logo">
            <img src="img/mini_logo.png" alt="">
        </div>
        <ul>
            <li><a href="#1" class="clickMenu"><em class="aHome">HOME</em></a></li>
            <li style="text-align: center;"><a href="#2" style="padding: 0px;" class="clickMenu"><em class="aEvento">PRÓXIMOS EVENTOS</em></a></li>
            <li><a href="#3" class="clickMenu"><em class="aReserva">RESERVA</em></a></li>
            <li style="text-align: center;"><a href="#4" style="padding: 0px;" class="clickMenu"><em class="jaPassaram">JÁ PASSARAM POR AQUI</em></a></li>
            <li><a href="#5" class="clickMenu"><em class="aMusiva">A MUSIVA</em></a></li>
            <li><a href="#6" class="clickMenu"><em class="aContato">CONTATO</em></a></li>
        </ul>
        <div class="col-md-12" style="padding-top: 12%;">
            <div class="col-md-6">
            <i class="zmdi zmdi-view-list-alt iconePlayList"></i>
            <p style="color:white;font-size: 18px;font-weight: 500;">Playlist</p>
            </div>
            <div class="col-md-6">
                <i class="zmdi zmdi-play iconePlay"></i>
                <i class="zmdi zmdi-pause iconePause"></i>
                <p style="color:white;font-size: 18px;font-weight: 500;">Play</p>
            </div>
        </div>
    
    {{-- <div style="width: 100%;height: 20px;background-color: aqua;"></div> --}}
    </nav>
    <div class="issoai">
        <div class="container-fluid">
            <div class="col-md-12" style="text-align: right;">
                <h2></h2>
            </div>
        </div>
    </div>
    <div class="slides">
        <div class="slide foto" id="1" style="<?php if(isset($novidades)){foreach($novidades as $novidade){if($novidade->principal == 'S'){ ?>background-image:url(<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>)<?php }}}?>">
            
            <div class="content first-content content2">
                <div class="container-fluid">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        
                        @if(isset($segundas))
                            @if($novidadesPrincipal[0]->novidades_id == $segundas[0]->imagem_novidade_id)
                                <div class="author-image"><img src="<?php echo asset('storage/app/public/'.$segundas[0]->imagem);?>" alt=""></div>
                            @endif
                        @endif
                    </div>
                    <div class="col-md-12" style="text-align: right;">
                        <h2>{{ Carbon\Carbon::parse($novidadesPrincipal[0]->data_novidade)->format('d/m') }}</h2>
                    </div>
                    <div class="col-md-12" style="text-align: right;">
                        <p style="text-align: right;"><em>{{$novidadesPrincipal[0]->nome}}</em></p>
                    </div>
                    <div class="col-md-12" style="padding: 0px;padding-top: 12px;width: 100%;text-align: right;">
                        <div class="fb-btn classesrdsftyfyujgfhrgsdfjggghkstgfhgdf" style="width: 100%;height: 73px;"><a href="<?php echo $novidadesPrincipal[0]->link_compra_ingresso;?>" target="_blank" style="font-size: 40px;">INGRESSOS ONLINE</a></div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="slide" id="2" style="background-image: url(public/img/Eventos.jpg);">
            {{-- <h1 class="vemPorAi">O QUE VEM POR AI</h1> --}}
                {{-- <p class="textoPag2">O QUE <span style="font-family: sans-serif;font-size: 195px;text-shadow: 13px 22px 41px rgb(26, 10, 31);color: white;">ESTÁ</span> POR VIR</p> --}}
                <p class="textoPag2">O QUE <span style="font-family: sans-serif;font-size: 195px;color: white;">ESTÁ</span> POR VIR</p>
                <div class="content third-content content3">
                    <div class="container-fluid">
                        <div class="col-md-12">
                            <div class="row">
                            @if (isset($novidades))
                                @if (count($novidades) > 1)
                                    @foreach($novidades as $novidade)
                                        @if ($novidade->principal == 'S')
                                            <div class="second-section">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="left-image" style="text-align: center;">
                                                                <div class="escurecer" style="width: 100%;height: 100%;position:absolute;transition: 0.4s;"></div>
                                                                <img src="<?php echo asset('storage/app/public/'.$segundas[0]->imagem);?>" alt="second service"  style="border: 3px solid #f9089b;">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="right-content" style="text-align: center;">
                                                                <h2>{{$novidade->nome}}</h2>
                                                                <p class="dataNovidade">{{ Carbon\Carbon::parse($novidade->data_novidade)->format('d/m') }}</p>
                                                                <div class="fb-btn"><a href="<?php echo $novidade->link_compra_ingresso;?>" target="_blank" class="botaoComprarIngresso">INGRESSOS ONLINE</a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                    @foreach($novidades as $novidade)
                                        @if ($novidade->principal == 'N')
                                            <div class="second-section">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="left-image" style="text-align: center;">
                                                                <div class="escurecer" style="width: 100%;height: 100%;position:absolute;transition: 0.4s;"></div>
                                                                <img src="<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>" alt="second service"  style="border: 3px solid #f9089b;">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="right-content" style="text-align: center;">
                                                                <h2>{{$novidade->nome}}</h2>
                                                                <p class="dataNovidade">{{ Carbon\Carbon::parse($novidade->data_novidade)->format('d/m') }}</p>
                                                                <div class="fb-btn"><a href="<?php echo $novidade->link_compra_ingresso;?>" target="_blank" class="botaoComprarIngresso">INGRESSOS ONLINE</a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            @else
                                
                            @endif
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            
        </div>
        <div class="slide" id="3" style="background-image: url(public/img/Reservas.jpg);">
            <p class="textoPag3">JA PASSARAM <span class="spanPela">PELA</span><img src="{{asset("public/img/logoMusivaBranca.png")}}" alt=""> </p>
            <div class="content fifth-content">
                <div class="container-fluid">
                    <h2 class="h2Reserva">Faça uma reserva!</h2>
                    <div class="col-md-12">
                        <form action="{{ route('reserva.novo') }}" method="post" onsubmit="return validaForm(this);">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                    <input name="nome" type="text" class="form-control inputReserva" id="nome" placeholder="Seu nome..." required="">
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <fieldset>
                                    <input name="email" type="email" class="form-control inputReserva" id="email" placeholder="Seu e-mail..." required="">
                                    </fieldset>
                                </div>
                                    <div class="col-md-12">
                                    <fieldset>
                                    <input name="n_pessoas" type="number" class="form-control inputReserva" id="n_pessoas" placeholder="Quantidade de pessoas" required="">
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <fieldset>
                                        <input name="telefone" type="text" class="form-control inputReserva" id="telefone" required="" placeholder="(99)9 9999-9999">
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <fieldset>
                                        <input name="data_reserva" type="date" class="form-control inputReserva" id="data_reserva" required="" placeholder="Data">
                                    </fieldset>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="col-md-6 col-sm-12 col-xs-12"></div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <fieldset>
                                            <button type="submit" id="form-submit" class="btn botaoReserva">Cadastrar</button>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide" id="4" style="background-image: url(public/img/Ja-Passaram-por-aqui.jpg);">
            <div class="content fourth-content">
                <div class="container-fluid">
                <h2 class="passaramMusiva">Já passaram pela MUSIVA</h2>
                    <div class="row">
                    @if (isset($portfolios))
                        @if (count($portfolios) > 1)
                            @foreach($portfolios as $portfolio)
                                <div class="col-md-3 col-sm-6" style="padding-right: 0px;padding-left: 0px;">
                                    <div class="item">
                                        <div class="thumb">
                                            <a href="#" data-lightbox="image-1"><div class="hover-effect">
                                                <div class="hover-content" style="width: 100%;height: 100%;">
                                                    <p style="width: 100%;text-align: center;font-size: 25px;font-weight: 800;height: 100%;">{{$portfolio->nome}}</p>
                                                    {{-- <h2>{{ Carbon\Carbon::parse($portfolio->data_novidade)->format('d/m/Y') }}</h2>
                                                    <p>Donec eget dictum tellus. Curabitur a interdum diam. Nulla vestibulum porttitor porta.</p> --}}
                                                </div>
                                            </div></a>
                                            <div class="image">
                                                <img src="<?php echo asset('storage/app/public/'.$portfolio->imagem_portfolio)?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach    
                        @endif
                    @else
                        
                    @endif
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="slide" id="5" style="background-image: url(public/img/A-Musiva.jpg);">
            <div class="content second-content">
                <div class="container-fluid">
                    {{-- <h2 style="font-weight: 800;color: white;font-size: 42px;text-transform: uppercase;letter-spacing: 4px;margin-top: 1%;">Nossa História!</h2> --}}
                    <div class="col-md-12">
                        
                        <div class="col-md-3">
                            <div class="left-content"  style="padding-top: 9px;padding-bottom: 24px;">
                            <div class="main-btn"></div>
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <div class="left-content" style="padding-top: 9px;padding-bottom: 24px;">
                            <div class="main-btn">
                                {{-- <a onclick="apa(1)" style="width: 100%;font-size: 28px;text-align: center;" class="apa1" id="botao1">Sobre nós</a> --}}
                            </div>
                            </div>
                        </div>  
                        <div class="col-md-3">
                            <div class="left-content" style="padding-top: 9px;padding-bottom: 24px;">
                            <div class="main-btn">
                                {{-- <a onclick="apa(2)" style="width: 100%;font-size: 28px;text-align: center;" id="botao3" class="apa2">Estrutura</a> --}}
                            </div>
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <div class="left-content"  style="padding-top: 9px;padding-bottom: 24px;">
                            <div class="main-btn"></div>
                            </div>
                        </div> 
                    </div>
                    <div id="conteudo1">
                        <div class="col-md-12">
                            <div class="left-content" style="text-align: center;padding-top: 0px;">
                                <video width="320" height="240" style="width: 100%;height: 100%;" loop controls id="videoMusiva">
                                    <source src="{{asset('public/video/musiva-informativo.mp4')}}" type="video/mp4">
                                </video>
                                <p style="padding-top: 3%;">Na música marcante ou mansa que gera o movimento e na massa de milhares de muitos que se fez dessa mistura maravilhosa que só o brasil tem.</p> 
                                <p>Nos motivos que movem e modificam o mundo. em toda manifestação que quebra o pensamento monótono e muda a mente, marco da morte do medo dos nossos próprios monstros, que afugenta a mesmice chata feito mosquito.</p>
                                <p>Também tem m na mão que mima e acaricia, seja de mãe, mulher, menino, moça ou macho. em tudo que é mágico, místico, mitológico.</p>
                                <p>No meme momentâneo que vira mania ou mimimi. na moda que movimenta o mercado e faz seu próprio marketing.</p>
                                <p>Em tudo que é múltiplo, moderno. um mosaico, uma mandala para montar de acordo com seu momento, para colocar o molho e fazer dele um mantra.</p>
                                <p>Naquilo que é mais. melhor pra você, melhor pra mim.</p>
                                <p>Musiva.</p>
                                <p>Um espaço de eventos como você nunca viu.</p>
                                <p>Um lugar de mistura e movimento.</p>
                            </div>
                        </div>
                    </div>
                    <div id="conteudo2">
                        <div class="col-md-12">
                            <div class="left-content" style="text-align: center;padding-top: 0px;">
                                <iframe width="420" height="315"
                                src="https://www.youtube.com/embed/SGWweYhkwOo">
                                </iframe>
                            </div>
                        </div>
                    </div>
                    <div id="conteudo3">
                        <div class="col-md-12">
                            <div class="left-content" style="text-align: center;">
                                <h2>Conteúdo 3</h2>
                                <p>Please tell your friends about templatemo website. A variety of free CSS templates are available for immediate downloads.</p> 
                                <p>Phasellus vitae faucibus orci. Etiam eleifend orci sed faucibus semper. Cras varius dolor et augue fringilla, eu commodo sapien iaculis. Donec eget dictum tellus. <a href="#">Curabitur</a> a interdum diam. Nulla vestibulum porttitor porta.</p>
                                <p>Nulla vitae interdum libero, vel posuere ipsum. Phasellus interdum est et dapibus tempus. Vestibulum malesuada lorem condimentum mauris ornare dapibus. Curabitur tempor ligula et <a href="#">placerat</a> molestie.</p>
                                <p>Aliquam efficitur eu purus in interdum. <a href="#">Etiam tincidunt</a> magna ex, sit amet lobortis felis bibendum id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                            </div>
                        </div>
                    </div>
                    <div id="conteudo4">
                        <div class="col-md-12">
                            <div class="left-content" style="text-align: center;">
                                <h2>Conteúdo 4</h2>
                                <p>Please tell your friends about templatemo website. A variety of free CSS templates are available for immediate downloads.</p> 
                                <p>Phasellus vitae faucibus orci. Etiam eleifend orci sed faucibus semper. Cras varius dolor et augue fringilla, eu commodo sapien iaculis. Donec eget dictum tellus. <a href="#">Curabitur</a> a interdum diam. Nulla vestibulum porttitor porta.</p>
                                <p>Nulla vitae interdum libero, vel posuere ipsum. Phasellus interdum est et dapibus tempus. Vestibulum malesuada lorem condimentum mauris ornare dapibus. Curabitur tempor ligula et <a href="#">placerat</a> molestie.</p>
                                <p>Aliquam efficitur eu purus in interdum. <a href="#">Etiam tincidunt</a> magna ex, sit amet lobortis felis bibendum id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                            </div>
                        </div>
                    </div>
                    
                    {{-- <div class="col-md-6">
                        <div class="right-image">
                            <img src="{{asset('public/pasta3/img/about_image.jpg')}}" alt="">
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    
        <div class="slide" id="6"  style="background-image: url(public/img/Contato.jpg);">
            <div class="content fifth-content">
                <div class="container-fluid">
                
                    <div class="col-md-6">
                        <div id="map">
                        <!-- How to change your own map point
                                1. Go to Google Maps
                                2. Click on your location point
                                3. Click "Share" and choose "Embed map" tab
                                4. Copy only URL and paste it within the src="" field below
                        -->
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3842.47181650222!2d-56.10003228514722!3d-15.619844089159988!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x939db1e223d840e3%3A0xbd212d145772d9c5!2sMusiva!5e0!3m2!1spt-BR!2sbr!4v1580497808035!5m2!1spt-BR!2sbr" width="100%" height="580px" frameborder="0" style="border:0;" allowfullscreen="" class="mapaResponsivo"></iframe>
                        </div>
                    </div>
                    <div class="col-md-6">
                    <h2 class="h2Contato">CONTATO!</h2>
                        <form action="{{ route('contato.novo') }}" method="post" id="contact">
                        {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                <fieldset>
                                    <input name="name" type="text" class="form-control" id="name" placeholder="Seu nome..." required="">
                                </fieldset>
                                </div>
                                <div class="col-md-12">
                                <fieldset>
                                    <input name="email" type="email" class="form-control" id="email" placeholder="Seu e-mail..." required="">
                                </fieldset>
                                </div>
                                <div class="col-md-12">
                                <fieldset>
                                    <input name="subject" type="text" class="form-control" id="subject" placeholder="Assunto..." required="">
                                </fieldset>
                                </div>
                                <div class="col-md-12">
                                <fieldset>
                                    <textarea name="message" rows="6" class="form-control" id="message" placeholder="Sua mensagem..." required=""></textarea>
                                </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 col-sm-12 col-xs-12"></div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <fieldset>
                                            <button type="submit" id="form-submit" class="btn botaoContato">Enviar</button>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="content">
            {{-- <p>Copyright &copy; 2018 Your Company . <a href="http://www.templatemo.com/tm-512-moonlight">Moonlight</a> by <a href="http://www.html5max.com" target="_parent">HTML5 Max</a></p> --}}
        </div>
    </div>
</div>
<div id="layoutResponsivo">
    {{-- <div class="bolinhaResponsivo pulseResponsivo">{!!img('m_da_musiva.png')!!}</div> --}}
        
    {{-- <div class="menuResponsivo">
        <ul class="listaResponsivo">
            <li><a href="#1">HOME</a></li>
            <li><a href="#2">EVENTOS</a></li>
            <li><a href="#3">RESERVA</a></li>
            <li><a href="#4">LEMBRANÇAS</a></li>
            <li><a href="#5">HISTÓRIA</a></li>
            <li><a href="#6">CONTATO</a></li>
        </ul>
    </div> --}}

    <div class="container-fluid" style="padding: 0px;">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    @if(isset($novidades))
                        @foreach ($novidades as $novidade)
                            
                            @if ($novidade->principal == 'S')
                                <img class="imagem-principal-responsivo" src="<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>" alt="">
                                <a href="<?php echo $novidade->link_compra_ingresso;?>">
                                <div class="botaoResponsivoComprarIngresso2" style="background-color: <?php echo $novidade->color_botao;?>">
                                    <p class="tituloCompreResponsivo2" style="color: <?php echo $novidade->color_fonte;?>">COMPRE SEU INGRESSO</p>
                                </div>
                                </a>
                            @endif
                        @endforeach
                    @else
                    @endif
                
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" style="padding: 0px;">
        <div style="width: 100%;" id="novidadesScrollAnimate">
            <div class="container-fluid fluido">
                <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <p class="lead">ENCONTRE SUA PRÓXIMA EXPERIÊNCIA</p>
                </div>
                </div>
                <!--<div id="barra-um"></div>-->
            </div>
        </div>
    </div>
    <div class="container-fluid justify-content-center paah">
        
        @if(isset($novidades))
            @foreach ($novidades as $novidade)
                @if ($novidade->principal != 'S')
                    <div class="card text-center gf" id="dff" style="width: 18rem;margin-bottom: 0px;">
                        <img class="card-img-top carta<?php echo $novidade->novidades_id;?>" src="<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>" alt="">
                        <a href="<?php echo $novidade->link_compra_ingresso;?>">
                        <div class="botaoResponsivoComprarIngresso" style="background-color: <?php echo $novidade->color_botao;?>">
                            <p class="tituloCompreResponsivo" style="color: <?php echo $novidade->color_fonte;?>">COMPRE SEU INGRESSO</p>
                        </div>
                        </a>
                        <div class="divFundo<?php echo $novidade->novidades_id;?>" ></div>
                        <div class="card-body">
                            <h5 class="card-title">Data: {{ Carbon\Carbon::parse($novidade->data_novidade)->format('d/m') }}</h5>
                        </div>
                    </div>
                @endif
            @endforeach
        @else
        @endif
    </div>
</div>
@endsection

@push('scripts')

@endpush