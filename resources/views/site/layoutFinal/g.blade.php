<div class="sequence">
    
    <div class="seq-preloader">
        <svg width="39" height="16" viewBox="0 0 39 16" xmlns="http://www.w3.org/2000/svg" class="seq-preload-indicator"><g fill="#F96D38"><path class="seq-preload-circle seq-preload-circle-1" d="M3.999 12.012c2.209 0 3.999-1.791 3.999-3.999s-1.79-3.999-3.999-3.999-3.999 1.791-3.999 3.999 1.79 3.999 3.999 3.999z"/><path class="seq-preload-circle seq-preload-circle-2" d="M15.996 13.468c3.018 0 5.465-2.447 5.465-5.466 0-3.018-2.447-5.465-5.465-5.465-3.019 0-5.466 2.447-5.466 5.465 0 3.019 2.447 5.466 5.466 5.466z"/><path class="seq-preload-circle seq-preload-circle-3" d="M31.322 15.334c4.049 0 7.332-3.282 7.332-7.332 0-4.049-3.282-7.332-7.332-7.332s-7.332 3.283-7.332 7.332c0 4.05 3.283 7.332 7.332 7.332z"/></g></svg>
    </div>

</div>
<div class="divAvisoPlay">
    <p class="pulse2 nomeAvisoPlay" style=""><strong>{{$novidadesPrincipal[0]->nome}} - </strong>{{$novidadesPrincipal[0]->nome_musica}}</p>
</div>
<nav class="">
    <div class="logo">
        <img src="{{asset("public/img/logoMusivaBranca.png")}}" alt="">
    </div>
    <div class="mini-logo">
        <img src="img/mini_logo.png" alt="">
    </div>
    <ul>
        <li><a href="#1">HOME</a></li>
        <li><a href="#2">EVENTOS</a></li>
        <li><a href="#3">RESERVA</a></li>
        <li><a href="#4">LEMBRANÇAS</a></li>
        <li><a href="#5">HISTÓRIA</a></li>
        <li><a href="#6">CONTATO</a></li>
    </ul>
    <div class="col-md-12" style="padding-top: 15%;">
        <div class="col-md-6 col-sm-6 col-xs-6 col-6">
        <i class="zmdi zmdi-view-list-alt iconePlayList"></i>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 col-6"><i class="zmdi zmdi-play iconePlay"></i><i class="zmdi zmdi-pause iconePause"></i></div>
    </div>

{{-- <div style="width: 100%;height: 20px;background-color: aqua;"></div> --}}
</nav>
<div class="issoai">
    <div class="container-fluid">
        <div class="col-md-12" style="text-align: right;">
            <h2></h2>
        </div>
    </div>
</div>
<div class="slides">
    <div class="slide foto" id="1" style="<?php if(isset($novidades)){foreach($novidades as $novidade){if($novidade->principal == 'S'){ ?>background-image:url(<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>)<?php }}}?>">
        <div style="width: 100%;height: 100%;position:absolute;">
        
        </div>
        <div class="content first-content content2">
            <div class="container-fluid" style="padding: 0px;height: 100%;">
                <div class="col-md-12 col-sm-12 col-xs-12 col-12" style="padding:0px;">
                    @if(isset($segundas))
                        @if($novidadesPrincipal[0]->novidades_id == $segundas[0]->imagem_novidade_id)
                            <div class="author-image"><img class="img2" style="width: 100%!important;margin: 0px;" src="<?php echo asset('storage/app/public/'.$segundas[0]->imagem);?>" alt=""></div>
                        @endif
                    @endif
                </div>
                <div class="col-md-12" style="text-align: center;position: absolute;top: 50%;">
                    <h2>{{ Carbon\Carbon::parse($novidade->data_novidade)->format('d/m') }}</h2>
                    <p style="text-align: center;"><em>{{$novidadesPrincipal[0]->nome}}</em></p>
                    <div class="main-btn classesrdsftyfyujgfhrgsdfjggghkstgfhgdf" style="width: 100%;height: 73px;"><a href="#2" style="font-size: 40px;">INGRESSOS ONLINE</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="slide" id="2" style="background-image: url(public/img/5303-1920x1080.jpg);">
        {{-- <h1 class="vemPorAi">O QUE VEM POR AI</h1> --}}
            <p class="textoPag2">O QUE <span style="font-family: sans-serif;font-size: 195px;text-shadow: 13px 22px 41px rgb(26, 10, 31);color: white;">ESTÁ</span> POR VIR</p>
            <div class="content third-content content3">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <div class="row">
                        @if (isset($novidades))
                            @if (count($novidades) > 1)
                                @foreach($novidades as $novidade)
                                    @if ($novidade->principal == 'N')
                                        <div class="second-section">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="left-image" style="text-align: center;">
                                                            <div class="escurecer" style="width: 100%;height: 100%;position:absolute;transition: 0.4s;"></div>
                                                            <img src="<?php echo asset('storage/app/public/'.$novidade->imagem_novidade)?>" alt="second service"  style="border: 3px solid #f9089b;">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="right-content" style="text-align: center;">
                                                            <h2>Nome cantor</h2>
                                                            <p class="dataNovidade">{{ Carbon\Carbon::parse($novidade->data_novidade)->format('d/m') }}</p>
                                                            {{-- <div class="main-btn"><a href="#4" onclick="comprar('<?php //echo $novidade->link_compra_ingresso;?>')" class="botaoComprarIngresso">Ingressos online</a></div> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        @else
                            
                        @endif
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        
    </div>
    <div class="slide" id="3" style="background-image: url(public/img/party-music-hd-wallpaper-1920x1080-3850.jpg);">
        <p class="textoPag3">JA PASSARAM <span class="spanPela">PELA</span><img src="{{asset("public/img/logoMusivaBranca.png")}}" alt=""> </p>
        <div class="content fifth-content">
            <div class="container-fluid">
                <h2 class="h2Reserva">Faça uma reserva!</h2>
                <div class="col-md-12">
                    <form action="{{ route('reserva.novo') }}" method="post" onsubmit="return validaForm(this);">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                <input name="nome" type="text" class="form-control inputReserva" id="nome" placeholder="Seu nome..." required="">
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset>
                                <input name="email" type="email" class="form-control inputReserva" id="email" placeholder="Seu e-mail..." required="">
                                </fieldset>
                            </div>
                                <div class="col-md-12">
                                <fieldset>
                                <input name="n_pessoas" type="number" class="form-control inputReserva" id="n_pessoas" placeholder="Quantidade de pessoas" required="">
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset>
                                    <input name="telefone" type="text" class="form-control inputReserva" id="telefone" required="" placeholder="(99)9 9999-9999">
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset>
                                    <input name="data_reserva" type="date" class="form-control inputReserva" id="data_reserva" required="" placeholder="Data">
                                </fieldset>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="col-md-6 col-sm-12 col-xs-12"></div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <fieldset>
                                        <button type="submit" id="form-submit" class="btn botaoReserva">Cadastrar</button>
                                    </fieldset>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="slide" id="4" style="background-image: url(public/img/imagemTOP.jpg);">
        <div class="content fourth-content">
            <div class="container-fluid">
            <h2 class="passaramMusiva">Já passaram pela MUSIVA</h2>
                <div class="row">
                @if (isset($portfolios))
                    @if (count($portfolios) > 1)
                        @foreach($portfolios as $portfolio)
                            <div class="col-md-3 col-sm-6" style="padding-right: 0px;padding-left: 0px;">
                                <div class="item">
                                    <div class="thumb">
                                        <a href="#" data-lightbox="image-1"><div class="hover-effect">
                                            <div class="hover-content" style="width: 100%;height: 100%;">
                                                <p style="width: 100%;text-align: center;font-size: 25px;font-weight: 800;height: 100%;">{{$portfolio->nome}}</p>
                                                {{-- <h2>{{ Carbon\Carbon::parse($portfolio->data_novidade)->format('d/m/Y') }}</h2>
                                                <p>Donec eget dictum tellus. Curabitur a interdum diam. Nulla vestibulum porttitor porta.</p> --}}
                                            </div>
                                        </div></a>
                                        <div class="image">
                                            <img src="<?php echo asset('storage/app/public/'.$portfolio->imagem_portfolio)?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach    
                    @endif
                @else
                    
                @endif
                    
                </div>
            </div>
        </div>
    </div>
    <div class="slide" id="5" style="background-image: url(public/img/outraImagemTOP.jpg);">
        <div class="content second-content">
            <div class="container-fluid">
                <h2 style="font-weight: 800;color: white;font-size: 42px;text-transform: uppercase;letter-spacing: 4px;margin-top: 1%;">Nossa História!</h2>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="left-content">
                        <div class="main-btn"><a onclick="apa(1)" style="width: 100%;font-size: 28px;text-align: center;" id="botao1">Botao1</a></div>
                        </div>
                    </div>  
                    <div class="col-md-3">
                        <div class="left-content">
                        <div class="main-btn"><a onclick="apa(2)" style="width: 100%;font-size: 28px;text-align: center;" id="botao2">Botao2</a></div>
                        </div>
                    </div> 
                    <div class="col-md-3">
                        <div class="left-content">
                        <div class="main-btn"><a onclick="apa(3)" style="width: 100%;font-size: 28px;text-align: center;" id="botao3">Botao3</a></div>
                        </div>
                    </div> 
                    <div class="col-md-3">
                        <div class="left-content">
                        <div class="main-btn"><a onclick="apa(4)" style="width: 100%;font-size: 28px;text-align: center;" id="botao4">Botao4</a></div>
                        </div>
                    </div> 
                </div>
                <div id="conteudo1">
                    <div class="col-md-12">
                        <div class="left-content" style="text-align: center;">
                            <h2>Conteúdo 1</h2>
                            <p>Please tell your friends about templatemo website. A variety of free CSS templates are available for immediate downloads.</p> 
                            <p>Phasellus vitae faucibus orci. Etiam eleifend orci sed faucibus semper. Cras varius dolor et augue fringilla, eu commodo sapien iaculis. Donec eget dictum tellus. <a href="#">Curabitur</a> a interdum diam. Nulla vestibulum porttitor porta.</p>
                            <p>Nulla vitae interdum libero, vel posuere ipsum. Phasellus interdum est et dapibus tempus. Vestibulum malesuada lorem condimentum mauris ornare dapibus. Curabitur tempor ligula et <a href="#">placerat</a> molestie.</p>
                            <p>Aliquam efficitur eu purus in interdum. <a href="#">Etiam tincidunt</a> magna ex, sit amet lobortis felis bibendum id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                        </div>
                    </div>
                </div>
                <div id="conteudo2">
                    <div class="col-md-12">
                        <div class="left-content" style="text-align: center;">
                            <h2>Conteúdo 2</h2>
                            <p>Please tell your friends about templatemo website. A variety of free CSS templates are available for immediate downloads.</p> 
                            <p>Phasellus vitae faucibus orci. Etiam eleifend orci sed faucibus semper. Cras varius dolor et augue fringilla, eu commodo sapien iaculis. Donec eget dictum tellus. <a href="#">Curabitur</a> a interdum diam. Nulla vestibulum porttitor porta.</p>
                            <p>Nulla vitae interdum libero, vel posuere ipsum. Phasellus interdum est et dapibus tempus. Vestibulum malesuada lorem condimentum mauris ornare dapibus. Curabitur tempor ligula et <a href="#">placerat</a> molestie.</p>
                            <p>Aliquam efficitur eu purus in interdum. <a href="#">Etiam tincidunt</a> magna ex, sit amet lobortis felis bibendum id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                        </div>
                    </div>
                </div>
                <div id="conteudo3">
                    <div class="col-md-12">
                        <div class="left-content" style="text-align: center;">
                            <h2>Conteúdo 3</h2>
                            <p>Please tell your friends about templatemo website. A variety of free CSS templates are available for immediate downloads.</p> 
                            <p>Phasellus vitae faucibus orci. Etiam eleifend orci sed faucibus semper. Cras varius dolor et augue fringilla, eu commodo sapien iaculis. Donec eget dictum tellus. <a href="#">Curabitur</a> a interdum diam. Nulla vestibulum porttitor porta.</p>
                            <p>Nulla vitae interdum libero, vel posuere ipsum. Phasellus interdum est et dapibus tempus. Vestibulum malesuada lorem condimentum mauris ornare dapibus. Curabitur tempor ligula et <a href="#">placerat</a> molestie.</p>
                            <p>Aliquam efficitur eu purus in interdum. <a href="#">Etiam tincidunt</a> magna ex, sit amet lobortis felis bibendum id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                        </div>
                    </div>
                </div>
                <div id="conteudo4">
                    <div class="col-md-12">
                        <div class="left-content" style="text-align: center;">
                            <h2>Conteúdo 4</h2>
                            <p>Please tell your friends about templatemo website. A variety of free CSS templates are available for immediate downloads.</p> 
                            <p>Phasellus vitae faucibus orci. Etiam eleifend orci sed faucibus semper. Cras varius dolor et augue fringilla, eu commodo sapien iaculis. Donec eget dictum tellus. <a href="#">Curabitur</a> a interdum diam. Nulla vestibulum porttitor porta.</p>
                            <p>Nulla vitae interdum libero, vel posuere ipsum. Phasellus interdum est et dapibus tempus. Vestibulum malesuada lorem condimentum mauris ornare dapibus. Curabitur tempor ligula et <a href="#">placerat</a> molestie.</p>
                            <p>Aliquam efficitur eu purus in interdum. <a href="#">Etiam tincidunt</a> magna ex, sit amet lobortis felis bibendum id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                        </div>
                    </div>
                </div>
                
                {{-- <div class="col-md-6">
                    <div class="right-image">
                        <img src="{{asset('public/pasta3/img/about_image.jpg')}}" alt="">
                    </div>
                </div> --}}
            </div>
        </div>
    </div>

    <div class="slide" id="6"  style="background-image: url(public/img/outraImagemTOP.jpg);">
        <div class="content fifth-content">
            <div class="container-fluid">
            
                <div class="col-md-6">
                    <div id="map">
                    <!-- How to change your own map point
                            1. Go to Google Maps
                            2. Click on your location point
                            3. Click "Share" and choose "Embed map" tab
                            4. Copy only URL and paste it within the src="" field below
                    -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3842.47181650222!2d-56.10003228514722!3d-15.619844089159988!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x939db1e223d840e3%3A0xbd212d145772d9c5!2sMusiva!5e0!3m2!1spt-BR!2sbr!4v1580497808035!5m2!1spt-BR!2sbr" width="100%" height="580px" frameborder="0" style="border:0;" allowfullscreen="" class="mapaResponsivo"></iframe>
                    </div>
                </div>
                <div class="col-md-6">
                <h2 class="h2Contato">Contato!</h2>
                    <form action="{{ route('contato.novo') }}" method="post" id="contact">
                    {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                            <fieldset>
                                <input name="name" type="text" class="form-control" id="name" placeholder="Seu nome..." required="">
                            </fieldset>
                            </div>
                            <div class="col-md-12">
                            <fieldset>
                                <input name="email" type="email" class="form-control" id="email" placeholder="Seu e-mail..." required="">
                            </fieldset>
                            </div>
                            <div class="col-md-12">
                            <fieldset>
                                <input name="subject" type="text" class="form-control" id="subject" placeholder="Assunto..." required="">
                            </fieldset>
                            </div>
                            <div class="col-md-12">
                            <fieldset>
                                <textarea name="message" rows="6" class="form-control" id="message" placeholder="Sua mensagem..." required=""></textarea>
                            </fieldset>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6 col-sm-12 col-xs-12"></div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <fieldset>
                                        <button type="submit" id="form-submit" class="btn botaoContato">Enviar</button>
                                    </fieldset>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="content">
        {{-- <p>Copyright &copy; 2018 Your Company . <a href="http://www.templatemo.com/tm-512-moonlight">Moonlight</a> by <a href="http://www.html5max.com" target="_parent">HTML5 Max</a></p> --}}
    </div>
</div>