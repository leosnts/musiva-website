<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="theme-color" content="#1b0a1d">
        <meta name="apple-mobile-web-app-status-bar-style" content="#1b0a1d">
        <meta name="msapplication-navbutton-color" content="#1b0a1d">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Musiva</title>
        <link rel="stylesheet" href="{{ asset('public/pasta2/css/slides.min.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
        {{-- <link rel="stylesheet" href="{{ asset('public/css/stilo.css') }}"> --}}
    </head>

<body>  
    
    @hasSection('body')
        @yield('body')
    @endif
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="{{asset("public/pasta2/js/slides.min.js")}}"></script>
    
    @stack('script')

</body>

</html>