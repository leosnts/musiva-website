<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="theme-color" content="#1b0a1d">
        <meta name="apple-mobile-web-app-status-bar-style" content="#1b0a1d">
        <meta name="msapplication-navbutton-color" content="#1b0a1d">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Musiva</title>
        

        <link rel="stylesheet" href="{{ asset('public/pasta3/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('public/pasta3/css/bootstrap-theme.min.css')}}">
        <link rel="stylesheet" href="{{ asset('public/pasta3/css/fontAwesome.css')}}">
        <link rel="stylesheet" href="{{ asset('public/pasta3/css/light-box.css')}}">
        <link rel="stylesheet" href="{{ asset('public/pasta3/css/templatemo-main.css')}}">
        {{-- <link rel="stylesheet" href="{{ asset('public/css/estilo.css') }}"> --}}
        {{-- <link rel="stylesheet" href="{{ asset('public/css/estilo.css') }}"> --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <script language="JavaScript">
            var resolucao = screen.width;
            setInterval(function(){ 
                let sw = screen.width;
                if(sw == resolucao){
                    return;
                }else{
                    resolucao = sw;
                    if(sw <= 425){
                        window.location.href = "http://webflavia.net.br/Musiva2020/2";            
                    }else{
                    }
                }
            }, 1000);
        if (screen.width<=425){
            resolucao = 425;
            window.location.href = "http://webflavia.net.br/Musiva2020/2";
        }else {

        }
            
        </script>
        <script>

            function apa(id){
                switch (id) {
                    case 1:
                        $('#conteudo1').css('display', 'block');
                        $('#conteudo2').css('display', 'none');
                        $('#conteudo3').css('display', 'none');
                        $('#conteudo4').css('display', 'none');
                        break;
                    case 2:
                        $('#conteudo1').css('display', 'none');
                        $('#conteudo2').css('display', 'block');
                        $('#conteudo3').css('display', 'none');
                        $('#conteudo4').css('display', 'none');
                        break;
                    case 3:
                        $('#conteudo1').css('display', 'none');
                        $('#conteudo2').css('display', 'none');
                        $('#conteudo3').css('display', 'block');
                        $('#conteudo4').css('display', 'none');
                        break;
                    case 4:
                        $('#conteudo1').css('display', 'none');
                        $('#conteudo2').css('display', 'none');
                        $('#conteudo3').css('display', 'none');
                        $('#conteudo4').css('display', 'block');
                        break;
                
                    default:
                        break;
                }
                
            }

            
        </script>
        
        <script src="{{ asset('public/pasta3/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
    </head>

<body>  
    
    @hasSection('body')
        @yield('body')
    @endif
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="{{asset("public/pasta2/js/slides.min.js")}}"></script>
    


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="{{ asset("public/pasta3/js/vendor/jquery-1.11.2.min.js")}}"></script>

    <script src="{{ asset('public/pasta3/js/vendor/bootstrap.min.js')}}"></script>
    
    <script src="{{ asset('public/pasta3/js/datepicker.js')}}"></script>
    <script src="{{ asset('public/pasta3/js/plugins.js')}}"></script>
    <script src="{{ asset('public/pasta3/js/main.js')}}"></script>

    
    <script type="text/javascript">
    $(document).ready(function() {

            // navigation click actions 
            $('.scroll-link').on('click', function(event){
                event.preventDefault();
                var sectionID = $(this).attr("data-id");
                scrollToID('#' + sectionID, 750);
            });
            // scroll to top action
            $('.scroll-top').on('click', function(event) {
                event.preventDefault();
                $('html, body').animate({scrollTop:0}, 'slow');         
            });
            // mobile nav toggle
            $('#nav-toggle').on('click', function (event) {
                event.preventDefault();
                $('#main-nav').toggleClass("open");
            });
        });
        // scroll function
        function scrollToID(id, speed){
            var offSet = 0;
            var targetOffset = $(id).offset().top - offSet;
            var mainNav = $('#main-nav');
            $('html,body').animate({scrollTop:targetOffset}, speed);
            if (mainNav.hasClass("open")) {
                mainNav.css("height", "1px").removeClass("in").addClass("collapse");
                mainNav.removeClass("open");
            }
        }
        if (typeof console === "undefined") {
            console = {
                log: function() { }
            };
        }
        </script>
        <script>
            $(document).ready(function() {
                var audioElement = document.createElement('audio');
                var vid = document.getElementById("videoMusiva");
                audioElement.setAttribute('src', '{{asset($novidadesPrincipal[0]->audio)}}');
                audioElement.setAttribute('autoplay', 'true');
                audioElement.addEventListener('ended', function() {
                    this.play();
                }, false);
                
                audioElement.addEventListener("canplay",function(){
                    $("#length").text("Duration:" + audioElement.duration + " seconds");
                    $("#source").text("Source:" + audioElement.src);
                    $("#status").text("Status: Ready to play").css("color","green");
                });
                
                audioElement.addEventListener("timeupdate",function(){
                    $("#currentTime").text("Current second:" + audioElement.currentTime);
                });
                
                $( ".bolinha" ).click(function() {
                    $("nav").toggleClass("menuAtivo");
                    $(".bolinha").toggleClass("bolinhaMenuAtivo");
                    setTimeout(function(){ $(".vemPorAi").toggleClass("sai"); }, 1000);
                    $(".textoPag2").toggleClass("aparecerTextoPag2");
                    $(".passaramMusiva").toggleClass("desaparecerMusiva");
                    $(".textoPag3").toggleClass("aparecerTextoPag3");
                });
                $(".clickMenu").click(function(){
                    $("nav").toggleClass("menuAtivo");
                    $(".bolinha").toggleClass("bolinhaMenuAtivo");
                    setTimeout(function(){ $(".vemPorAi").toggleClass("sai"); }, 1000);
                    $(".textoPag2").toggleClass("aparecerTextoPag2");
                    $(".passaramMusiva").toggleClass("desaparecerMusiva");
                    $(".textoPag3").toggleClass("aparecerTextoPag3");
                });
                $(".aMusiva").click(function(){
                    vid.play();
                });
                $(".aHome").click(function(){
                    vid.pause();
                });
                $(".aEvento").click(function(){
                    vid.pause();
                });
                $(".aReserva").click(function(){
                    vid.pause();
                });
                $(".aContato").click(function(){
                    vid.pause();
                });
                
                
                
                $( ".bolinhaResponsivo" ).click(function() {
                    $(".menuResponsivo").toggleClass("apareceResponsivo");
                });
                $( ".bolinha" ).hover(function() {
                    $(".bolinha").toggleClass("menuHover");
                    
                    $(".bolinha img").toggleClass("menuHoverimg");
                });
                $(".iconePlayList").hover(function(){
                    $(".iconePlayList").toggleClass("aumentaIcone");
                });
                $(".iconePlay").hover(function(){
                    $(".iconePlay").toggleClass("aumentaIcone");
                });
                $(".iconePause").hover(function(){
                    $(".iconePause").toggleClass("aumentaIcone");
                });
                $( ".iconePlay" ).click(function() {
                    $(".iconePause").css("display", "block");
                    $(".iconePlay").css("display", "none");
                    $(".divAvisoPlay").css("opacity", "1");
                    $(".divAvisoPlay").css("top", "-12%");
                    setTimeout(function(){ 
                        $(".divAvisoPlay").css("opacity", "0");
                        $(".divAvisoPlay").css("top", "-500%"); 
                    }, 3000);
                    audioElement.play();
                });
                $( ".iconePause" ).click(function() {
                    $(".iconePause").css("display", "none");
                    $(".iconePlay").css("display", "block");
                    audioElement.pause();
                });
                $(".iconePlayList").click(function(){
                    
                    window.open('{{$novidadesPrincipal[0]->link_playlist}}', '_blank');
                });
                
            });
        
    </script>

<script type="text/javascript">
    function validaForm(frm) {
        if(frm.nome.value == "" || frm.nome.value == null) {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o seu Nome para reserva'
            });
            frm.nome.focus();
            return false;
        }
        if(frm.email.value == "" || frm.email.value == null) {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o seu E-mail para reserva'
            });
            frm.email.focus();
            return false;
        }
        if(frm.n_pessoas.value == '') {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o seu E-mail para reserva'
            });
            frm.n_pessoas.focus();
            return false;
        }
        if(frm.data_reserva.value == "") {
            Swal.fire({
            icon: 'warning',
            title: 'Informe a data para reserva'
            });
            frm.data_reserva.focus();
            return false;
        }
        if(frm.telefone.value == "") {
            Swal.fire({
            icon: 'warning',
            title: 'Informe o telefone para reserva'
            });
            frm.telefone.focus();
            return false;
        }
        Swal.fire({
            icon: 'success',
            title: 'Formulário enviado com sucesso!'
            });

        return true;
    }
    
    </script>
    
    @stack('script')

</body>

</html>