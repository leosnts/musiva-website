<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="theme-color" content="#1b0a1d">
        <meta name="apple-mobile-web-app-status-bar-style" content="#1b0a1d">
        <meta name="msapplication-navbutton-color" content="#1b0a1d">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Musiva</title>
        <link rel="stylesheet" href="{{ asset('public/css/estilo.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="{{asset("public/css/bootstrap.min.css")}}">
        <link rel="stylesheet" href="{{ asset('public/css/estilo.css') }}">
        <link href="{{asset("public/css/aos.css")}}" rel="stylesheet">
        <script language="JavaScript">
            var resolucao = screen.width;
            setInterval(function(){ 
                let sw = screen.width;
                if(sw == resolucao){
                    return;
                }else{
                    resolucao = sw;
                    if(sw > 425){
                        window.location.href = "http://webflavia.net.br/Musiva2020/";
                    }else{
                        
                    }
                }
            }, 1000);
            
        </script>
    </head>

<body>  
    
    @hasSection('body')
        @yield('body')
    @endif

    {{-- incluindo rodape do site --}}
    @include('site.components.footer')
    {{-- fim rodape --}}

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="{{asset("public/js/bootstrap.min.js")}}"></script>
    <script src="https://use.fontawesome.com/04cc70fa39.js"></script>
    <script src="{{asset("public/js/aos.js")}}"></script>
    <script src="{{asset("public/js/player.js")}}"></script>
    <script src="{{asset("public/js/html5slider.js")}}"></script>
    <script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
    <script src="{{asset("public/js/animatescroll.js")}}"></script>
    @stack('script')

</body>

</html>