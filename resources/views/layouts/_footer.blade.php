	<input type="hidden" name="app_url" id="app_url" value="{!!url("/")!!}">
	@if (file_exists("public/js/".Route::currentRouteName()."-libs.js"))
		<script src="{{asset('public'.elixir('js/'.Route::currentRouteName().'-libs.js'))}}"></script> 
	@else
		<script src="{{asset('public'.elixir('js/default-libs.js'))}}"></script> 
	@endif

	@if (file_exists("public/js/".Route::currentRouteName().".js"))
		<script src="{{asset('public'.elixir('js/'.Route::currentRouteName().'.js'))}}" async></script>  
	@else
		<script src="{{asset('public'.elixir('js/default.js'))}}" async></script>  
	@endif
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-542ac47249eade94"></script>
		<script src="{{asset('public'.elixir('js/loadCommunity.js'))}}"></script>
		<script>
			$(document).ready(function(){
				var e = $(".selectPrincipal").val();
				if(e == 'S'){
					$(".segundaImagemClass").css("display", "block");
				}else{
					$(".segundaImagemClass").css("display", "none");
				}
				$( ".selectPrincipal" ).change(function() {
					if($(this).val() == "S"){
						$(".segundaImagemClass").css("display", "block");
					}else{
						$(".segundaImagemClass").css("display", "none");
					}
				});
			});	
		</script>
	</body>
</html>