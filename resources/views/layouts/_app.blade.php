<!-- Include the header -->
@include('layouts._header')

<!-- Set the content page -->
@yield('content')

<!-- Include the footer -->
@include('layouts._footer')