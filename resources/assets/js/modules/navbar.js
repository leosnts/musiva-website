; (function ($) {

    function Navbar() {
        $("a#navdown").click(function () {
            $("ul#navdown-content").toggleClass("active");
        });
        $("a#discipulos").click(function () {
            $("ul#navdown-discipulos").toggleClass("active");
        });
        $("a#sacramentos").click(function () {
            $("ul#navdown-sacramento").toggleClass("active");
        });
        $("a#pastorais").click(function () {
            $("ul#navdown-pastorais").toggleClass("active");
        });
        $("a#evangelizacao").click(function () {
            $("ul#navdown-evangelizacao").toggleClass("active");
        });

        $("a#community").click(function () {
            $("ul#navdown-community").toggleClass("active");
        }); 
        
        $("a#comunidades").click(function () {
            $("ul#navdown-comunidades").toggleClass("active");
         
        }); 



        $(".disciple").click(function () {
            $(".sub-disciple").addClass("active");
        });
        $(".close").click(function () {
            $(".sub-disciple").removeClass("active");
        });
    }

    new Navbar();

}(jQuery));




