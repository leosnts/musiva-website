; (function ($) {

    function Contact() {
        var _ = this;

        var $myForm = $("#contact-form").validate({
            rules: {
                name: { required: true },
                email: { required: true, email: true },
                phone: { required: true },
                subject: { required: true },
                text: { required: true }
            },
            messages: {
                name: { required: "Informe seu nome" },
                email: { required: 'Informe o seu email', email: 'Ops, informe um email válido' },
                phone: { required: "Informe o nº do seu telefone" },
                citie_state: { required: "infrome a Cidade/UF" },
                cnpj_cpf: { required: "informe o CNPJ/CPF" },
                subject: { required: "Informe o assunto" },
                text: { required: "Insira uma descrição" }
            },
            invalidHandler: function (e) {
                swal({
                    title: "OPS! Você não preencheu todos os campos!",
                    text: "Preencha todos os campos e tente novamente.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK!",
                    closeOnConfirm: true
                });

            },

            submitHandler: function (form) {

                $("#contact-form .btn-send").html("Enviando...");

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: $("#app_url").val() + "/contato",
                    data: $(form).serialize(),
                    success: function (result) {
                        swal("Contato enviado com sucesso!", "Em breve nosso atendimento retornará o seu contato.", "success")
                        form.reset();
                        grecaptcha.reset();
                    },
                    error: function (response) {
                        console.log(response);
                        swal("Atenção!", "por favor marque o campo captcha!", "error")
                    }
                });
            }
        });
    }

    new Contact();

}(jQuery));