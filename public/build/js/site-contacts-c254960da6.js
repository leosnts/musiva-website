;(function($){

    function Navigation() {
   	 	var _ =  this;

   	 	//Events
        $(document).on("click", ".btn-menu, .btn-close", function(e){
            e.preventDefault();
            _.navInit();
        });

        $('a[href*="#"]:not([href="#"])').click(function() {
            _.navInative();
           
            var hash = this.hash, 
            target   = $(hash);
            target   = target.length ? target : $('[name=' + this.hash.slice(1) +']');

            if (target.length) {
              $('html, body').animate({
                scrollTop: target.offset().top
              });
              return false;
            }            
        });

        $(".btn-top").on("click", function(e) {
            e.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return true;
        });
    }

   	Navigation.prototype.navInit =  function() {
        var _ =  this;
        _.navIsActive() ? _.navInative() : _.navActive();
    };

    Navigation.prototype.navIsActive =  function() {
        return $("body").hasClass('navIsActive');
    };

    Navigation.prototype.navActive =  function() {
        $("body").addClass('navIsActive');
    };

    Navigation.prototype.navInative =  function() {
        $("body").removeClass('navIsActive');
    };

   	new Navigation();

}(jQuery));
 
 
 
 

;(function($){

    function Mask() {
   	 	//$('[name=phone]').mask('(00) 0000-0000');
        //$('[name=cellphone]').mask('(00) 00000-0000');
        //$('[name=cpf]').mask('999.999.999-99');
        //$("[name=cnpj]").mask("99.999.999/9999-99");
        //$("[name=postal_code]").mask("99999-999");
        //$('[data-money]').mask('000.000.000.000.000,00', {reverse: true});
        //$('[data-integrate]').mask('0#');
        //$('[data-percentage]').mask('##0.00', {reverse: true});
        //$('[data-cref]').mask('999999-A/AA');
    }

   	new Mask();

}(jQuery));
 
 
 
 

; (function ($) {

    function Contact() {
        var _ = this;

        var $myForm = $("#contact-form").validate({
            rules: {
                name: { required: true },
                email: { required: true, email: true },
                phone: { required: true },
                subject: { required: true },
                text: { required: true }
            },
            messages: {
                name: { required: "Informe seu nome" },
                email: { required: 'Informe o seu email', email: 'Ops, informe um email válido' },
                phone: { required: "Informe o nº do seu telefone" },
                citie_state: { required: "infrome a Cidade/UF" },
                cnpj_cpf: { required: "informe o CNPJ/CPF" },
                subject: { required: "Informe o assunto" },
                text: { required: "Insira uma descrição" }
            },
            invalidHandler: function (e) {
                swal({
                    title: "OPS! Você não preencheu todos os campos!",
                    text: "Preencha todos os campos e tente novamente.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK!",
                    closeOnConfirm: true
                });

            },

            submitHandler: function (form) {

                $("#contact-form .btn-send").html("Enviando...");

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: $("#app_url").val() + "/contato",
                    data: $(form).serialize(),
                    success: function (result) {
                        swal("Contato enviado com sucesso!", "Em breve nosso atendimento retornará o seu contato.", "success")
                        form.reset();
                        grecaptcha.reset();
                    },
                    error: function (response) {
                        console.log(response);
                        swal("Atenção!", "por favor marque o campo captcha!", "error")
                    }
                });
            }
        });
    }

    new Contact();

}(jQuery));
; (function ($) {

    function Navbar() {
        $("a#navdown").click(function () {
            $("ul#navdown-content").toggleClass("active");
        });
        $("a#discipulos").click(function () {
            $("ul#navdown-discipulos").toggleClass("active");
        });
        $("a#sacramentos").click(function () {
            $("ul#navdown-sacramento").toggleClass("active");
        });
        $("a#pastorais").click(function () {
            $("ul#navdown-pastorais").toggleClass("active");
        });
        $("a#evangelizacao").click(function () {
            $("ul#navdown-evangelizacao").toggleClass("active");
        });

        $("a#community").click(function () {
            $("ul#navdown-community").toggleClass("active");
        }); 
        
        $("a#comunidades").click(function () {
            $("ul#navdown-comunidades").toggleClass("active");
         
        }); 



        $(".disciple").click(function () {
            $(".sub-disciple").addClass("active");
        });
        $(".close").click(function () {
            $(".sub-disciple").removeClass("active");
        });
    }

    new Navbar();

}(jQuery));





//# sourceMappingURL=site-contacts.js.map
