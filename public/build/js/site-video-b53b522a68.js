;(function($){

    function Navigation() {
   	 	var _ =  this;

   	 	//Events
        $(document).on("click", ".btn-menu, .btn-close", function(e){
            e.preventDefault();
            _.navInit();
        });

        $('a[href*="#"]:not([href="#"])').click(function() {
            _.navInative();
           
            var hash = this.hash, 
            target   = $(hash);
            target   = target.length ? target : $('[name=' + this.hash.slice(1) +']');

            if (target.length) {
              $('html, body').animate({
                scrollTop: target.offset().top
              });
              return false;
            }            
        });

        $(".btn-top").on("click", function(e) {
            e.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return true;
        });
    }

   	Navigation.prototype.navInit =  function() {
        var _ =  this;
        _.navIsActive() ? _.navInative() : _.navActive();
    };

    Navigation.prototype.navIsActive =  function() {
        return $("body").hasClass('navIsActive');
    };

    Navigation.prototype.navActive =  function() {
        $("body").addClass('navIsActive');
    };

    Navigation.prototype.navInative =  function() {
        $("body").removeClass('navIsActive');
    };

   	new Navigation();

}(jQuery));
 
 
 
 

;(function($){

	function Slides() {
		var _ =  this;

		$('.banner-home').slick({
			dots: false,
			arrows: false,
			infinite: true,
			speed: 500,
			fade: true,
			autoplay: true,
			autoplaySpeed: 3500
		});
	}
	
	new Slides();

}(jQuery));





; (function ($) {

    function Navbar() {
        $("a#navdown").click(function () {
            $("ul#navdown-content").toggleClass("active");
        });
        $("a#discipulos").click(function () {
            $("ul#navdown-discipulos").toggleClass("active");
        });
        $("a#sacramentos").click(function () {
            $("ul#navdown-sacramento").toggleClass("active");
        });
        $("a#pastorais").click(function () {
            $("ul#navdown-pastorais").toggleClass("active");
        });
        $("a#evangelizacao").click(function () {
            $("ul#navdown-evangelizacao").toggleClass("active");
        });

        $("a#community").click(function () {
            $("ul#navdown-community").toggleClass("active");
        }); 
        
        $("a#comunidades").click(function () {
            $("ul#navdown-comunidades").toggleClass("active");
         
        }); 



        $(".disciple").click(function () {
            $(".sub-disciple").addClass("active");
        });
        $(".close").click(function () {
            $(".sub-disciple").removeClass("active");
        });
    }

    new Navbar();

}(jQuery));





//# sourceMappingURL=site-video.js.map
