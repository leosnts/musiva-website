jQuery(document).ready(function() {
	var currentSong = 0;
	container = $('.container21');
	cover = $('.cover');
	play = $('#play');
	pause = $('#pause');
	mute = $('#mute');
	muted = $('#muted');
	close = $('#close');
	song = new Audio();
	duration = song.duration;
	var id = 0;
	audioPlayer();
	function audioPlayer(){
		var currentSong = 0;
		song.src = $("playlist li a")[0];
		
		song.play();
		$("#playlist li a").click(function(e){
			e.preventDefault();
			song.src = this;
			song.play();
			$("$playlist li").removeClass("current-song");
			currentSong = $(this).parent().index();
			$(this).parent().addClass("current-song");
		});
		song.addEventListener("ended", function(){
			currentSong++;
			if(currentSong == $("#playlist li a").length)
				currentSong = 0;
			$("#playlist li").removeClass("current-song");
			$("#playlist li:eq("+currentSong+")").addClass("current-song");
			song.src = $("#playlist li a")[currentSong].href;
			song.play();
		});
	}
	song.addEventListener('playing', (event) => {
		console.log('play');
	});
	
	if (song.canPlayType('audio/mpeg;')) {
    	song.type= 'audio/mpeg';
    	song.src= 'public/img/50_cent.mp3';
	} else {
    	song.type= 'audio/ogg';
    	song.src= 'public/img/50_cent.ogg';
	}



	$('body').on('click', '#play', function(e) {
		e.preventDefault();
		song.play();

		$(this).replaceWith('<a class="button gradient" id="pause" href="" title="" style="border: 5px solid white;border-radius: 50%;z-index: 10;"><img src="public/img/pause.png" style="margin-left: 8px;margin-top: 4px;" alt=""></a>');
		cover.addClass('coverLarge');
		$('#close').fadeIn(300);
		$('#seek').attr('max',song.duration);
	});


	$('body').on('click', '#pause', function(e) {
		e.preventDefault();
		song.pause();
		$(this).replaceWith('<a class="button gradient" id="play" href="" title="" style="border: 5px solid white;border-radius: 50%;z-index: 10;"><img src="public/img/Ativo3.png" style="margin-left: 10px;margin-top: 4px;" alt=""></a>');

	});

	$('body').on('click', '#diminuir', function(e) {
		e.preventDefault();
		song.volume -= 0.1;
		// $(this).replaceWith('<a class="button gradient" id="muted" href="music/50_cent.mp3" title=""></a>');

	});
	$('body').on('click', '#aumentar', function(e) {
		e.preventDefault();
		song.volume += 0.1;
		// $(this).replaceWith('<a class="button gradient" id="muted" href="music/50_cent.mp3" title=""></a>');

	});
	$('body').on('click', '#muted', function(e) {
		e.preventDefault();
		song.volume += 0.1;
		console.log(song.volume);
		$(this).replaceWith('<a class="button gradient" id="mute" href="imf/50_cent.mp3" title=""></a>');

	});

	$('#close').click(function(e) {
		e.preventDefault();
		cover.removeClass('coverLarge');
		song.pause();
		song.currentTime = 0;
		$('#pause').replaceWith('<a class="button gradient" id="play" href="img/50_cent.mp3" title=""></a>');
		$('#close').fadeOut(300);
	});



	$("#seek").bind("change", function() {
		song.currentTime = $(this).val();
		$("#seek").attr("max", song.duration);
	});

	song.addEventListener('timeupdate',function (){
		curtime = parseInt(song.currentTime, 10);
	$("#seek").attr("value", curtime);
	});









	
	
});
