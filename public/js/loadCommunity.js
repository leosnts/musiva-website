var load = document.querySelector('#load')    
        var content = document.querySelector('#content')    
    
        var linkHistoria = document.querySelector('#h')
        var linkCordenadores = document.querySelector('#c')
        var linkMissa = document.querySelector('#m')
        var linkAtividade = document.querySelector('#a')
        var linkContato = document.querySelector('#co')    
    
        var btnHistoria = document.querySelector('#historia')
        var btnCordenadores = document.querySelector('#cordenadores')
        var btnMissa = document.querySelector('#missa')
        var btnAtividade = document.querySelector('#atividade')
        var btnContato = document.querySelector('#contato')
    
        var divHistoria = document.querySelector('#hist')
        var divCordenadores = document.querySelector('#coord')
        var divMissa = document.querySelector('#mis')
        var divAtividade = document.querySelector('#ativ')
        var divContato = document.querySelector('#cont')
        var divGaleria = document.querySelector('#gallery')
    
        divCordenadores.style.display = 'none'
        divMissa.style.display = 'none'
        divAtividade.style.display = 'none'
        divContato.style.display = 'none'
        load.style.display = 'none'
    
    
        btnHistoria.addEventListener('click', function(){
            load.style.display = 'block'        
            content.style.display = 'none'   
            divGaleria.style.display = 'none'  
    
            setTimeout(function(){ 
                load.style.display = 'none'        
                content.style.display = 'block'        
                divCordenadores.style.display = 'none'
                divHistoria.style.display = 'block'
                divMissa.style.display = 'none'
                divAtividade.style.display = 'none'
                divContato.style.display = 'none'
                divGaleria.style.display = 'block'  
            }, 1000);
    
        });
    
        btnCordenadores.addEventListener('click', function(){
            load.style.display = 'block'        
            content.style.display = 'none'   
            divGaleria.style.display = 'none'  
    
            setTimeout(function(){ 
                load.style.display = 'none'  
                content.style.display = 'block'        
                divCordenadores.style.display = 'block'
                divHistoria.style.display = 'none'
                divMissa.style.display = 'none'
                divAtividade.style.display = 'none'
                divContato.style.display = 'none'
                divGaleria.style.display = 'none'
            }, 1000);
    
        });
    
        btnMissa.addEventListener('click', function(){
            load.style.display = 'block'        
            content.style.display = 'none'   
            divGaleria.style.display = 'none' 
    
            setTimeout(function(){ 
                load.style.display = 'none'  
                divCordenadores.style.display = 'none'
                content.style.display = 'block'        
                divHistoria.style.display = 'none'
                divMissa.style.display = 'block'
                divAtividade.style.display = 'none'
                divContato.style.display = 'none'
                divGaleria.style.display = 'none'
            }, 1000);
    
        });
    
        btnAtividade.addEventListener('click', function(){
            load.style.display = 'block'        
            content.style.display = 'none'   
            divGaleria.style.display = 'none'   
    
            setTimeout(function(){ 
                load.style.display = 'none'  
                divCordenadores.style.display = 'none'
                content.style.display = 'block'        
                divHistoria.style.display = 'none'
                divMissa.style.display = 'none'
                divAtividade.style.display = 'block'
                divContato.style.display = 'none'
                divGaleria.style.display = 'none'
            }, 1000);
    
        });
    
        btnContato.addEventListener('click', function(){
            load.style.display = 'block'        
            content.style.display = 'none'   
            divGaleria.style.display = 'none'
    
            setTimeout(function(){ 
                load.style.display = 'none'
                content.style.display = 'block'        
                divCordenadores.style.display = 'none'
                divHistoria.style.display = 'none'
                divMissa.style.display = 'none'
                divAtividade.style.display = 'none'
                divContato.style.display = 'block'
                divGaleria.style.display = 'none'
            }, 1000);
    
        });
    
    
        linkHistoria.addEventListener('click', function(event){
            event.preventDefault();
        });
        linkCordenadores.addEventListener('click', function(event){
            event.preventDefault();
        });
        linkMissa.addEventListener('click', function(event){
            event.preventDefault();
        });
        linkAtividade.addEventListener('click', function(event){
            event.preventDefault();
        });
        linkContato.addEventListener('click', function(event){
            event.preventDefault();
        });