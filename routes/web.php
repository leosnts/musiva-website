<?php

/*
|--------------------------------------------------------------------------
| SEO
|--------------------------------------------------------------------------|
*/
	Route::group(['namespace' => 'Seo'], function() {

		Route::get('sitemap', array('as'     => 'sitemap', 'uses' => 'SitemapController@index'));
		Route::get('sitemap.xml', array('as' => 'sitemapxml', 'uses' => 'SitemapController@index'));
		Route::get('feed', array('as'        => 'feed', 'uses' => 'FeedController@index'));
		Route::get('feed.xml', array('as'    => 'feedxml', 'uses' => 'FeedController@index'));
	});	

/*
|--------------------------------------------------------------------------
| Web Routes Site
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
	Route::group(['namespace' => 'Site'], function() { 
		Route::get('/', 'HomeController@index3');
		Route::get('/2', 'HomeController@index2');
		Route::get('/3', 'HomeController@index');
		Route::post('/contato', 'ContactController@Store')->name('contato.novo');
		Route::post('/contato2', 'ContactController@Store2')->name('contato.novo2');
		Route::post('/reserva', 'ReservaController@Store')->name('reserva.novo');
	});


/*
|--------------------------------------------------------------------------
| Web Routes CMS
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
	Route::group(['namespace' => 'Cms', 'prefix' => 'cms'], function() {
		Route::get('auth', array('as'        => 'cms-auth', 'uses' => 'LoginController@index', 'nickname' => "Login do CMS"));	
		Route::post('auth', array('as'       => 'cms-auth', 'uses' => 'LoginController@authenticate', 'nickname' => "Login do CMS"));
		Route::get('auth/logout', array('as' => 'cms-auth-logout', 'uses' => 'LoginController@logout', 'nickname' => "Logout do CMS"));

		Route::group(['middleware' => 'auth:cms'], function() {
			
			Route::get('/', array('as'                             => 'cms-home', 'uses' => 'HomeController@index', 'nickname' => "Página Inicial"));
			
			Route::get('usuarios', array('as'                      => 'cms-users', 'uses' => 'UsersController@index', 'nickname' => "Listar Usuários"));
			Route::get('usuario/novo', array('as'                  => 'cms-user-create', 'uses' => 'UsersController@create', 'nickname' => "Criar Usuário"));
			Route::post('usuario/novo', array('as'                 => 'cms-user-create', 'uses' => 'UsersController@store', 'nickname' => "Criar Usuário"));
			Route::get('usuario/{id}', array('as'                  => 'cms-user-show', 'uses' => 'UsersController@show', 'nickname' => "Visualizar Usuário"));
			Route::put('usuario/{id}', array('as'                  => 'cms-user-update', 'uses' => 'UsersController@update', 'nickname' => "Atualizar Usuário"));
			Route::get('usuario/excluir/{id}', array('as'          => 'cms-user-delete', 'uses' => 'UsersController@destroy', 'nickname' => "Excluir Usuário"));
			
			Route::get('grupos-de-usuarios', array('as'            => 'cms-roles', 'uses' => 'RolesController@index', 'nickname' => "Listar Grupos de Usuários"));
			Route::get('grupo-de-usuario/novo', array('as'         => 'cms-role-create', 'uses' => 'RolesController@create', 'nickname' => "Criar Grupo de Usuário"));
			Route::post('grupo-de-usuario/novo', array('as'        => 'cms-role-create', 'uses' => 'RolesController@store', 'nickname' => "Criar Grupo de Usuário"));
			Route::get('grupo-de-usuario/{id}', array('as'         => 'cms-role-show', 'uses' => 'RolesController@show', 'nickname' => "Visualizar Grupo de Usuário"));
			Route::put('grupo-de-usuario/{id}', array('as'         => 'cms-role-update', 'uses' => 'RolesController@update', 'nickname' => "Atualizar Grupo de Usuário"));
			Route::get('grupo-de-usuario/excluir/{id}', array('as' => 'cms-role-delete', 'uses' => 'RolesController@destroy', 'nickname' => "Excluir Grupo de Usuário"));
			
			Route::get('paises', array('as'                        => 'cms-countries', 'uses' => 'CountriesController@index', 'nickname' => "Listar Paises"));
			Route::get('pais/novo', array('as'                     => 'cms-countrie-create', 'uses' => 'CountriesController@create', 'nickname' => "Criar Pais"));
			Route::post('pais/novo', array('as'                    => 'cms-countrie-create', 'uses' => 'CountriesController@store', 'nickname' => "Criar Pais"));
			Route::get('pais/{id}', array('as'                     => 'cms-countrie-show', 'uses' => 'CountriesController@show', 'nickname' => "Visualizar Pais"));
			Route::put('pais/{id}', array('as'                     => 'cms-countrie-update', 'uses' => 'CountriesController@update', 'nickname' => "Atualizar Pais"));
			Route::get('pais/excluir/{id}', array('as'             => 'cms-countrie-delete', 'uses' => 'CountriesController@destroy', 'nickname' => "Excluir Pais"));
			
			Route::get('estados', array('as'                       => 'cms-states', 'uses' => 'StatesController@index', 'nickname' => "Listar Estados"));
			Route::get('estado/novo', array('as'                   => 'cms-state-create', 'uses' => 'StatesController@create', 'nickname' => "Criar Estado"));
			Route::post('estado/novo', array('as'                  => 'cms-state-create', 'uses' => 'StatesController@store', 'nickname' => "Criar Estado"));
			Route::get('estado/{id}', array('as'                   => 'cms-state-show', 'uses' => 'StatesController@show', 'nickname' => "Visualizar Estado"));
			Route::put('estado/{id}', array('as'                   => 'cms-state-update', 'uses' => 'StatesController@update', 'nickname' => "Atualizar Estado"));
			Route::get('estado/excluir/{id}', array('as'           => 'cms-state-delete', 'uses' => 'StatesController@destroy', 'nickname' => "Criar Estado"));
			
			Route::get('cidades', array('as'                       => 'cms-cities', 'uses' => 'CitiesController@index', 'nickname' => "Listar Cidades"));
			Route::get('cidade/novo', array('as'                   => 'cms-citie-create', 'uses' => 'CitiesController@create', 'nickname' => "Criar Cidade"));
			Route::post('cidade/novo', array('as'                  => 'cms-citie-create', 'uses' => 'CitiesController@store', 'nickname' => "Criar Cidade"));
			Route::get('cidade/{id}', array('as'                   => 'cms-citie-show', 'uses' => 'CitiesController@show', 'nickname' => "Visualizar Cidade"));
			Route::put('cidade/{id}', array('as'                   => 'cms-citie-update', 'uses' => 'CitiesController@update', 'nickname' => "Atualizar Cidade"));
			Route::get('cidade/excluir/{id}', array('as'           => 'cms-citie-delete', 'uses' => 'CitiesController@destroy', 'nickname' => "Excluir Cidade"));
			
			Route::get('sexos', array('as'                         => 'cms-sexes', 'uses' => 'SexesController@index', 'nickname' => "Listar Sexos"));
			Route::get('sexo/novo', array('as'                     => 'cms-sexe-create', 'uses' => 'SexesController@create', 'nickname' => "Criar Sexo"));
			Route::post('sexo/novo', array('as'                    => 'cms-sexe-create', 'uses' => 'SexesController@store', 'nickname' => "Criar Sexo"));
			Route::get('sexo/{id}', array('as'                     => 'cms-sexe-show', 'uses' => 'SexesController@show', 'nickname' => "Visualizar Sexo"));
			Route::put('sexo/{id}', array('as'                     => 'cms-sexe-update', 'uses' => 'SexesController@update', 'nickname' => "Atualizar Sexo"));
			Route::get('sexo/excluir/{id}', array('as'             => 'cms-sexe-delete', 'uses' => 'SexesController@destroy', 'nickname' => "Excluir Sexo"));
			
			Route::get('log-de-erros', array('as'                  => 'cms-log-errors', 'uses' => 'Log_errorsController@index', 'nickname' => "Listar Log de Erros"));
			Route::get('log-de-erros/{id}', array('as'             => 'cms-log-errors-show', 'uses' => 'Log_errorsController@show', 'nickname' => "Visualizar Log de Erro"));


			Route::get('slide', array('as'                         => 'cms-slide', 'uses' => 'SlideController@index', 'nickname' => "Listar Slide"));
			Route::get('slide/novo', array('as'                     => 'cms-slide-create', 'uses' => 'SlideController@create', 'nickname' => "Criar Slide"));
			Route::post('slide/novo', array('as'                    => 'cms-slide-create', 'uses' => 'SlideController@store', 'nickname' => "Criar Slide"));
			Route::get('slide/{id}', array('as'                     => 'cms-slide-show', 'uses' => 'SlideController@show', 'nickname' => "Visualizar Slide"));
			Route::put('slide/{id}', array('as'                     => 'cms-slide-update', 'uses' => 'SlideController@update', 'nickname' => "Atualizar Slide"));
			Route::get('slide/excluir/{id}', array('as'             => 'cms-slide-delete', 'uses' => 'SlideController@destroy', 'nickname' => "Excluir Slide"));

			Route::get('news', array('as'                         => 'cms-news', 'uses' => 'NewsController@index', 'nickname' => "Listar blog"));
			Route::get('news/novo', array('as'                     => 'cms-news-create', 'uses' => 'NewsController@create', 'nickname' => "Criar blog"));
			Route::post('news/novo', array('as'                    => 'cms-news-create', 'uses' => 'NewsController@store', 'nickname' => "Criar blog"));
			Route::get('news/{id}', array('as'                     => 'cms-news-show', 'uses' => 'NewsController@show', 'nickname' => "Visualizar blog"));
			Route::put('news/{id}', array('as'                     => 'cms-news-update', 'uses' => 'NewsController@update', 'nickname' => "Atualizar blog"));
			Route::get('news/excluir/{id}', array('as'             => 'cms-news-delete', 'uses' => 'NewsController@destroy', 'nickname' => "Excluir blog"));
	

			Route::get('video', array('as'                         => 'cms-video', 'uses' => 'VideoController@index', 'nickname' => "Listar Vídeos"));
			Route::get('video/novo', array('as'                     => 'cms-video-create', 'uses' => 'VideoController@create', 'nickname' => "Criar Vídeo"));
			Route::post('video/novo', array('as'                    => 'cms-video-create', 'uses' => 'VideoController@store', 'nickname' => "Criar Vídeo"));
			Route::get('video/{id}', array('as'                     => 'cms-video-show', 'uses' => 'VideoController@show', 'nickname' => "Visualizar Vídeo"));
			Route::put('video/{id}', array('as'                     => 'cms-video-update', 'uses' => 'VideoController@update', 'nickname' => "Atualizar Vídeo"));
			Route::get('video/excluir/{id}', array('as'             => 'cms-video-delete', 'uses' => 'VideoController@destroy', 'nickname' => "Excluir Vídeo"));


			Route::get('photo', array('as'                         => 'cms-photo', 'uses' => 'PhotoController@index', 'nickname' => "Listar Fotos"));
			Route::get('photo/novo', array('as'                     => 'cms-photo-create', 'uses' => 'PhotoController@create', 'nickname' => "Criar Foto"));
			Route::post('photo/novo', array('as'                    => 'cms-photo-create', 'uses' => 'PhotoController@store', 'nickname' => "Criar Foto"));
			Route::get('photo/{id}', array('as'                     => 'cms-photo-show', 'uses' => 'PhotoController@show', 'nickname' => "Visualizar Foto"));
			Route::put('photo/{id}', array('as'                     => 'cms-photo-update', 'uses' => 'PhotoController@update', 'nickname' => "Atualizar Foto"));
			Route::get('photo/excluir/{id}', array('as'             => 'cms-photo-delete', 'uses' => 'PhotoController@destroy', 'nickname' => "Excluir Foto"));


			Route::get('patron', array('as'                         => 'cms-patron', 'uses' => 'PatronController@index', 'nickname' => "Listar Fotos"));
			Route::get('patron/novo', array('as'                     => 'cms-patron-create', 'uses' => 'PatronController@create', 'nickname' => "Criar Foto"));
			Route::post('patron/novo', array('as'                    => 'cms-patron-create', 'uses' => 'PatronController@store', 'nickname' => "Criar Foto"));
			Route::get('patron/{id}', array('as'                     => 'cms-patron-show', 'uses' => 'PatronController@show', 'nickname' => "Visualizar Foto"));
			Route::put('patron/{id}', array('as'                     => 'cms-patron-update', 'uses' => 'PatronController@update', 'nickname' => "Atualizar Foto"));
			Route::get('patron/excluir/{id}', array('as'             => 'cms-patron-delete', 'uses' => 'PatronController@destroy', 'nickname' => "Excluir Foto"));

			Route::get('hair', array('as'                            => 'cms-hair', 'uses' => 'HairController@index', 'nickname' => 'Exibir postagem da página hair'));
			Route::get('hair/novo', array('as'                     => 'cms-hair-create', 'uses' => 'HairController@create', 'nickname' => "Criar Conteúdo Hair"));
			Route::post('hair/novo', array('as'                    => 'cms-hair-create', 'uses' => 'HairController@store', 'nickname' => "Criar Conteúdo Hair"));
			Route::get('hair/{id}', array('as'                     => 'cms-hair-show', 'uses' => 'HairController@show', 'nickname' => "Visualizar Hair"));
			Route::put('hair/{id}', array('as'                     => 'cms-hair-update', 'uses' => 'HairController@update', 'nickname' => "Atualizar Hair"));
			Route::get('hair/excluir/{id}', array('as'             => 'cms-hair-delete', 'uses' => 'HairController@destroy', 'nickname' => "Excluir Hair"));

			Route::get('novidades', array('as'                            => 'cms-novidades', 'uses' => 'NovidadesController@index', 'nickname' => 'Exibir página de novidades Musiva'));
			Route::get('novidades/novo', array('as'                     => 'cms-novidades-create', 'uses' => 'NovidadesController@create', 'nickname' => "Criar Conteúdo Novidade Musiva"));
			Route::post('novidades/novo', array('as'                    => 'cms-novidades-store', 'uses' => 'NovidadesController@store', 'nickname' => "Criar Conteúdo Novidade Musiva"));
			Route::get('novidades/{id}', array('as'                     => 'cms-novidades-show', 'uses' => 'NovidadesController@show', 'nickname' => "Visualizar Conteúdo Novidades Musiva"));
			Route::post('novidades/update', array('as'                     => 'cms-novidades-update', 'uses' => 'NovidadesController@update', 'nickname' => "Atualizar Conteúdo Novidades Musiva"));
			Route::get('novidades/excluir/{id}', array('as'             => 'cms-novidades-delete', 'uses' => 'NovidadesController@destroy', 'nickname' => "Excluir Conteúdo Novidades Musiva"));

			Route::get('portfolio', array('as' => 'cms-portfolio', 'uses' => 'PortfolioController@index', 'nickname' => 'Exibir página de portfólio de shows as Musiva'));
			Route::get('portfolio/novo', array('as' => 'cms-portfolio-create', 'uses' => 'PortfolioController@create', 'nickname' => 'Criar conteúdo de portfólio Musiva'));
			Route::post('portfolio/novo', array('as' => 'cms-portfolio-store', 'uses' => 'PortfolioController@store', 'nickname' => 'Criar conteúdo Portfólio Musiva'));
			Route::get('portfolio/excluir/{id}', array('as' => 'cms-portfolio-delete', 'uses' => 'PortfolioController@destroy', 'nickname' => 'Excluir conteúdo Portfólio Musiva'));
			Route::get('portfolio/alterar/{id}', array('as' => 'cms-portfolio-edit', 'uses' => 'PortfolioController@edit', 'nickname' => 'Editar conteúdo Portfólio Musiva'));
			Route::post('portfolio/update', array('as'                     => 'cms-portfolio-update', 'uses' => 'PortfolioController@update', 'nickname' => "update Conteúdo Novidades Musiva"));

			Route::get('makeup', array('as'                            => 'cms-makeup', 'uses' => 'MakeupController@index', 'nickname' => 'Exibir postagem da página MakeUp'));
			Route::get('makeup/novo', array('as'                     => 'cms-makeup-create', 'uses' => 'MakeupController@create', 'nickname' => "Criar Conteúdo MakeUp"));
			Route::post('makeup/novo', array('as'                    => 'cms-makeup-create', 'uses' => 'MakeupController@store', 'nickname' => "Criar Conteúdo MakeUp"));
			Route::get('makeup/{id}', array('as'                     => 'cms-makeup-show', 'uses' => 'MakeupController@show', 'nickname' => "Visualizar MakeUp"));
			Route::put('makeup/{id}', array('as'                     => 'cms-makeup-update', 'uses' => 'MakeupController@update', 'nickname' => "Atualizar MakeUp"));
			Route::get('makeup/excluir/{id}', array('as'             => 'cms-makeup-delete', 'uses' => 'MakeupController@destroy', 'nickname' => "Excluir MakeUp"));

			Route::get('unhas', array('as'                            => 'cms-unhas', 'uses' => 'UnhasController@index', 'nickname' => 'Exibir postagem da página Unhas'));
			Route::get('unhas/novo', array('as'                     => 'cms-unhas-create', 'uses' => 'UnhasController@create', 'nickname' => "Criar Conteúdo Unhas"));
			Route::post('unhas/novo', array('as'                    => 'cms-unhas-create', 'uses' => 'UnhasController@store', 'nickname' => "Criar Conteúdo Unhas"));
			Route::get('unhas/{id}', array('as'                     => 'cms-unhas-show', 'uses' => 'UnhasController@show', 'nickname' => "Visualizar Unhas"));
			Route::put('unhas/{id}', array('as'                     => 'cms-unhas-update', 'uses' => 'UnhasController@update', 'nickname' => "Atualizar Unhas"));
			Route::get('unhas/excluir/{id}', array('as'             => 'cms-unhas-delete', 'uses' => 'UnhasController@destroy', 'nickname' => "Excluir Unhas"));

			Route::get('esteticas', array('as'                            => 'cms-esteticas', 'uses' => 'EsteticasController@index', 'nickname' => 'Exibir postagem da página Estéticas'));
			Route::get('esteticas/novo', array('as'                     => 'cms-esteticas-create', 'uses' => 'EsteticasController@create', 'nickname' => "Criar Conteúdo Estéticas"));
			Route::post('esteticas/novo', array('as'                    => 'cms-esteticas-create', 'uses' => 'EsteticasController@store', 'nickname' => "Criar Conteúdo Estéticas"));
			Route::get('esteticas/{id}', array('as'                     => 'cms-esteticas-show', 'uses' => 'EsteticasController@show', 'nickname' => "Visualizar Estéticas"));
			Route::put('esteticas/{id}', array('as'                     => 'cms-esteticas-update', 'uses' => 'EsteticasController@update', 'nickname' => "Atualizar Estéticas"));
			Route::get('esteticas/excluir/{id}', array('as'             => 'cms-esteticas-delete', 'uses' => 'EsteticasController@destroy', 'nickname' => "Excluir Estéticas"));

			Route::get('barbearias', array('as'                            => 'cms-barbearias', 'uses' => 'BarbeariasController@index', 'nickname' => 'Exibir postagem da página Barbearia'));
			Route::get('barbearias/novo', array('as'                     => 'cms-barbearias-create', 'uses' => 'BarbeariasController@create', 'nickname' => "Criar Conteúdo Barbearia"));
			Route::post('barbearias/novo', array('as'                    => 'cms-barbearias-create', 'uses' => 'BarbeariasController@store', 'nickname' => "Criar Conteúdo Barbearia"));
			Route::get('barbearias/{id}', array('as'                     => 'cms-barbearias-show', 'uses' => 'BarbeariasController@show', 'nickname' => "Visualizar Barbearia"));
			Route::put('barbearias/{id}', array('as'                     => 'cms-barbearias-update', 'uses' => 'BarbeariasController@update', 'nickname' => "Atualizar Barbearia"));
			Route::get('barbearias/excluir/{id}', array('as'             => 'cms-barbearias-delete', 'uses' => 'BarbeariasController@destroy', 'nickname' => "Excluir Barbearia"));

			Route::get('priest', array('as'                         => 'cms-priest', 'uses' => 'PriestController@index', 'nickname' => "Listar Padre"));
			Route::get('priest/novo', array('as'                     => 'cms-priest-create', 'uses' => 'PriestController@create', 'nickname' => "Criar Padre"));
			Route::post('priest/novo', array('as'                    => 'cms-priest-create', 'uses' => 'PriestController@store', 'nickname' => "Criar Padre"));
			Route::get('priest/{id}', array('as'                     => 'cms-priest-show', 'uses' => 'PriestController@show', 'nickname' => "Visualizar Padre"));
			Route::put('priest/{id}', array('as'                     => 'cms-priest-update', 'uses' => 'PriestController@update', 'nickname' => "Atualizar Padre"));
			Route::get('priest/excluir/{id}', array('as'             => 'cms-priest-delete', 'uses' => 'PriestController@destroy', 'nickname' => "Excluir Padre"));

			Route::get('groupcommunity', array('as'                         => 'cms-community', 'uses' => 'CommunityController@index', 'nickname' => "Listar comunidade"));
			Route::get('groupcommunity/novo', array('as'                     => 'cms-community-create', 'uses' => 'CommunityController@create', 'nickname' => "Criar comunidade"));
			Route::post('groupcommunity/novo', array('as'                    => 'cms-community-create', 'uses' => 'CommunityController@store', 'nickname' => "Criar grupo de comunidade"));
			Route::get('community/{id}', array('as'                     => 'cms-community-show', 'uses' => 'CommunityController@show', 'nickname' => "Visualizar Comunidade"));
			Route::put('community/{id}', array('as'                     => 'cms-community-update', 'uses' => 'CommunityController@update', 'nickname' => "Atualizar Comunidade"));
			Route::get('community/excluir/{id}', array('as'             => 'cms-community-delete', 'uses' => 'CommunityController@destroy', 'nickname' => "Excluir Comunidade"));
			Route::get('community/image/{id}', array('as'             => 'cms-community-image', 'uses' => 'CommunityController@addImage', 'nickname' => "View de adicionar imagens"));
			Route::post('community/image/{id}', array('as'             => 'cms-community-addImage', 'uses' => 'CommunityController@storeImage', 'nickname' => "Cadastrar imagens"));
			Route::post('community/deleteimage/{id}', array('as'             => 'cms-community-deleteImage', 'uses' => 'CommunityController@destroyImage', 'nickname' => "Deletar imagens"));
			


			Route::get('sector', array('as'                         => 'cms-sector', 'uses' => 'SectorController@index', 'nickname' => "Listar Setor"));
			Route::get('sectorView/novo', array('as'                     => 'cms-sectorView-create', 'uses' => 'SectorController@create', 'nickname' => "Criar setor"));
			Route::post('sector/novo', array('as'                    => 'cms-sector-create', 'uses' => 'SectorController@store', 'nickname' => "Cadastrar setor"));
			Route::get('sector/{id}', array('as'                     => 'cms-sector-show', 'uses' => 'SectorController@show', 'nickname' => "Visualizar setor"));
			Route::put('sectort/{id}', array('as'                     => 'cms-sector-update', 'uses' => 'SectorController@update', 'nickname' => "Atualizar sector"));
			Route::get('sector/excluir/{id}', array('as'             => 'cms-sector-delete', 'uses' => 'SectorController@destroy', 'nickname' => "Excluir setor"));
			Route::get('sector/content/{id}', array('as'             => 'cms-sector-create-content', 'uses' => 'SectorController@addImage', 'nickname' => "View de adicionar conteudo"));

			Route::get('contacts', array('as'                         => 'cms-contacts', 'uses' => 'ContactController@index', 'nickname' => "Listar contatos"));
			Route::get('contacts/{id}', array('as'                     => 'cms-contacts-show', 'uses' => 'ContactController@show', 'nickname' => "Visualizar setor"));

			//GRUPOS DAS MINISTRAÇÃO
			Route::get('groupMinistry', array('as'                         => 'cms-group-ministry', 'uses' => 'GroupMinistryController@index', 'nickname' => "Listar grupo ministração"));
			Route::get('groupMinistry/novo', array('as'                     => 'cms-groupMinistry-create', 'uses' => 'GroupMinistryController@create', 'nickname' => "Criar grupo ministração view"));
			Route::post('groupMinistry/novo', array('as'                    => 'cms-groupMinistry-create', 'uses' => 'GroupMinistryController@store', 'nickname' => "Criar grupo de ministração"));
			Route::get('groupMinistry/{id}', array('as'                     => 'cms-groupMinistry-show', 'uses' => 'GroupMinistryController@show', 'nickname' => "Editar grupo ministração"));
			Route::put('groupMinistry/{id}', array('as'                     => 'cms-groupMinistry-update', 'uses' => 'GroupMinistryController@update', 'nickname' => "Atualizar grupo ministração"));
			Route::get('groupMinistry/excluir/{id}', array('as'             => 'cms-groupMinistry-delete', 'uses' => 'GroupMinistryController@destroy', 'nickname' => "Deletar grupo ministração"));

			//GERENCIAR MINISTRAÇÕES 
			Route::get('ministry', array('as'                         => 'cms-ministry', 'uses' => 'MinistryController@index', 'nickname' => "Listar ministração"));
			Route::get('ministry/novo', array('as'                     => 'cms-ministry-create', 'uses' => 'MinistryController@create', 'nickname' => "Criar ministração"));
			Route::post('ministry/novo', array('as'                    => 'cms-ministry-create', 'uses' => 'MinistryController@store', 'nickname' => "Criar de ministração"));
			Route::get('ministry/{id}', array('as'                     => 'cms-ministry-show', 'uses' => 'MinistryController@show', 'nickname' => "Editar ministração"));
			Route::put('ministry/{id}', array('as'                     => 'cms-ministry-update', 'uses' => 'MinistryController@update', 'nickname' => "Atualizar ministração"));
			Route::get('ministry/excluir/{id}', array('as'             => 'cms-ministry-delete', 'uses' => 'MinistryController@destroy', 'nickname' => "Deletar ministração"));
		});
	});

/*
|--------------------------------------------------------------------------
| Web Routes Uploads
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

	Route::group(['middleware' => 'auth:cms'], function() {
		Route::get('upload', array('as'    => 'flexUpload', 'uses' => 'FlexUploadController@get', 'nickname' => "Listar Uploads"));
		Route::post('upload', array('as'   => 'flexUpload', 'uses' => 'FlexUploadController@create', 'nickname' => "Criar Upload"));
		Route::put('upload', array('as'    => 'flexUpload', 'uses' => 'FlexUploadController@update', 'nickname' => "Atualizar Upload"));
		Route::delete('upload', array('as' => 'flexUpload', 'uses' => 'FlexUploadController@destroy', 'nickname' => "Exluir Upload"));
	});
