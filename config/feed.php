<?php

return [
    /*
    | -------------------------------------------------------------------------
    | Feed Config
    | -------------------------------------------------------------------------
    |
    |   
    |   just move the models and the method that is
    |   the feed for the feed to be mounted
    |
    |
    |   EXAMPLE USAGE
    |   array(
    |       'model'    => 'model\name_model',
    |       'method'   => 'name_method',
    |       'nickname' => 'Name of the route'
    |   ),
    |   array(
    |       'model'    => 'name_model',
    |       'method'   => 'name_method',
    |       'nickname' => 'Name of the route'
    |   )
    |
    */
    'feed' => array(
    )
];
