<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SegundaImagem extends Model
{
    protected $table = 'segunda_imagem';
    protected $primaryKey = 'segunda_imagem_id';
}
