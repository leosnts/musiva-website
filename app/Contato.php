<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table = 'contacts_site';
    protected $primaryKey = 'contact_id';
}
