<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table = 'shows_portfolio_musiva';
    protected $primaryKey = 'portfolio_id'; 
}
