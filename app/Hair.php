<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hair extends Model
{
    protected $table = 'hairs';
    protected $primaryKey = 'hairs_id';
}
