<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Esteticas extends Model
{
    protected $table = 'esteticas';
    protected $primaryKey = 'esteticas_id';
}
