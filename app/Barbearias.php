<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barbearias extends Model
{
    protected $table = 'barbearias';
    protected $primaryKey = 'barbearias_id';
}
