<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Makeup extends Model
{
    protected $table = 'makeups';
    protected $primaryKey = 'makeups_id';
}
