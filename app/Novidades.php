<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Novidades extends Model
{
    protected $table = 'novidades';
    protected $primaryKey = 'novidades_id';
}
