<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Imagem extends Model
{
    protected $table      = "imagem_community";
	protected $primaryKey = 'imagem_id';
	protected $fillable   = ['imagem_id','community_id', 'path', 'created_at', 'updated_at'];
}
