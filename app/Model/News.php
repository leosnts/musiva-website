<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table      = "news";
	protected $primaryKey = 'news_id';
	protected $fillable   = ['title', 'subtitle', 'content', 'image', 'featured', 'status', 'created_at', 'updated_at'];

	public function status()
	{
		switch($this->status){
			case "1":
			return "Ativo";
			break;
			case "2":
			return "Inativo";
			break;
		}
	}
	public function featured()
	{
		switch($this->featured){
			case "1":
			return "Destaque";
			break;
			case "2":
			return "Notícia";
			break;
		}
	}
}