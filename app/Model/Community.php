<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    //
    protected $table      = "group_community";
	protected $primaryKey = 'community_id';
	protected $fillable   = ['community_id','sector_id', 'name','history','coordinators','mass','activities','contact', 'status', 'created_at', 'updated_at'];
}
