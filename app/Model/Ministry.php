<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ministry extends Model
{
    //
    protected $table = 'ministry';
    protected $primaryKey = 'ministry_id';
    protected $fillable = ['ministry_id', 'group_ministry_id', 'name', 'created_at', 'updated_at'];
}
