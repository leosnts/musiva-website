<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupMinistry extends Model
{
    //
    protected $table      = "group_ministry";
	protected $primaryKey = 'group_ministry_id';
	protected $fillable   = ['name','created_at', 'updated_at'];
}
