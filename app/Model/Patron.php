<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Patron extends Model
{
    protected $table      = "patron";
	protected $primaryKey = 'patron_id';
	protected $fillable   = ['title', 'content', 'status', 'created_at', 'updated_at'];

	public function status()
	{
		switch($this->status){
			case "1":
			return "Ativo";
			break;
			case "2":
			return "Inativo";
			break;
		}
	}
}