<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $table      = "contacts_site";
	protected $primaryKey = 'contact_id';
	protected $fillable   = ['contact_id','name', 'email','cellFone','subject','message', 'created_at', 'updated_at'];
}
