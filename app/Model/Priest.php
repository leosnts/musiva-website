<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Priest extends Model
{
    protected $table      = "priest";
	protected $primaryKey = 'priest_id';
	protected $fillable   = [ 'name', 'description', 'image', 'status', 'created_at', 'updated_at'];

	public function status() {
		
		switch ($this->status) {
			case '1':
				return "Ativo";
			break;
			case '2':
				return "Inativo";
			break;
		}
	}
}
