<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Sector;


class Sector extends Model
{
    protected $table      = "sector_community";
	protected $primaryKey = 'id_sector';
	protected $fillable   = ['id_sector','name', 'id_community', 'created_at', 'updated_at'];

	public function community(){
		return $this->belongsTo('App\Model\Community');
	}
}


