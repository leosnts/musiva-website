<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unhas extends Model
{
    protected $table = 'unhas';
    protected $primaryKey = 'unhas_id';
}
