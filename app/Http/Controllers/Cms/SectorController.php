<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sector;


class SectorController extends Controller
{
    public function index(Request $request)
    {
        $sector = Sector::orderBy("id_sector", "DESC")->get();

        if($request->input('name'))
        {
           $sector->where('name', 'like', '%'.$request->input('name').'%');
        }

        return view("cms/sectorCommunity/index", array(
            "sector" => $sector
        ));
    }


    public function create()
    {
        return view("cms/sectorCommunity/show");
    }


    public function store(Request $request)
    {

        try {
            $sector = Sector::create($request->all());
            return redirect(route('cms-sector-show', $sector->id_sector)); 
        } catch (Exception $e) {
            $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
            return redirect(route('cms-sector')); 
        }        
    }

    public function show($id)
    {   
        $sector = Sector::find($id);

        if (empty($sector)) {
            abort(404);
        }

        return view("cms/sectorCommunity/show", array(
            "sector" => $sector
        ));
    }


    public function update(Request $request, $id)
    {

        try {
           
            Sector::find($id)->update($request->all());
            $request->session()->flash('alert', array('code'=> 'success', 'text'  => 'Operação realizada com sucesso!'));
        } catch (Exception $e) {
            $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
        }
       
        return redirect(route('cms-sector'));
    }

    public function destroy(Request $request, $id)
    {
        try {
            $sector = Sector::find($id);


            if(empty($sector)) {
                abort(404);
            }

            $sector->delete();
            $request->session()->flash('alert', array('code'=> 'success', 'text'  => 'Operação realizada com sucesso!'));
        } catch (Exception $e) {
            $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
        }
       
        return redirect(route('cms-sector'));
    }

    public function contentView()
    {
        return view('cms.addGroupCommunity.content');
    }
}
