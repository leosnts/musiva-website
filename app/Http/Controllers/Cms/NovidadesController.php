<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Novidades;
use App\SegundaImagem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class NovidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $novidades = Novidades::all();
        
        return view("cms/novidades/index", compact('novidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("cms/novidades/show");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $novidade = new Novidades();
        $segunda_imagem = new SegundaImagem();
        $validator = Validator::make($request->all(), [
            'imagem_novidade' => 'required',
            'nome' => 'required',
            'data_novidade' => 'required',
            'color_botao' => 'required',
            'color_fonte' => 'required',
            'link_compra_ingresso' => 'required',
            'principal' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect(route('cms-novidades-create'))
                        ->withErrors($validator)
                        ->withInput();
        }
        if($request->input('principal') == 'S')
        {
            DB::table('novidades')->update(['principal' => 'N']);
        }
        $imagem_novidade = $request->file('imagem_novidade');
        $novidade->data_novidade = $request->input('data_novidade');

        $novidade->nome = $request->input('nome');
        
        
        $novidade->link_playlist = $request->input('link_playlist');
        $novidade->color_botao = $request->input('color_botao');
        $novidade->color_fonte  = $request->input('color_fonte');
        $novidade->link_compra_ingresso = $request->input('link_compra_ingresso');
        $novidade->principal = $request->input('principal');
        $novidade->save();
        if($request->file('audio')){
            // $music_file = $request->file('audio');
            // $filename = $music_file->getClientOriginalExtension();
            // $location = public_path('audio/' . $filename);
            // $music_file->move($location,$filename);
    
            // dd($location);
            // $novidade->nome_musica = $request->input('nome_musica');
            // $audio = $request->file('audio');
            // $pathAudio = $audio->store('imagem_novidade/'.$novidade->novidades_id.'/audio', 'public');
            // $pieces = explode(".", $pathAudio);
            // $pieces[1] = "mp3";
            // $parara = $pieces[0].".".$pieces[1];
            // dd($parara);
            // $novidade->audio = $parara;
        }
        
        // $size = count($images);
        // foreach ($images as $i) {
        //     $imagem = new Imagem();
        //     $path = $i->store('imageCommunity', 'public');
        //     $imagem->community_id = $id;
        //     $imagem->path = $path;
        //     $imagem->save();
        // }
            $path = $imagem_novidade->store('imagem_novidade/'.$novidade->novidades_id, 'public');
            if($request->file('segunda_imagem')){
                $imagem = $request->file('segunda_imagem');
                $segunda_imagem->imagem_novidade_id = $novidade->novidades_id;
                $segunda_imagem->save();
            }
            if ($request->file('audio')) {
                
                // The file
                $file = $request->file('audio');
                // File extension
                $extention = $file->getClientOriginalExtension();
                // File name ex: my-audio-song.mp3
                $name = str_slug($request->input('nome_musica')) . '.' . $extention;
                // Path
                $public_path = public_path();
                // Save location /public/audio/mp3
                $location = $public_path . '/audio/'.$novidade->novidades_id;
                // Move file to /public/audio/mp3 and save it as my-audio-song.mp3
                $p = $file->move($location, $name);
                $diretoriofinal = "public/audio/".$novidade->novidades_id."/".$name;
                $novidade->nome_musica = $request->input('nome_musica');
                $novidade->audio = $diretoriofinal;

            }
            // dd($path);
            // $imagem->community_id = $id;
            $novidade->imagem_novidade = $path;
            $novidade->update();
            if($request->file("segunda_imagem")){
                $path2 = $imagem->store('imagem_novidade/'.$novidade->novidades_id.'/segunda_imagem/'.$segunda_imagem->segunda_imagem_id, 'public');
                $segunda_imagem->imagem = $path2;
                $segunda_imagem->update();
            }
            
        return redirect(route('cms-novidades'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $novidade = Novidades::find($id);
        $segunda_imagem = SegundaImagem::where('imagem_novidade_id', $novidade->novidades_id)->get();
        
        return view("cms/novidades/show", compact('novidade', 'segunda_imagem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        if($request->input('principal') == 'S')
        {
            DB::table('novidades')->update(['principal' => 'N']);
        }
        $novidade = Novidades::find($id);
        $segunda_imagem = SegundaImagem::where('imagem_novidade_id', $novidade->novidades_id)->get();
        $validator = Validator::make($request->all(), [
            'nome' => 'required',
            'data_novidade' => 'required',
            'color_botao' => 'required',
            'color_fonte' => 'required',
            'link_compra_ingresso' => 'required',
            'principal' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect(route('cms-novidades-create'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $novidade->data_novidade = $request->input('data_novidade');
        $novidade->nome = $request->input('nome');
        $novidade->link_playlist = $request->input('link_playlist');
        $novidade->color_botao = $request->input('color_botao');
        $novidade->color_fonte  = $request->input('color_fonte');
        $novidade->link_compra_ingresso = $request->input('link_compra_ingresso');
        $novidade->principal = $request->input('principal');
        $novidade->save();
        
        if($request->file('imagem_novidade')){
            $imagem_novidade = $request->file('imagem_novidade');
            $path = $imagem_novidade->store('imagem_novidade/'.$novidade->novidades_id, 'public');
            $novidade->imagem_novidade = $path;
            $novidade->update();
        }
        if($request->file('segunda_imagem')){
            if(count($segunda_imagem) == 0){
                $segunda_imagem_novo = new SegundaImagem();
                $imagem = $request->file('segunda_imagem');
                $segunda_imagem_novo->imagem_novidade_id = $novidade->novidades_id;
                $segunda_imagem_novo->save();
                $path2 = $imagem->store('imagem_novidade/'.$novidade->novidades_id.'/segunda_imagem/'.$segunda_imagem_novo->segunda_imagem_id, 'public');
                $segunda_imagem_novo->imagem = $path2;
                $segunda_imagem_novo->update();
            }else{
                $imagem = $request->file('segunda_imagem');
                $path2 = $imagem->store('imagem_novidade/'.$novidade->novidades_id.'/segunda_imagem/'.$segunda_imagem[0]->segunda_imagem_id, 'public');
                $segunda_imagem[0]->imagem_novidade_id = $novidade->novidades_id;
                $segunda_imagem[0]->imagem = $path2;
                $segunda_imagem[0]->update();
            }
            
        }
        if ($request->file('audio')) {
            // The file
            $file = $request->file('audio');
            // File extension
            $extention = $file->getClientOriginalExtension();
            // File name ex: my-audio-song.mp3
            $name = str_slug($request->input('nome_musica')) . '.' . $extention;
            // Path
            $public_path = public_path();
            // Save location /public/audio/mp3
            $location = $public_path . '/audio/'.$novidade->novidades_id;
            // Move file to /public/audio/mp3 and save it as my-audio-song.mp3
            $p = $file->move($location, $name);
            $diretoriofinal = "public/audio/".$novidade->novidades_id."/".$name;
            $novidade->nome_musica = $request->input('nome_musica');
            $novidade->audio = $diretoriofinal;
            $novidade->update();
        }   
        return redirect(route('cms-novidades'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $imagem = Novidades::find($id);
        if (isset($imagem)) {
            $arquivo = $imagem->path;
            Storage::delete($arquivo);
            if($imagem->delete()){
                return 200;
            }else{
                return 0;
            }
        }
    }
}
