<?php

namespace App\Http\Controllers\Cms;

use App\Barbearias;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BarbeariasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barbearias = Barbearias::all();
        return view("cms/barbearias/index", compact('barbearias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $barbearias = new Barbearias();
        $barbearias->conteudo = $request->input('content');
        $barbearias->save();
        return redirect(route('cms-barbearias'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {  
            $b = Barbearias::find($id);
            $b->conteudo = $request->input('content');
            $b->save();
            return redirect(route('cms-barbearias'));
        } catch (Exception $e) {
            return redirect(route('cms-barbearias'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
