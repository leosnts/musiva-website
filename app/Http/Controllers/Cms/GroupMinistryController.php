<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\GroupMinistry;

class GroupMinistryController extends Controller
{
    public function index(Request $request)
    {   
        $groupMinistry = GroupMinistry::all(); 

        return view("cms/groupMinistry/index", array(
            'groupMinistry' => $groupMinistry
        ));
    }

    public function create(Request $request)
    {
        return view("cms/groupMinistry/show");
    }

    public function store(Request $request)
    {
        $ministry = new GroupMinistry();

        $name = $request->input('name');

        $ministry->name = $name;
        $ministry->save();

        return redirect()->route('cms-group-ministry');
    }
    
    public function show(Request $request, $id)
    {

        $ministry = GroupMinistry::find($id);

        if(isset($ministry)){
            return view('cms/groupMinistry/edit', array(
                "ministry" => $ministry
            ));
        } else {
            abort(404);
        }

    }

    public function update(Request $request, $id)
    {  

        $ministry = GroupMinistry::find($id);
        
        $name = $request->input('name');

        if(isset($ministry)){

            $ministry->name = $name;
            $ministry->save();

            return redirect()->route('cms-group-ministry')->with('message', 'Alteração realizada com sucesso');

        } else {
            abort(404);
        }
    }

    public function destroy($id)
    {
        $ministry = GroupMinistry::find($id);
        if(isset($ministry)){
            $ministry->delete();
            return redirect()->route('cms-group-ministry')->with('message-delete', 'Grupo de ministração excluida sucesso');
        } else {
            abort(404);
        }
    }

}
