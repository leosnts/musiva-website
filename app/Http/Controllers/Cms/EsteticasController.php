<?php

namespace App\Http\Controllers\Cms;

use App\Esteticas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EsteticasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $esteticas = Esteticas::all();
        return view("cms/esteticas/index", compact('esteticas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $esteticas = new Esteticas();
        $esteticas->conteudo = $request->input('content');
        $esteticas->save();
        return redirect(route('cms-esteticas'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {  
            $e = Esteticas::find($id);
            $e->conteudo = $request->input('content');
            $e->save();
            return redirect(route('cms-esteticas'));
        } catch (Exception $e) {
            return redirect(route('cms-esteticas'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
