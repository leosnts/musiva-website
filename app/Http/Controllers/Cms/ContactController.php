<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Contact;


class ContactController extends Controller
{
    public function index(Request $request){
        
        $contacts = Contact::orderBy("contact_id", "DESC");

        if($request->input('name'))
        {
           $contacts->where('name', 'like', '%'.$request->input('name').'%');
        }
        
        return view("cms/contacts/index", array(
            "contacts" => $contacts->paginate(50)
        ));
    }

    public function show($id)
    {   
        $contacts = Contact::find($id);

        if (isset($contacts)) {
            $contacts = Contact::where("contact_id", "=", $id)->get();
            return view('cms.contacts.show', array(
                "contacts" => $contacts
            ));
        }
    }
}
