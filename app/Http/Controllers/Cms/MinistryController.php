<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Ministry;
use App\Model\GroupMinistry;


class MinistryController extends Controller
{

    public function index()
    {   
        
        $ministry = Ministry::all();

        return view('cms/ministry/index', array(
            "ministry" => $ministry
        ));
    }
    
    
    public function create()
    {
        $groupMinistry = GroupMinistry::orderBy('group_ministry_id', 'asc')->get();
        return view('cms/ministry/create', array(
            "groupMinistry" => $groupMinistry
        ));
    }

    public function store(Request $request)
    {
        
        $ministry = new Ministry();

        $name = $request->input('name');
        $group_id = $request->input('group_id');
        $content = $request->input('content');

        $ministry->name = $name;
        $ministry->content = $content;
        $ministry->group_ministry_id = $group_id;
        $ministry->save();

        return redirect()->route('cms-ministry');
    
    }

    public function show($id)
    {
        
        $ministry = Ministry::find($id);
        $groupMinistry = GroupMinistry::orderBy('group_ministry_id', 'asc')->get();

        if(isset($ministry)){
            return view('cms/ministry/edit', array(
                "ministry" => $ministry,
                "groupMinistry" => $groupMinistry
            ));
        } else {
            abort(404);
        }
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $ministry = Ministry::find($id);
        
        $name = $request->input('name');
        $group_id = $request->input('group_id');
        $content = $request->input('content');

        if(isset($ministry)){
    
            $ministry->name = $name;
            $ministry->content = $content;
            $ministry->group_ministry_id = $group_id;
            $ministry->save();

            return redirect()->route('cms-ministry')->with('message', 'Alteração realizada com sucesso');

        } else {
            abort(404);
        }
    }

    public function destroy($id)
    {
        $ministry = Ministry::find($id);
        if(isset($ministry)){
            $ministry->delete();
            return redirect()->route('cms-ministry');
        } else {
            abort(404);
        }
    }
}
