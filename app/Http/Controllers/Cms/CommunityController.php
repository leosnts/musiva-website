<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Model\Community;
use App\Model\Sector;
use App\Model\Imagem;

class CommunityController extends Controller
{

    public function index(Request $request)
    {
        $community = Community::orderBy("community_id", "DESC")->get();

        if($request->input('name'))
        {
           $priest->where('name', 'like', '%'.$request->input('name').'%');
        }

        if($request->input('status'))
        {
           $priest->where('status', $request->input('status'));
        }

        return view("cms/addGroupCommunity/index", array(
            "community" => $community
        ));
    }


    public function create()
    {
        $sector = Sector::orderBy("id_sector", "asc")->get();
        
        return view("cms/addGroupCommunity/show", array(
            "sector" => $sector
        ));
    }


    public function store(Request $request)
    {

        try {
            $gropCommunity = Community::create($request->all());
            return redirect(route('cms-community-show', $gropCommunity->community_id)); 
        } catch (Exception $e) {
            $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
            return redirect(route('cms-priest')); 
        }        
    }

    public function show($id)
    {   
        $gropCommunity = Community::find($id);
        $sector = Sector::orderBy("id_sector", "asc")->get();
        

        if (empty($gropCommunity)) {
            abort(404);
        }

        return view("cms/addGroupCommunity/show", array(
            "gropCommunity" => $gropCommunity,
            "sector" => $sector                        
        ));
    }


    public function update(Request $request, $id)
    {

        try {
           
            Community::find($id)->update($request->all());
            $request->session()->flash('alert', array('code'=> 'success', 'text'  => 'Operação realizada com sucesso!'));
        } catch (Exception $e) {
            $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
        }
       
        return redirect(route('cms-community'));
    }

    public function destroy(Request $request, $id)
    {
        try {
            $community = Community::find($id);


            if(empty($community)) {
                abort(404);
            }

            $community->delete();
            $request->session()->flash('alert', array('code'=> 'success', 'text'  => 'Operação realizada com sucesso!'));
        } catch (Exception $e) {
            $request->session()->flash('alert', array('code'=> 'error', 'text'  => $e));
        }
       
        return redirect(route('cms-community'));
    }

    public function addImage($id)
    {   

        $imagem = Imagem::where('community_id', '=', $id)->get();
        return view('cms.addGroupCommunity.image', array(
            "idImage" => $id,
            "imagem" => $imagem
        ));
    }
    
    public function storeImage(Request $request, $id)
    {   


        // $this->validate($request, [
        //     'image' => 'required',
        // ]);


        $images = $request->file('image');
        $size = count($images);
        foreach ($images as $i) {
            $imagem = new Imagem();
            $path = $i->store('imageCommunity', 'public');
            $imagem->community_id = $id;
            $imagem->path = $path;
            $imagem->save();
        }
        return redirect(route('cms-community'))->with('message', 'Imagens adicionadas com sucesso!');


    }

    public function destroyImage(Request $request, $id)
    {   
        $imagem = Imagem::find($id);
        if (isset($imagem)) {
            $arquivo = $imagem->path;
            Storage::disk('public')->delete($arquivo);
            $imagem->delete();
        }

    }
}
