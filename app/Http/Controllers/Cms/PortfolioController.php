<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Portfolio;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::all();
        return view("cms/shows-portfolio/index", compact('portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("cms/shows-portfolio/show");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $portfolio = new Portfolio();
        $validator = Validator::make($request->all(), [
            'imagem_portfolio' => 'required',
            'nome' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect(route('cms-portfolio-create'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $imagem_portfolio = $request->file('imagem_portfolio');

        $portfolio->save();

        // $size = count($images);
        // foreach ($images as $i) {
        //     $imagem = new Imagem();
        //     $path = $i->store('imageCommunity', 'public');
        //     $imagem->community_id = $id;
        //     $imagem->path = $path;
        //     $imagem->save();
        // }
            $path = $imagem_portfolio->store('imagem_portfolio/'.$portfolio->portfolio_id, 'public');
            // dd($path);
            // $imagem->community_id = $id;
            $portfolio->imagem_portfolio = $path;
            $portfolio->nome = $request->input('nome');
            $portfolio->update();
            
        return redirect(route('cms-portfolio'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolio = Portfolio::find($id);
        return view("cms/shows-portfolio/show", compact('portfolio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $portfolio = Portfolio::find($request->input('id'));
        
        $imagem_portfolio = $request->file('imagem_portfolio');
        $portfolio->nome = $request->input('nome');
        if($imagem_portfolio){
            $path = $imagem_portfolio->store('imagem_portfolio/'.$portfolio->portfolio_id, 'public');
            $portfolio->imagem_portfolio = $path;
        }else{

        }
        $portfolio->update();
        return redirect(route('cms-portfolio'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $imagem = Portfolio::find($id);
        if (isset($imagem)) {
            $arquivo = $imagem->path;
            Storage::delete($arquivo);
            $imagem->delete();
        }
    }
}
