<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Novidades;
use App\Portfolio;
use App\SegundaImagem;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function index()
    {
        $novidades = Novidades::all();
        $novidadesPrincipal = Novidades::where('principal', 'S')->first();
        $portfolios = Portfolio::all();
        $segundas = SegundaImagem::all();
        // return view('site.home.index', compact('novidades'), compact('portfolios'));
        return view('site.layoutNovo.home', compact('novidades'), compact('portfolios'), compact('segundas'), compact('novidadesPrincipal'));
    }
    public function index2()
    {
        $novidades = Novidades::all();
        $portfolios = Portfolio::all();
        return view('site.home.index', compact('novidades'), compact('portfolios'));
        // return view('site.layoutNovo.home', compact('novidades'), compact('portfolios'));
    }
    public function index3()
    {
        $novidades = DB::table('novidades')->orderBy('data_novidade', 'asc')->get();
        $novidadesPrincipal = Novidades::where('principal', 'S')->get();
        $portfolios = DB::table('shows_portfolio_musiva')->orderBy('created_at', 'desc')->get();
        
        $segundas = SegundaImagem::where('imagem_novidade_id', $novidadesPrincipal[0]->novidades_id)->get();
        return view('site.layoutFinal.home', compact('novidades', 'portfolios', 'segundas', 'novidadesPrincipal'));
        // return view('site.layoutNovo.home', compact('novidades'), compact('portfolios'));
    }
    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
