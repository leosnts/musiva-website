<?php

namespace App\Http\Controllers\Site;

use App\Contato;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contato = new Contato();
        $contato->name = $request->input('name');
        $contato->email = $request->input('email');
        $contato->subject = $request->input('subject');
        $contato->message = $request->input('message');
        $contato->save();
        return redirect('/');
    }
    public function store2(Request $request)
    {
        $contato = new Contato();
        $contato->name = $request->input('name');
        $contato->email = $request->input('email');
        $contato->subject = $request->input('subject');
        $contato->message = $request->input('message');
        // multiple recipients
        $to  = 'matheus1995pini@hotmail.com';

        // subject
        $subject = 'Contato';

        // message
        $message = '
        <html>
        <head>
        <title>Contato</title>
        </head>
        <body>
        <p>Contato do site!</p>
        <table>
        <tr>
        <th>Nome</th><th>E-mail</th><th>Assunto</th>
        </tr>
        <tr>
        <td>'.$request->input('name').'</td><td>'.$request->input('email').'</td><td>'.$request->input('subject').'</td>
        </tr>
        </table>
        <p>Mensagem</p>
        <br>
        <p>'.$request->input('message').'</p>
        </body>
        </html>
        ';

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        $headers .= 'To: mat <matheus1995pini@hotmail.com>' . "\r\n";
        $headers .= 'From: Musiva <'.$request->input('email').'>' . "\r\n";
        $headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
        $headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";

        // Mail it
        mail($to, $subject, $message, $headers);
        return redirect('/2');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
